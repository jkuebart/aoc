#include <cassert>
#include <iostream>
#include <iterator>
#include <string>
#include <unordered_set>
#include <vector>

namespace
{

struct Instruction
{
    std::string mnemonic;
    int operand;
};

std::istream& operator>>(std::istream& is, Instruction& instruction)
{
    return is >> instruction.mnemonic >> instruction.operand;
}

class Processor
{
public:
    /// @param program The program to run.
    /// @return The set of executed locations.
    std::unordered_set<std::size_t> run(
        std::vector<Instruction> const& program);

    std::size_t pc() const;
    int acc() const;

private:
    std::size_t mPc{0};
    int mAcc{0};
};

std::unordered_set<std::size_t> Processor::run(
    std::vector<Instruction> const& program)
{
    std::unordered_set<std::size_t> pcs{};
    while (mPc < program.size() && pcs.insert(mPc).second) {
        Instruction const& inst{program[mPc]};
        if ("acc" == inst.mnemonic) {
            mAcc += inst.operand;
        } else if ("jmp" == inst.mnemonic) {
            mPc += static_cast<std::size_t>(inst.operand - 1);
        }
        ++mPc;
    }
    return pcs;
}

std::size_t Processor::pc() const
{
    return mPc;
}

int Processor::acc() const
{
    return mAcc;
}

} // namespace

int main()
{
    std::vector<Instruction> const instructions{
        std::istream_iterator<Instruction>(std::cin),
        std::istream_iterator<Instruction>()};

    Processor proc{};
    auto const locations{proc.run(instructions)};
    std::cout << "loop: acc=" << proc.acc() << '\n';

    for (auto const patch : locations) {
        std::vector<Instruction> fix{instructions};
        if ("jmp" == fix[patch].mnemonic) {
            fix[patch].mnemonic = "nop";
        } else if ("nop" == fix[patch].mnemonic) {
            fix[patch].mnemonic = "jmp";
        } else {
            continue;
        }
        Processor fixProc{};
        fixProc.run(fix);
        if (fix.size() == fixProc.pc()) {
            std::cout << "fixed: acc=" << fixProc.acc() << '\n';
        }
    }

    return 0;
}
