#include "streamer/stream.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <iterator>
#include <map>
#include <set>
#include <string>
#include <string_view>
#include <unordered_map>
#include <vector>

using streamer::stream;

namespace
{

std::string_view trim(std::string_view str, std::string_view const& space = " ")
{
    auto const firstNot{str.find_first_not_of(space)};
    if (std::string::npos == firstNot) {
        return {};
    }
    return str.substr(firstNot, 1 + str.find_last_not_of(space) - firstNot);
}

std::vector<std::string> split(
    std::string_view const& str,
    std::string_view const& separator = " ")
{
    std::vector<std::string> res{};
    for (std::string::size_type pos{0};;) {
        auto const nextPos{str.find(separator, pos)};
        res.emplace_back(str.substr(pos, nextPos - pos));
        if (std::string::npos == nextPos) {
            break;
        }
        pos = separator.size() + nextPos;
    }
    return res;
}

} // namespace

int main()
{
    std::unordered_map<std::string, unsigned> recipes{};
    std::map<std::string, std::set<std::string>> allergens;
    std::unordered_map<std::string, std::set<std::string>> ingredients;
    unsigned recipe{0};
    for (std::string line; std::getline(std::cin, line); ++recipe) {
        auto const paren{line.find('(')};
        assert(0 != paren);
        assert(std::string::npos != paren);
        assert("(contains " == line.substr(paren, 10));
        auto const ins{split(line.substr(0, paren - 1))};
        auto const alls{split(trim(line.substr(10 + paren), " )"), ", ")};
        for (auto const& ingredient : ins) {
            ++recipes[ingredient];
            allergens[ingredient].insert(alls.begin(), alls.end());
        }
        for (auto const& allergen : alls) {
            auto const al{ingredients.find(allergen)};
            if (ingredients.end() == al) {
                ingredients.insert({allergen, {ins.begin(), ins.end()}});
            } else {
                std::set<std::string> const set0{al->second};
                std::set<std::string> const set1{ins.begin(), ins.end()};
                al->second.clear();
                std::set_intersection(
                    set0.begin(),
                    set0.end(),
                    set1.begin(),
                    set1.end(),
                    std::inserter(al->second, al->second.end()));
            }
        }
    }

    bool changed{true};
    while (changed) {
        changed = false;
        for (auto const& [al, ins] : ingredients) {
            if (1 == ins.size()) {
                changed = true;
                auto const in{*ins.begin()};
                for (auto& [al0, ins0] : ingredients) {
                    ins0.erase(in);
                }
                for (auto& [in0, als] : allergens) {
                    if (in == in0) {
                        als = {al};
                    } else {
                        als.erase(al);
                    }
                }
            }
        }
    }

    unsigned safe{0};
    std::map<std::string, std::string> inerts{};
    for (auto const& [in, als] : allergens) {
        if (als.empty()) {
            safe += recipes[in];
        } else {
            inerts.insert({*als.begin(), in});
        }
    }

    std::cout << "safe=" << safe << " inerts=";
    char const* sep{""};
    for (auto const& [al, in] : inerts) {
        std::cout << sep << in;
        sep = ",";
    }
    std::cout << '\n';

    return 0;
}
