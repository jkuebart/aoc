#include "geo/direction.h"
#include "geo/point.h"

#include <array>
#include <iostream>
#include <string>
#include <vector>

using Direction = geo::Direction<2, unsigned>;
using Point = geo::Point<Direction>;

int main()
{
    std::vector<std::string> maze{};
    for (std::string line{}; std::getline(std::cin, line);) {
        maze.push_back(std::move(line));
    }
    std::array<Direction, 5> const slopes{
        {{1, 1}, {3, 1}, {5, 1}, {7, 1}, {1, 2}}};
    unsigned long product{1};
    for (auto const& slope : slopes) {
        Point pos{0, 0};
        unsigned count{0};
        while (pos[1] < maze.size()) {
            if ('#' == maze[pos[1]][pos[0] % maze[pos[1]].size()]) {
                ++count;
            }
            pos = pos + slope;
        }
        product *= count;
        std::cout << slope << ": " << count << '\n';
    }
    std::cout << product << '\n';
    return 0;
}
