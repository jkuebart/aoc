#include <iostream>
#include <string>
#include <vector>

namespace
{

constexpr unsigned kMod{20201227};

unsigned transform(unsigned subject, unsigned loop)
{
    std::uint64_t result{1};
    for (; 0 != loop; loop >>= 1) {
        if (loop % 2) {
            result = (result * subject) % kMod;
        }
        subject = (static_cast<std::uint64_t>(subject) * subject) % kMod;
    }
    return static_cast<unsigned>(result);
}

unsigned findLoop(unsigned const subject, unsigned const pub)
{
    unsigned loop{0};
    while (pub != transform(subject, loop)) {
        ++loop;
    }
    return loop;
}

} // namespace

int main()
{
    unsigned cardPub{};
    unsigned doorPub{};
    std::cin >> cardPub >> doorPub;
    unsigned const cardLoop{findLoop(7, cardPub)};
    std::cout << "cardLoop=" << cardLoop
              << " key=" << transform(doorPub, cardLoop) << '\n';
    unsigned const doorLoop{findLoop(7, doorPub)};
    std::cout << "doorLoop=" << doorLoop
              << " key=" << transform(cardPub, doorLoop) << '\n';
    return 0;
}
