#include "geo/bounds.h"
#include "geo/direction.h"
#include "geo/point.h"
#include "streamer/stream.h"

#include <iostream>
#include <set>
#include <string>
#include <vector>

using Direction = geo::Direction<2>;
using Point = geo::Point<Direction>;
using Bounds = geo::Bounds<Point>;
using streamer::stream;

constexpr Direction kEast{1, 0};
constexpr Direction kNorthEast{0, 1};
constexpr Direction kNorthWest{-1, 1};
constexpr Direction kSouthEast{-kNorthWest};
constexpr Direction kSouthWest{-kNorthEast};
constexpr Direction kWest{-kEast};

int main()
{
    std::set<Point> flipped{};
    Bounds bounds{};
    for (std::string line{}; std::getline(std::cin, line);) {
        Point point{};
        for (std::string::size_type pos{0}; line.size() != pos; ++pos) {
            switch (line[pos]) {
            case 'e':
                point = point + kEast;
                break;
            case 'n':
                ++pos;
                switch (line[pos]) {
                case 'e':
                    point = point + kNorthEast;
                    break;
                case 'w':
                    point = point + kNorthWest;
                    break;
                }
                break;
            case 's':
                ++pos;
                switch (line[pos]) {
                case 'e':
                    point = point + kSouthEast;
                    break;
                case 'w':
                    point = point + kSouthWest;
                    break;
                }
                break;
            case 'w':
                point = point + kWest;
                break;
            }
        }
        if (0 == flipped.count(point)) {
            flipped.insert(point);
            bounds.include(point);
        } else {
            flipped.erase(point);
        }
    }
    std::cout << "black=" << flipped.size() << '\n';

    for (unsigned day{100}; 0 != day; --day) {
        std::set<Point> newFlipped{};
        Bounds newBounds{};
        bounds.inflate(1).each(
            [&flipped, &newFlipped, &newBounds](Point const& point)
            {
                std::set<Point>::size_type const count{
                    flipped.count(point + kEast) +
                    flipped.count(point + kNorthEast) +
                    flipped.count(point + kNorthWest) +
                    flipped.count(point + kSouthEast) +
                    flipped.count(point + kSouthWest) +
                    flipped.count(point + kWest)};
                if (2 == count || (1 == count && 0 != flipped.count(point))) {
                    newFlipped.insert(point);
                    newBounds.include(point);
                }
            });
        flipped = newFlipped;
        bounds = newBounds;
    }
    std::cout << "day=100 black=" << flipped.size() << '\n';

    return 0;
}
