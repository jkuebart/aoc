#include <algorithm>
#include <iostream>
#include <string>
#include <string_view>
#include <vector>

namespace
{

int Main(std::vector<std::string_view> args)
{
    auto const bufferLength{
        static_cast<unsigned>(std::stoi(std::string{args.at(1)}))};
    std::vector<unsigned> buffer(bufferLength);
    for (unsigned& i : buffer) {
        std::cin >> i;
    }
    unsigned candidate{};
    while (std::cin >> candidate) {
        bool ok{false};
        for (std::size_t i{buffer.size() - bufferLength}; i != buffer.size();
             ++i) {
            if (buffer[i] == candidate - buffer[i]) {
                continue;
            }
            if (buffer.end() !=
                std::find(
                    buffer.end() - bufferLength,
                    buffer.end(),
                    candidate - buffer[i]))
            {
                ok = true;
                break;
            }
        }
        if (!ok) {
            break;
        }
        buffer.push_back(candidate);
    }
    std::cout << "candidate=" << candidate << '\n';

    for (auto start{buffer.begin()}; start != buffer.end(); ++start) {
        unsigned sum{0};
        for (auto end{1 + start}; end != buffer.end() && sum < candidate; ++end)
        {
            sum += end[-1];
            if (candidate == sum) {
                auto const min{*std::min_element(start, end)};
                auto const max{*std::max_element(start, end)};
                std::cout << "min=" << min << " max=" << max
                          << " sum=" << (min + max) << '\n';
                return 0;
            }
        }
    }

    return 0;
}

} // namespace

int main(int argc, char* argv[])
{
    return Main(std::vector<std::string_view>{argv, argc + argv});
}
