#include "geo/bounds.h"
#include "geo/direction.h"
#include "geo/point.h"

#include <iostream>
#include <map>
#include <set>
#include <string>

using Direction = geo::Direction<4>;
using Point = geo::Point<Direction>;
using Bounds = geo::Bounds<Point>;

int main()
{
    std::set<Point> cube{};
    int row{0};
    for (std::string line{}; std::getline(std::cin, line);) {
        int column{0};
        for (char const ch : line) {
            if ('#' == ch) {
                cube.insert({column, row, 0, 0});
            }
            ++column;
        }
        ++row;
    }

    for (unsigned cycle{0}; 6 != cycle; ++cycle) {
        std::cout << "cycle=" << cycle << " count=" << cube.size() << '\n';
        std::map<Point, unsigned> neighbours{};
        for (auto const& pt : cube) {
            Bounds{pt}.inflate(1).eachBoundaryPoint(
                [&neighbours](auto const& neighbour)
                {
                    ++neighbours[neighbour];
                });
        }
        std::set<Point> newCube{};
        for (auto const& p : neighbours) {
            if (3 == p.second || (2 == p.second && 0 != cube.count(p.first))) {
                newCube.insert(p.first);
            }
        }
        cube = newCube;
    }

    std::cout << "count=" << cube.size() << '\n';
    return 0;
}
