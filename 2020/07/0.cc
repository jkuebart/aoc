#include <cassert>
#include <functional>
#include <iostream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace
{

struct Bag
{
    std::string colour;
};

bool operator==(Bag const& lhs, Bag const& rhs)
{
    return lhs.colour == rhs.colour;
}

std::istream& operator>>(std::istream& is, Bag& bag)
{
    std::string adjective, colour, dummy;
    is >> adjective >> colour >> dummy;
    assert("bag" == dummy.substr(0, 3));
    bag.colour = adjective + " " + colour;
    return is;
}

#ifdef notdef
std::ostream& operator<<(std::ostream& os, Bag const& bag)
{
    return os << bag.colour << " bag";
}
#endif

} // namespace

template<>
struct std::hash<Bag>
{
    std::size_t operator()(Bag const& bag) const
    {
        return hash<std::string>{}(bag.colour);
    }
};

namespace
{

class BagPolicy
{
private:
    struct ContainedBag
    {
        unsigned const count;
        Bag const bag;
    };

public:
    BagPolicy& insert(Bag const& outer, unsigned const count, Bag const& inner)
    {
        mContained.emplace(inner, outer);
        mContains.emplace(outer, ContainedBag{count, inner});
        return *this;
    }

    std::size_t containedIn(Bag const& bag) const
    {
        std::vector<Bag> newContainers{1, bag};
        std::unordered_set<Bag> containers{};
        while (!newContainers.empty()) {
            Bag const inner{newContainers.back()};
            newContainers.pop_back();
            auto const range{mContained.equal_range(inner)};
            for (auto it{range.first}; it != range.second; ++it) {
                if (containers.insert(it->second).second) {
                    newContainers.push_back(it->second);
                }
            }
        }
        return containers.size();
    }

    unsigned contains(Bag const& bag) const
    {
        unsigned count{0};
        auto const range{mContains.equal_range(bag)};
        for (auto it{range.first}; it != range.second; ++it) {
            count += it->second.count * (1 + contains(it->second.bag));
        }
        return count;
    }

private:
    std::unordered_multimap<Bag, Bag> mContained;
    std::unordered_multimap<Bag, ContainedBag> mContains;
};

} // namespace

int main()
{
    BagPolicy bagPolicy{};
    for (std::string line{}; std::getline(std::cin, line);) {
        std::istringstream is{line};
        std::string dummy;
        Bag outer;
        is >> outer >> dummy;
        assert("contain" == dummy);
        is >> dummy;
        if ("no" == dummy) {
            continue;
        }
        while (is) {
            auto const num{std::stoi(dummy)};
            Bag bag;
            is >> bag >> dummy;
            if (0 < num) {
                bagPolicy.insert(outer, static_cast<unsigned>(num), bag);
            }
        }
    }

    std::cout << "containedIn=" << bagPolicy.containedIn({"shiny gold"})
              << "\ncontains=" << bagPolicy.contains({"shiny gold"}) << '\n';

    return 0;
}
