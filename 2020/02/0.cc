#include <algorithm>
#include <iostream>
#include <string>

namespace
{

struct Policy
{
    bool check0(std::string_view const& password);
    bool check1(std::string_view const& password);

    int low;
    int high;
    char chr;
};

std::istream& operator>>(std::istream& is, Policy& policy);

bool Policy::check0(std::string_view const& password)
{
    auto const count{std::count(password.begin(), password.end(), chr)};
    return low <= count && count <= high;
}

bool Policy::check1(std::string_view const& password)
{
    auto const ulow{static_cast<unsigned>(low - 1)};
    auto const uhigh{static_cast<unsigned>(high - 1)};
    if (password.size() <= uhigh || uhigh < ulow) {
        return false;
    }
    return (chr == password[ulow]) != (chr == password[uhigh]);
}

std::istream& operator>>(std::istream& is, Policy& policy)
{
    std::string dummy{};
    if (std::getline(is, dummy, '-')) {
        policy.low = std::stoi(dummy);
        std::getline(is, dummy, ' ');
        policy.high = std::stoi(dummy);
        std::getline(is, dummy, ' ');
        policy.chr = dummy.at(0);
    }
    return is;
}

} // namespace

int main()
{
    unsigned valid0{0};
    unsigned valid1{0};
    Policy policy;
    while (std::cin >> policy) {
        std::string password;
        std::getline(std::cin, password);
        if (policy.check0(password)) {
            ++valid0;
        }
        if (policy.check1(password)) {
            ++valid1;
        }
    }
    std::cout << valid0 << '\n' << valid1 << '\n';
    return 0;
}
