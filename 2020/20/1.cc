#include "streamer/stream.h"

#include <algorithm>
#include <array>
#include <cassert>
#include <iomanip>
#include <iostream>
#include <map>
#include <numeric>
#include <optional>
#include <string>
#include <unordered_map>
#include <vector>

using streamer::stream;

namespace
{

/// I represent a flip and rotation-invariant ID for a tile border.
struct BorderId
{
    constexpr BorderId(unsigned const x, unsigned const y)
    : first{x < y ? x : y}
    , second{y < x ? x : y}
    {}

    unsigned first;
    unsigned second;
};

bool operator<(BorderId const& lhs, BorderId const& rhs)
{
    return std::tie(lhs.first, lhs.second) < std::tie(rhs.first, rhs.second);
}

#ifdef notdef
std::ostream& operator<<(std::ostream& os, BorderId const& borderId)
{
    return os << "id=<" << borderId.first << ',' << borderId.second << '>';
}
#endif

unsigned stringId(std::string_view const& s)
{
    unsigned id{0};
    for (char ch : s) {
        id *= 2;
        if ('#' == ch) {
            ++id;
        }
    }
    return id;
}

std::array<std::string, 4> borders(std::vector<std::string> const& maze)
{
    std::string top{maze.front()};
    std::string bottom{maze.back()};
    std::string left{};
    std::string right{};
    for (auto const& row : maze) {
        left.push_back(row.front());
        right.push_back(row.back());
    }
    return {top, right, bottom, left};
}

struct Tile
{
    static std::array<BorderId, 4> borderIds(
        std::vector<std::string> const& maze);

    Tile(std::vector<std::string> const& maze);

    BorderId const& borderOn(bool flip, unsigned side) const;

    std::vector<std::string> const maze;
    // Top, right, bottom, left.
    std::array<BorderId, 4> const borders;
};

std::array<BorderId, 4> Tile::borderIds(std::vector<std::string> const& maze)
{
    auto const brdrs{::borders(maze)};
    auto flippedBrdrs{brdrs};
    for (auto& brdr : flippedBrdrs) {
        std::reverse(brdr.begin(), brdr.end());
    }
    return {
        BorderId{stringId(brdrs[0]), stringId(flippedBrdrs[0])},
        BorderId{stringId(brdrs[1]), stringId(flippedBrdrs[1])},
        BorderId{stringId(brdrs[2]), stringId(flippedBrdrs[2])},
        BorderId{stringId(brdrs[3]), stringId(flippedBrdrs[3])},
    };
}

Tile::Tile(std::vector<std::string> const& maze_)
: maze(maze_)
, borders(borderIds(maze_))
{}

BorderId const& Tile::borderOn(bool const flip, unsigned side) const
{
    side %= 4;
    return borders[flip ? (4 - side) % 4 : side];
}

struct Placement
{
    int tileId;
    bool flip;
    // Which border is up.
    unsigned rotation;
};

#ifdef notdef
std::ostream& operator<<(std::ostream& os, Placement const& placement)
{
    return os << "{tileId=" << placement.tileId << " flip=" << placement.flip
              << " rotation=" << placement.rotation << '}';
}
#endif

std::array<unsigned, 4> borderIds(std::vector<std::string> const& maze)
{
    auto const brdrs{borders(maze)};
    return {
        stringId(brdrs[0]),
        stringId(brdrs[1]),
        stringId(brdrs[2]),
        stringId(brdrs[3]),
    };
}

std::vector<std::string> flip(std::vector<std::string> maze)
{
    for (auto& row : maze) {
        std::reverse(row.begin(), row.end());
    }
    return maze;
}

std::vector<std::string> rotate(std::vector<std::string> const& maze)
{
    std::vector<std::string> newMaze(maze.size());
    for (unsigned i{0}; i != newMaze.size(); ++i) {
        for (auto const& row : maze) {
            newMaze[i].push_back(row[row.size() - i - 1]);
        }
    }
    return newMaze;
}

unsigned occupancy(std::vector<std::string> const& maze)
{
    return std::accumulate(
        maze.begin(),
        maze.end(),
        0u,
        [](unsigned const acc, std::string const& str)
        {
            return acc + std::count(str.begin(), str.end(), '#');
        });
}

} // namespace

int main()
{
    // A map from border ID to tile IDs.
    std::map<BorderId, std::vector<int>> borderTile{};

    std::unordered_map<int, Tile> tiles{};
    for (std::string line{}; std::getline(std::cin, line);) {
        assert("Tile " == line.substr(0, 5));
        int const tileId{std::stoi(line.substr(5))};
        std::vector<std::string> maze{};
        for (std::string row{}; std::getline(std::cin, row);) {
            if (row.empty()) {
                break;
            }
            maze.push_back(std::move(row));
        }
        Tile tile{maze};
        for (BorderId const& borderId : tile.borders) {
            borderTile[borderId].push_back(tileId);
        }
        tiles.emplace(tileId, std::move(maze));
    }

    int cornerTileId{};
    for (auto const& [tileId, tile] : tiles) {
        auto const multiple{std::count_if(
            tile.borders.begin(),
            tile.borders.end(),
            [&borderTile](BorderId const& id)
            {
                return 1 != borderTile[id].size();
            })};

        // Inner tiles have three or four multiply occurring border IDs.
        if (multiple < 3) {
            cornerTileId = tileId;
            break;
        }
    }

    // Place a corner tile in the top left corner.
    auto const& cornerTile{tiles.at(cornerTileId)};
    Placement topTilePlacement{};
    for (unsigned rotation{0}; 4 != rotation; ++rotation) {
#ifdef notdef
        std::cout << "cornerTileId=" << cornerTileId << " rotation=" << rotation
                  << " top="
                  << borderTile[cornerTile.borderOn(false, rotation)].size()
                  << " left="
                  << borderTile[cornerTile.borderOn(false, 3 + rotation)].size()
                  << '\n';
#endif
        if (2 ==
            borderTile[cornerTile.borderOn(false, rotation)].size() +
                borderTile[cornerTile.borderOn(false, 3 + rotation)].size())
        {
            topTilePlacement = {cornerTileId, false, rotation};
            // std::cout << "topTilePlacement=" << topTilePlacement << '\n';
            break;
        }
    }

    // Find an adjacent unplaced tile for the borderId.
    auto const findTile = [&borderTile](
                              int const tileId,
                              BorderId const& borderId) -> std::optional<int>
    {
        for (int const adjacent : borderTile[borderId]) {
            if (tileId != adjacent) {
                return adjacent;
            }
        }
        return std::nullopt;
    };

    std::vector<std::string> finalMaze{};
    unsigned finalRow{0};
    auto const addTile =
        [&finalMaze, &finalRow](std::vector<std::string> const& maze)
    {
        unsigned const border{1};
        for (auto it{border + maze.begin()}; it != maze.end() - border; ++it) {
            if (finalMaze.size() <= finalRow) {
                finalMaze.resize(1 + finalRow);
            }
            finalMaze[finalRow].insert(
                finalMaze[finalRow].end(),
                border + it->begin(),
                it->end() - border);
            ++finalRow;
        }
        // std::cout << "\nfinalMaze:\n" << stream(finalMaze, "\n") << '\n';
    };
    {
        auto topMaze{cornerTile.maze};
        for (unsigned rotation{0}; topTilePlacement.rotation != rotation;
             ++rotation) {
            topMaze = rotate(topMaze);
        }
        addTile(topMaze);
    }
    for (;;) {
        for (Placement placement{topTilePlacement};;) {
            auto const& tile{tiles.at(placement.tileId)};
            auto topMaze{tile.maze};
            if (placement.flip) {
                topMaze = flip(topMaze);
            }
            for (unsigned i{0}; i != placement.rotation; ++i) {
                topMaze = rotate(topMaze);
            }
            unsigned const bottomBorderId{borderIds(topMaze)[2]};

#ifdef notdef
            std::cout << "finding neighbours for placement=" << placement
                      << " bottomBorderId=" << bottomBorderId << '\n';
#endif

            // Find a tile below.
            if (auto const adjacent{findTile(
                    placement.tileId,
                    tile.borderOn(placement.flip, 2 + placement.rotation))})
            {
                auto const& neighbour{tiles.at(*adjacent)};
                auto maze{neighbour.maze};

                // Find flipping and rotation.
                for (unsigned flipped{0}; 2 != flipped; ++flipped) {
                    if (flipped) {
                        maze = flip(maze);
                    }
                    for (unsigned rotation{0}; 4 != rotation; ++rotation) {
                        unsigned const topId{borderIds(maze)[0]};
                        if (bottomBorderId == topId) {
                            placement = {*adjacent, !!flipped, rotation};
                            flipped = 1;
                            break;
                        }
                        maze = rotate(maze);
                    }
                }
                // std::cout << "  below=" << placement << '\n';
                addTile(maze);
            } else {
                // std::cout << "finished column\n\n";
                break;
            }
        }

        auto const& topTile{tiles.at(topTilePlacement.tileId)};
        auto topMaze{topTile.maze};
        if (topTilePlacement.flip) {
            topMaze = flip(topMaze);
        }
        for (unsigned i{0}; i != topTilePlacement.rotation; ++i) {
            topMaze = rotate(topMaze);
        }
        unsigned const rightBorderId{borderIds(topMaze)[1]};

#ifdef notdef
        std::cout << "finding neighbours for placement=" << topTilePlacement
                  << " rightBorderId=" << rightBorderId << '\n';
#endif

        // Find a tile to the right.
        if (auto const adjacent{findTile(
                topTilePlacement.tileId,
                topTile.borderOn(
                    topTilePlacement.flip,
                    1 + topTilePlacement.rotation))})
        {
            auto const& neighbour{tiles.at(*adjacent)};
            auto maze{neighbour.maze};

            // Find flipping and rotation.
            for (unsigned flipped{0}; 2 != flipped; ++flipped) {
                if (flipped) {
                    maze = flip(maze);
                }
                for (unsigned rotation{0}; 4 != rotation; ++rotation) {
                    unsigned const leftId{borderIds(maze)[3]};
                    if (rightBorderId == leftId) {
                        topTilePlacement = {*adjacent, !!flipped, rotation};
                        flipped = 1;
                        break;
                    }
                    maze = rotate(maze);
                }
            }
            // std::cout << "  right=" << topTilePlacement << '\n';
            finalRow = 0;
            addTile(maze);
        } else {
            // std::cout << "finished board\n\n";
            break;
        }
    }

    std::vector<std::string> const monster{
        "                  # ",
        "#    ##    ##    ###",
        " #  #  #  #  #  #   ",
    };

    unsigned occpncy{occupancy(finalMaze)};
    std::cout << "occupancy=" << occpncy << " monster=" << occupancy(monster)
              << '\n';
    for (unsigned flipped{0}; 2 != flipped; ++flipped) {
        for (unsigned rotation{0}; 4 != rotation; ++rotation) {
            for (unsigned x{0};
                 x != 1 + finalMaze[0].size() - monster[0].size();
                 ++x) {
                for (unsigned y{0}; y != 1 + finalMaze.size() - monster.size();
                     ++y) {
                    bool match{true};
                    for (unsigned row{0}; match && row != monster.size(); ++row)
                    {
                        for (unsigned i{0}; match && i != monster[row].size();
                             ++i) {
                            match = '#' != monster[row][i] ||
                                '#' == finalMaze[y + row][x + i];
                        }
                    }
                    if (match) {
                        occpncy -= occupancy(monster);
                    }
                }
            }
            finalMaze = rotate(finalMaze);
        }
        finalMaze = flip(finalMaze);
    }
    std::cout << "occupancy=" << occpncy << '\n';

    return 0;
}
