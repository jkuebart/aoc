#include "streamer/stream.h"

#include <algorithm>
#include <array>
#include <cassert>
#include <iomanip>
#include <iostream>
#include <map>
#include <numeric>
#include <optional>
#include <string>
#include <unordered_map>
#include <vector>

using streamer::stream;

namespace
{

/// I represent a flip and rotation-invariant ID for a tile border.
struct BorderId
{
    constexpr BorderId(unsigned const x, unsigned const y)
    : first{x < y ? x : y}
    , second{y < x ? x : y}
    {}

    unsigned first;
    unsigned second;
};

bool operator<(BorderId const& lhs, BorderId const& rhs)
{
    return std::tie(lhs.first, lhs.second) < std::tie(rhs.first, rhs.second);
}

#ifdef notdef
std::ostream& operator<<(std::ostream& os, BorderId const& borderId)
{
    return os << "id=<" << borderId.first << ',' << borderId.second << '>';
}
#endif

unsigned stringId(std::string_view const& s)
{
    unsigned id{0};
    for (char ch : s) {
        id *= 2;
        if ('#' == ch) {
            ++id;
        }
    }
    return id;
}

std::array<std::string, 4> borders(std::vector<std::string> const& maze)
{
    std::string top{maze.front()};
    std::string bottom{maze.back()};
    std::string left{};
    std::string right{};
    for (auto const& row : maze) {
        left.push_back(row.front());
        right.push_back(row.back());
    }
    return {top, right, bottom, left};
}

struct Tile
{
    static std::array<BorderId, 4> borderIds(
        std::vector<std::string> const& maze);

    Tile(std::vector<std::string> const& maze);

    std::vector<std::string> const maze;
    // Top, right, bottom, left.
    std::array<BorderId, 4> const borders;
};

std::array<BorderId, 4> Tile::borderIds(std::vector<std::string> const& maze)
{
    auto const brdrs{::borders(maze)};
    auto flippedBrdrs{brdrs};
    for (auto& brdr : flippedBrdrs) {
        std::reverse(brdr.begin(), brdr.end());
    }
    return {
        BorderId{stringId(brdrs[0]), stringId(flippedBrdrs[0])},
        BorderId{stringId(brdrs[1]), stringId(flippedBrdrs[1])},
        BorderId{stringId(brdrs[2]), stringId(flippedBrdrs[2])},
        BorderId{stringId(brdrs[3]), stringId(flippedBrdrs[3])},
    };
}

Tile::Tile(std::vector<std::string> const& maze_)
: maze(maze_)
, borders(borderIds(maze_))
{}

} // namespace

int main()
{
    // A map from border ID to tile IDs.
    std::map<BorderId, std::vector<int>> borderTile{};

    std::unordered_map<int, Tile> tiles{};
    for (std::string line{}; std::getline(std::cin, line);) {
        assert("Tile " == line.substr(0, 5));
        int const tileId{std::stoi(line.substr(5))};
        std::vector<std::string> maze{};
        for (std::string row{}; std::getline(std::cin, row);) {
            if (row.empty()) {
                break;
            }
            maze.push_back(std::move(row));
        }
        Tile tile{maze};
        for (BorderId const& borderId : tile.borders) {
            borderTile[borderId].push_back(tileId);
        }
        tiles.emplace(tileId, std::move(maze));
    }

    std::int64_t product{1};
    for (auto const& [tileId, tile] : tiles) {
        auto const multiple{std::count_if(
            tile.borders.begin(),
            tile.borders.end(),
            [&borderTile](BorderId const& id)
            {
                return 1 != borderTile[id].size();
            })};

        // Inner tiles have three or four multiply occurring border IDs.
        if (multiple < 3) {
            product *= tileId;
        }
    }
    std::cout << "product=" << product << '\n';
    return 0;
}
