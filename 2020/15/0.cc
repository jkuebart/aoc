#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

namespace
{

int Main(std::vector<std::string_view>&& args)
{
    std::unordered_map<unsigned, unsigned> numbers{};
    unsigned turn{0};
    unsigned number{};
    unsigned nextNumber{};
    while (std::cin >> number) {
        if (0 == numbers.count(number)) {
            nextNumber = 0;
        } else {
            nextNumber = turn - numbers[number];
        }
        numbers[number] = turn;
        ++turn;
        char dummy{};
        std::cin >> dummy;
    }
    auto const maxTurns{std::stoul(std::string{args[1]})};
    while (turn < maxTurns) {
        number = nextNumber;
        if (0 == numbers.count(number)) {
            nextNumber = 0;
        } else {
            nextNumber = turn - numbers[number];
        }
        numbers[number] = turn;
        ++turn;
    }
    std::cout << number << '\n';
    return 0;
}

} // namespace

int main(int argc, char* argv[])
{
    return Main(std::vector<std::string_view>{argv, argv + argc});
}
