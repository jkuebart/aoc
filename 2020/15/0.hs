import Prelude hiding (lookup)
import Data.HashMap.Strict (fromList, insert, lookup)
import Data.Maybe (fromMaybe)

-- numbers = [0, 3, 6]
numbers = [6, 19, 0, 5, 7, 13, 1]
-- maxRounds = 2020
maxRounds = 30000000

-- Take a number, the round and the map of turns. Result is the next number
-- spoken.
nextNumber n round turns =
    fromMaybe 0 $ (\x -> round - x) <$> lookup n turns

-- Take the current round, the current number and turns. Result is the last
-- spoken number.
playTurns round n turns =
    if maxRounds == round then
        n
    else
        playTurns (1 + round) (nextNumber n round turns) (insert n round turns)

debugPlayTurns round n turns =
    if maxRounds == round then
        [(round, n)]
    else
        (round, n) : (debugPlayTurns (1 + round) (nextNumber n round turns) (insert n round turns))

main = print $ playTurns (1 + length numbers) 0 (fromList $ zip numbers [1..])
