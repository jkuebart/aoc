#include <algorithm>
#include <cassert>
#include <iostream>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace
{

struct Field
{
    constexpr bool has(int i) const;

    std::string const name;
    int const from0;
    int const to0;
    int const from1;
    int const to1;
};

constexpr bool Field::has(int i) const
{
    return (from0 <= i && i <= to0) || (from1 <= i && i <= to1);
}

using Ticket = std::vector<int>;

Ticket readCsv(std::string const& line)
{
    Ticket res{};
    std::string::size_type pos{0};
    for (;;) {
        auto const comma{line.find(',', pos)};
        if (std::string::npos == comma) {
            break;
        }
        res.push_back(std::stoi(line.substr(pos)));
        pos = 1 + comma;
    }
    res.push_back(std::stoi(line.substr(pos)));
    return res;
}

} // namespace

int main()
{
    std::vector<Field> fields{};
    for (std::string line{}; std::getline(std::cin, line);) {
        auto const colon{line.find(": ")};
        if (std::string::npos == colon) {
            break;
        }
        auto const from0{std::stoi(line.substr(2 + colon))};

        auto const dash0{line.find('-', colon)};
        assert(std::string::npos != dash0);
        auto const to0{std::stoi(line.substr(1 + dash0))};

        auto const or_{line.find(" or ", 1 + dash0)};
        assert(std::string::npos != or_);
        auto const from1{std::stoi(line.substr(4 + or_))};

        auto const dash1{line.find('-', 4 + or_)};
        assert(std::string::npos != dash1);
        auto const to1{std::stoi(line.substr(1 + dash1))};

        fields.push_back({line.substr(0, colon), from0, to0, from1, to1});
    }

    std::string dummy{};
    std::getline(std::cin, dummy);
    assert("your ticket:" == dummy);

    std::getline(std::cin, dummy);
    auto const ticket{readCsv(dummy)};
    assert(fields.size() <= ticket.size());

    std::getline(std::cin, dummy);
    assert("" == dummy);
    std::getline(std::cin, dummy);
    assert("nearby tickets:" == dummy);
    int invalid{0};
    std::vector<Ticket> tickets{};
    for (std::string line{}; std::getline(std::cin, line);) {
        auto const nearby{readCsv(line)};
        bool valid{true};
        for (auto const& v : nearby) {
            if (std::none_of(
                    fields.begin(),
                    fields.end(),
                    [v](auto const& field)
                    {
                        return field.has(v);
                    }))
            {
                invalid += v;
                valid = false;
            }
        }
        if (valid) {
            tickets.push_back(nearby);
        }
    }
    std::cout << "error rate: " << invalid << '\n';

    std::vector<std::unordered_set<unsigned>> candidateColumns{};
    candidateColumns.reserve(fields.size());
    for (auto const& field : fields) {
        std::unordered_set<unsigned> columns{};
        for (unsigned col{0}; col != ticket.size(); ++col) {
            if (std::all_of(
                    tickets.begin(),
                    tickets.end(),
                    [&col, &field](auto const& tckt)
                    {
                        return field.has(tckt.at(col));
                    }))
            {
                columns.insert(col);
            }
        }
        candidateColumns.push_back(std::move(columns));
    }

    // Find a possible assignment by elimination.
    std::unordered_map<unsigned, std::string> assignment{};
    while (assignment.size() != fields.size()) {
        for (unsigned field{0}; field != candidateColumns.size(); ++field) {
            // Assign fields with a single candidate column to that column.
            if (1 == candidateColumns[field].size()) {
                auto const column{*candidateColumns[field].begin()};
                assignment.emplace(column, fields[field].name);
                for (auto& candidate : candidateColumns) {
                    candidate.erase(column);
                }
            }
        }
    }

    std::int64_t product{1};
    for (auto const& ass : assignment) {
        if ("departure" == ass.second.substr(0, 9)) {
            product *= ticket.at(ass.first);
        }
    }
    std::cout << "product=" << product << '\n';

    return 0;
}
