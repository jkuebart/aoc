#include <iostream>
#include <map>
#include <set>
#include <string>
#include <string_view>
#include <variant>
#include <vector>

namespace
{

template<typename... Ts>
struct overloaded : Ts...
{
    using Ts::operator()...;
};
template<typename... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

using Sequence = std::vector<unsigned>;
using Alternatives = std::vector<Sequence>;
using Rule = std::variant<char, Alternatives>;

std::istream& operator>>(std::istream& is, Rule& rule)
{
    std::string line{};
    std::getline(is, line);
    auto const quote{line.find('"')};
    if (std::string::npos != quote) {
        rule = line[1 + quote];
    } else {
        Alternatives alts{};
        std::string::size_type pos{0};
        while (line.size() != pos) {
            Sequence seq{};
            auto const bar{line.find('|', pos)};
            while (line.size() != pos && pos < bar) {
                auto const space{line.find(' ', pos)};
                if (pos != space) {
                    seq.push_back(
                        static_cast<unsigned>(std::stoi(line.substr(pos))));
                }
                pos = 1 + std::min(line.size() - 1, space);
            }
            alts.push_back(std::move(seq));
            pos = 1 + std::min(line.size() - 1, bar);
        }
        rule = alts;
    }
    return is;
}

class Parser
{
public:
    Parser& update(unsigned idx, Rule const& rule);
    std::set<std::string_view> parse(
        unsigned idx,
        std::string_view const& input) const;

    friend std::istream& operator>>(std::istream& is, Parser& parser);

private:
    std::map<unsigned, Rule> mRules;
};

Parser& Parser::update(unsigned const idx, Rule const& rule)
{
    mRules[idx] = rule;
    return *this;
}

std::set<std::string_view> Parser::parse(
    unsigned const idx,
    std::string_view const& input) const
{
    if (input.empty()) {
        return {};
    }
    return std::visit(
        overloaded{
            [&input](char const c)
            {
                return c == input[0]
                    ? std::set<std::string_view>{input.substr(1)}
                    : std::set<std::string_view>{};
            },

            [&input, this](Alternatives const& alts)
            {
                std::set<std::string_view> results{};
                for (Sequence const& seq : alts) {
                    std::set<std::string_view> remainders{{input}};
                    for (unsigned const r : seq) {
                        std::set<std::string_view> newRems{};
                        for (std::string_view const& rem : remainders) {
                            auto const rems{parse(r, rem)};
                            newRems.insert(rems.begin(), rems.end());
                        }
                        remainders = std::move(newRems);
                    }
                    results.insert(remainders.begin(), remainders.end());
                }
                return results;
            },
        },
        mRules.at(idx));
}

std::istream& operator>>(std::istream& is, Parser& parser)
{
    unsigned idx{};
    while (is >> idx) {
        char dummy{};
        is >> dummy >> parser.mRules[idx];
    }
    is.clear();
    return is;
}

} // namespace

int main()
{
    Parser parser;
    std::cin >> parser;
    Parser parserUpdated{parser};
    parserUpdated.update(8, Alternatives{{42}, {42, 8}});
    parserUpdated.update(11, Alternatives{{42, 31}, {42, 11, 31}});
    unsigned valid{0};
    unsigned validUpdated{0};
    for (std::string line{}; std::getline(std::cin, line);) {
        for (auto const& rem : parser.parse(0, line)) {
            if (rem.empty()) {
                ++valid;
            }
        }
        for (auto const& rem : parserUpdated.parse(0, line)) {
            if (rem.empty()) {
                ++validUpdated;
            }
        }
    }
    std::cout << "valid=" << valid << " validUpdated=" << validUpdated << '\n';
    return 0;
}
