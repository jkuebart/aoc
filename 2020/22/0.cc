#include "streamer/stream.h"

#include <algorithm>
#include <cassert>
#include <deque>
#include <iostream>
#include <string>
#include <vector>

using streamer::stream;

using Game = std::vector<std::deque<unsigned>>;

int main()
{
    Game players{};
    for (std::string line{}; std::getline(std::cin, line);) {
        assert("Player " == line.substr(0, 7));

        std::deque<unsigned> deck{};
        for (std::string card{}; std::getline(std::cin, card);) {
            if (card.empty()) {
                break;
            }
            deck.push_back(static_cast<unsigned>(std::stoi(card)));
        }
        players.push_back(std::move(deck));
    }
    assert(2 == players.size());

    while (!players[0].empty() && !players[1].empty()) {
        if (players[0][0] < players[1][0]) {
            std::rotate(
                players[1].begin(),
                1 + players[1].begin(),
                players[1].end());
            players[1].push_back(players[0][0]);
            players[0].pop_front();
        } else {
            std::rotate(
                players[0].begin(),
                1 + players[0].begin(),
                players[0].end());
            players[0].push_back(players[1][0]);
            players[1].pop_front();
        }
    }

    unsigned score{0};
    for (auto const& player : players) {
        for (unsigned pos{0}; pos != player.size(); ++pos) {
            score += (player.size() - pos) * player[pos];
        }
    }
    std::cout << "score=" << score << '\n';
    return 0;
}
