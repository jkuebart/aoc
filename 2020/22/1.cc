#include "streamer/stream.h"

#include <algorithm>
#include <cassert>
#include <deque>
#include <iostream>
#include <set>
#include <string>
#include <vector>

using streamer::stream;

using Game = std::vector<std::deque<unsigned>>;

namespace
{

Game combat(Game game)
{
    std::set<Game> played{};
    assert(2 == game.size());
    while (!game[0].empty() && !game[1].empty()) {
        // Played this before: Player 1 wins.
        if (0 != played.count(game)) {
            game[1].clear();
            break;
        }
        played.insert(game);

        unsigned winner{};
        if (game[0][0] < game[0].size() && game[1][0] < game[1].size()) {
            Game subGame{
                {1 + game[0].begin(), game[0][0] + 1 + game[0].begin()},
                {1 + game[1].begin(), game[1][0] + 1 + game[1].begin()},
            };
            subGame = combat(subGame);
            if (subGame[0].empty()) {
                assert(!subGame[1].empty());
                winner = 1;
            } else {
                winner = 0;
            }
        } else if (game[0][0] < game[1][0]) {
            winner = 1;
        } else {
            winner = 0;
        }

        std::rotate(
            game[winner].begin(),
            1 + game[winner].begin(),
            game[winner].end());
        game[winner].push_back(game[1 - winner][0]);
        game[1 - winner].pop_front();
    }

    return game;
}

} // namespace

int main()
{
    Game players{};
    for (std::string line{}; std::getline(std::cin, line);) {
        assert("Player " == line.substr(0, 7));

        std::deque<unsigned> deck{};
        for (std::string card{}; std::getline(std::cin, card);) {
            if (card.empty()) {
                break;
            }
            deck.push_back(static_cast<unsigned>(std::stoi(card)));
        }
        players.push_back(std::move(deck));
    }
    players = combat(players);

    unsigned score{0};
    for (auto const& player : players) {
        for (unsigned pos{0}; pos != player.size(); ++pos) {
            score += (player.size() - pos) * player[pos];
        }
    }
    std::cout << "score=" << score << '\n';
    return 0;
}
