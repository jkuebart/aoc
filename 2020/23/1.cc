#include "streamer/stream.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <iterator>
#include <set>
#include <string>
#include <string_view>
#include <vector>

using streamer::stream;
using streamer::streamFromTo;

namespace
{

/// A circular list which provides direct lookup of its elements.
template<typename T>
class clist
{
    struct node
    {
        friend bool operator<(node const& lhs, node const& rhs)
        {
            return lhs.value < rhs.value;
        }

        std::add_const_t<T> value;
        node* next;
    };

public:
    using size_type = typename std::set<node>::size_type;

    class iterator
    {
    public:
        using difference_type = std::ptrdiff_t;
        using value_type = T const;
        using pointer = T const*;
        using reference = T const&;
        using iterator_category = std::forward_iterator_tag;

        constexpr iterator() noexcept : mPos{nullptr} {}

        explicit constexpr iterator(node* const pos) noexcept : mPos{pos} {}

        iterator& operator++() noexcept
        {
            assert(mPos);
            mPos = mPos->next;
            return *this;
        }

        iterator operator++(int) noexcept
        {
            iterator res{*this};
            ++*this;
            return res;
        }

        /// I know it's O(n) and not O(1). Sue me.
        iterator& operator+=(difference_type distance) noexcept
        {
            for (; 0 != distance; --distance) {
                operator++();
            }
            return *this;
        }

        T const* operator->() const noexcept
        {
            assert(mPos);
            return &mPos->value;
        }

        T const& operator*() const noexcept
        {
            return *operator->();
        }

        friend constexpr bool operator==(
            iterator const& lhs,
            iterator const& rhs) noexcept
        {
            return lhs.mPos == rhs.mPos;
        }

        friend constexpr bool operator!=(
            iterator const& lhs,
            iterator const& rhs) noexcept
        {
            return !(lhs == rhs);
        }

        /// I know it's O(n) and not O(1). Sue me.
        friend constexpr iterator operator+(
            iterator it,
            difference_type const distance) noexcept
        {
            return it += distance;
        }

        /// I know it's O(n) and not O(1). Sue me.
        friend constexpr iterator operator+(
            difference_type const distance,
            iterator it) noexcept
        {
            return it += distance;
        }

    private:
        friend class clist;
        node* mPos;
    };

    clist() : mNodes{}, mLast{nullptr} {}

    constexpr bool empty() const
    {
        return !mLast;
    }

    size_type size() const
    {
        return mNodes.size();
    }

    constexpr T& front()
    {
        assert(!empty());
        return *mLast->next->value;
    }

    constexpr T& back()
    {
        assert(!empty());
        return *mLast->value;
    }

    constexpr iterator begin()
    {
        assert(!empty());
        return iterator{mLast->next};
    }

    iterator find(T const& t)
    {
        auto const it{mNodes.find({t, nullptr})};
        return mNodes.end() == it ? iterator{}
                                  : iterator{const_cast<node*>(&*it)};
    }

    void push_back(T const& t)
    {
        if (mLast) {
            auto const it{mNodes.insert({t, mLast->next})};
            assert(it.second);
            mLast->next = const_cast<node*>(&*it.first);
            mLast = mLast->next;
        } else {
            auto const it{mNodes.insert({t, nullptr})};
            assert(it.second);
            mLast = const_cast<node*>(&*it.first);
            mLast->next = mLast;
        }
    }

    /// Splice (first, last] after pos.
    void splice_after(iterator pos, iterator first, iterator last)
    {
        using std::swap;
        swap(pos.mPos->next, first.mPos->next);
        swap(first.mPos->next, last.mPos->next);
    }

private:
    std::set<node> mNodes{};
    node* mLast{};
};

int Main(std::vector<std::string_view>&& args)
{
    clist<unsigned> cups{};

    char c{};
    while (std::cin >> c) {
        cups.push_back(static_cast<unsigned>(c - '1'));
    }

    auto const number{static_cast<unsigned>(std::stoi(std::string{args[2]}))};
    while (cups.size() < number) {
        cups.push_back(static_cast<unsigned>(cups.size()));
    }

    auto current{cups.begin()};
    for (auto move{std::stoul(std::string{args[1]})}; 0 != move; --move) {
        // std::cout << "current=" << *current << "  cups=" <<
        // streamFromTo(current, current + 8) << '\n';
        auto destination{(number + *current - 1) % number};
        auto end{4 + current};
        while (end != std::find(1 + current, end, destination)) {
            // std::cout << "  destination=" << destination << '\n';
            destination = (number + destination - 1) % number;
        }
        // std::cout << "  destination=" << destination << '\n';
        cups.splice_after(cups.find(destination), current, 3 + current);
        ++current;
    }
    // std::cout << "cups=" << stream(cups) << '\n';
    auto zero{cups.find(0)};
    std::cout << "cups=" << streamFromTo(zero, 8 + zero) << ' ' << *(8 + zero)
              << " product="
              << static_cast<std::uint64_t>(1 + *(1 + zero)) * (1 + *(2 + zero))
              << '\n';
    return 0;
}

} // namespace

int main(int argc, char* argv[])
{
    return Main(std::vector<std::string_view>{argv, argv + argc});
}
