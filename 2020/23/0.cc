#include "streamer/stream.h"

#include <algorithm>
#include <iostream>
#include <string>
#include <string_view>
#include <vector>

using streamer::stream;
using streamer::streamFromTo;

namespace
{

int Main(std::vector<std::string_view>&& args)
{
    std::vector<unsigned> cups{};
    char c{};
    while (std::cin >> c) {
        cups.push_back(static_cast<unsigned>(c - '1'));
    }
    for (int move{std::stoi(std::string{args[1]})}; 0 != move; --move) {
        // std::cout << "cups=" << stream(cups) << '\n';
        auto destination{(cups.size() + cups.front() - 1) % cups.size()};
        auto pos{std::find(4 + cups.begin(), cups.end(), destination)};
        while (cups.end() == pos) {
            // std::cout << "  destination=" << destination << " pos=" << (pos -
            // cups.begin()) << '\n';
            destination = (cups.size() + destination - 1) % cups.size();
            pos = std::find(4 + cups.begin(), cups.end(), destination);
        }
        // std::cout << "  destination=" << destination << " pos=" << (pos -
        // cups.begin()) << '\n';

        // 0 1 2 3 4 5 6 7 8
        // ^ (   )   !
        std::rotate(1 + cups.begin(), 4 + cups.begin(), 1 + pos);
        // 0 4 5 1 2 3 6 7 8
        // ^   ! (   )
        std::rotate(cups.begin(), 1 + cups.begin(), cups.end());
        // 4 5 1 2 3 6 7 8 0
        //   ! (   )       ^
    }
    // std::cout << "cups=" << stream(cups) << '\n';
    auto const zero{std::find(cups.begin(), cups.end(), 0)};
    std::cout << "cups=" << streamFromTo(zero, std::min(10 + zero, cups.end()))
              << '\n';
    return 0;
}

} // namespace

int main(int argc, char* argv[])
{
    return Main(std::vector<std::string_view>{argv, argv + argc});
}
