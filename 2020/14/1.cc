#include <algorithm>
#include <bitset>
#include <cassert>
#include <functional>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

namespace
{

constexpr std::uint64_t kMask{(std::uint64_t{1} << 36) - 1};

struct Set
{
    std::uint64_t address;
    std::uint64_t mask;
    std::uint64_t value;
};

#ifdef notdef
std::ostream& operator<<(std::ostream& os, Set const& set)
{
    return os << std::hex << "{address=" << set.address << " mask=" << set.mask
              << " value=" << set.value << std::dec << '}';
}
#endif

class RangeMemory
{
public:
    RangeMemory& set(Set set)
    {
        std::vector<Set> const oldSets{std::move(mSets)};
        mSets.clear();

        set.address &= ~set.mask;
        for (auto oldSet : oldSets) {
            for (std::uint64_t mask{1}; mask < kMask; mask *= 2) {
                if (mask & set.mask) {
                    // If the new setting overwrites the old one, skip it.
                } else if (mask & oldSet.mask) {
                    // The new setting misses (part of) the old one, keep
                    // remainder(s).
                    oldSet.address |= mask & set.address;
                    oldSet.mask &= ~mask;
                    mSets.push_back(
                        {mask ^ oldSet.address, oldSet.mask, oldSet.value});
                } else if ((mask & oldSet.address) != (mask & set.address)) {
                    // The new setting misses the old one completely. Keep
                    // both.
                    mSets.push_back(oldSet);
                    break;
                }
            }
        }

        mSets.push_back(set);
        return *this;
    }

    std::uint64_t sum() const
    {
        return std::accumulate(
            mSets.begin(),
            mSets.end(),
            std::uint64_t{0},
            [](std::uint64_t const acc, auto const& set)
            {
                auto const count{std::bitset<64>{set.mask}.count()};
                return acc + (std::uint64_t{1} << count) * set.value;
            });
    }

#ifdef notdef
    friend std::ostream& operator<<(
        std::ostream& os,
        RangeMemory const& memory);
#endif

private:
    std::vector<Set> mSets;
};

#ifdef notdef
std::ostream& operator<<(std::ostream& os, RangeMemory const& memory)
{
    os << "RangeMemory{\n";
    for (auto const& set : memory.mSets) {
        os << "  " << set << '\n';
    }
    return os << "}\n";
}
#endif

} // namespace

int main()
{
    std::uint64_t changeMask{};
    std::uint64_t valueMask{};
    RangeMemory rangeMemory;
    for (std::string line{}; std::getline(std::cin, line);) {
        if ("mask = " == line.substr(0, 7)) {
            changeMask = 0;
            valueMask = 0;
            for (auto const& ch : line.substr(7)) {
                changeMask *= 2;
                valueMask *= 2;
                if ('X' == ch) {
                    changeMask += 1;
                } else if ('1' == ch) {
                    valueMask += 1;
                } else {
                    assert('0' == ch);
                }
            }
        } else {
            assert("mem[" == line.substr(0, 4));
            auto const index{std::stoul(line.substr(4))};
            auto const value{std::stoul(line.substr(line.rfind(' ')))};
            rangeMemory.set({index | valueMask, changeMask, value});
        }
    }
    std::cout << "sum=" << rangeMemory.sum() << '\n';
    return 0;
}
