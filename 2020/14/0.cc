#include <cassert>
#include <functional>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

int main()
{
    std::uint64_t changeMask{};
    std::uint64_t valueMask{};
    std::vector<std::uint64_t> mem{};
    for (std::string line{}; std::getline(std::cin, line);) {
        if ("mask = " == line.substr(0, 7)) {
            changeMask = 0;
            valueMask = 0;
            for (auto const& ch : line.substr(7)) {
                changeMask *= 2;
                valueMask *= 2;
                if ('X' == ch) {
                    changeMask += 1;
                } else if ('1' == ch) {
                    valueMask += 1;
                } else {
                    assert('0' == ch);
                }
            }
        } else {
            assert("mem[" == line.substr(0, 4));
            auto const index{std::stoul(line.substr(4))};
            auto const value{std::stoul(line.substr(line.rfind(' ')))};
            if (mem.size() <= index) {
                mem.resize(1 + index);
            }
            mem[index] = (value & changeMask) + valueMask;
        }
    }
    std::cout << std::accumulate(
                     mem.begin(),
                     mem.end(),
                     std::size_t{0},
                     std::plus<std::uint64_t>{})
              << '\n';
    return 0;
}
