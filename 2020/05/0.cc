#include <iostream>
#include <set>
#include <string>

int main()
{
    unsigned maxSeatId{0};
    std::set<unsigned> available{};
    for (unsigned seatId{1}; seatId < 0x400; ++seatId) {
        available.insert(seatId);
    }
    for (std::string line{}; std::getline(std::cin, line);) {
        unsigned seatId{0};
        for (char const c : line) {
            seatId = 2 * seatId + ('B' == c || 'R' == c ? 1 : 0);
        }
        if (maxSeatId < seatId) {
            maxSeatId = seatId;
        }
        available.erase(seatId);
    }
    std::cout << maxSeatId << '\n';

    // Find the first "lone" seat ID.
    for (auto const seatId : available) {
        if (0 == available.count(seatId - 1) &&
            0 == available.count(seatId + 1)) {
            std::cout << seatId << '\n';
        }
    }
    return 0;
}
