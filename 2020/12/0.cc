#include "geo/direction.h"
#include "geo/point.h"

#include <cassert>
#include <iostream>
#include <string>

using Direction = geo::Direction<2>;
using Point = geo::Point<Direction>;

namespace
{

constexpr Direction kEast{1, 0};
constexpr Direction kNorth{0, 1};
constexpr Direction kSouth{0, -1};
constexpr Direction kWest{-1, 0};

} // namespace

int main()
{
    Direction direction{kEast};
    Point ship0{};

    Direction waypoint{kNorth + 10 * kEast};
    Point ship1{};

    for (std::string line{}; std::getline(std::cin, line);) {
        char action{line[0]};
        int operand{std::stoi(line.substr(1))};

        switch (action) {
        case 'E':
            ship0 = ship0 + operand * kEast;
            waypoint = waypoint + operand * kEast;
            break;
        case 'F':
            ship0 = ship0 + operand * direction;
            ship1 = ship1 + operand * waypoint;
            break;
        case 'L':
            assert(0 == operand % 90);
            for (int i{operand / 90}; i != 0; --i) {
                direction.rotateLeft();
                waypoint.rotateLeft();
            }
            break;
        case 'N':
            ship0 = ship0 + operand * kNorth;
            waypoint = waypoint + operand * kNorth;
            break;
        case 'R':
            assert(0 == operand % 90);
            for (int i{operand / 90}; i != 0; --i) {
                direction.rotateRight();
                waypoint.rotateRight();
            }
            break;
        case 'S':
            ship0 = ship0 + operand * kSouth;
            waypoint = waypoint + operand * kSouth;
            break;
        case 'W':
            ship0 = ship0 + operand * kWest;
            waypoint = waypoint + operand * kWest;
            break;
        }
    }

    std::cout << "distance0=" << rectilinearDistance(ship0, {}) << '\n'
              << "distance1=" << rectilinearDistance(ship1, {}) << '\n';

    return 0;
}
