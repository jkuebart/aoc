#include <algorithm>
#include <iostream>
#include <string>
#include <unordered_set>
#include <vector>

int main()
{
    unsigned anyoneCount{0};
    unsigned everyoneCount{0};
    for (;;) {
        std::unordered_set<char> anyone{};
        std::vector<char> everyone{};
        for (std::string line{}; std::getline(std::cin, line);) {
            if (line.empty()) {
                break;
            }
            std::unordered_set<char> const set{line.begin(), line.end()};
            if (anyone.empty()) {
                everyone.insert(everyone.end(), set.begin(), set.end());
            }
            everyone.erase(
                std::remove_if(
                    everyone.begin(),
                    everyone.end(),
                    [&set](char const c)
                    {
                        return 0 == set.count(c);
                    }),
                everyone.end());
            anyone.insert(set.begin(), set.end());
        }
        if (anyone.empty()) {
            break;
        }
        anyoneCount += anyone.size();
        everyoneCount += everyone.size();
    }
    std::cout << "anyoneCount=" << anyoneCount << '\n';
    std::cout << "everyoneCount=" << everyoneCount << '\n';
    return 0;
}
