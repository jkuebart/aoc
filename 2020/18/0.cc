#include <cassert>
#include <iostream>
#include <string>
#include <tuple>

namespace
{

std::pair<std::int64_t, std::string::size_type> expr(
    std::string const& line,
    std::string::size_type pos);

std::pair<std::int64_t, std::string::size_type> num(
    std::string const& line,
    std::string::size_type pos)
{
    while (pos != line.size()) {
        char const ch{line[pos]};
        switch (ch) {
        case ' ':
            ++pos;
            break;
        case '(':
        {
            std::int64_t result{};
            std::tie(result, pos) = expr(line, 1 + pos);
            assert(pos < line.size());
            assert(')' == line[pos]);
            return {result, 1 + pos};
        }
        default:
            assert('0' <= ch && ch <= '9');
            return {ch - '0', 1 + pos};
        }
    }
    assert(false);
    return {0, pos};
}

std::pair<std::int64_t, std::string::size_type> expr(
    std::string const& line,
    std::string::size_type pos)
{
    std::int64_t value{};
    std::tie(value, pos) = num(line, pos);
    while (pos != line.size()) {
        std::int64_t op{};
        switch (line[pos]) {
        case ' ':
            ++pos;
            break;
        case ')':
            return {value, pos};
        case '*':
            std::tie(op, pos) = num(line, 1 + pos);
            value *= op;
            break;
        case '+':
            std::tie(op, pos) = num(line, 1 + pos);
            value += op;
            break;
        default:
            assert(false);
        }
    }
    return {value, pos};
}

} // namespace

int main()
{
    std::int64_t result{0};
    for (std::string line{}; std::getline(std::cin, line);) {
        std::int64_t value{};
        std::string::size_type pos{};
        std::tie(value, pos) = expr(line, 0);
        assert(line.size() == pos);
        std::cout << line << " = " << value << '\n';
        result += value;
    }
    std::cout << "result=" << result << '\n';
    return 0;
}
