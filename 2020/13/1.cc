#include <functional>
#include <iostream>
#include <map>
#include <string>
#include <vector>

int main()
{
    unsigned start{};
    std::cin >> start;
    std::map<unsigned, unsigned, std::greater<unsigned>> times{};
    unsigned offset{0};
    for (std::string dummy{}; std::getline(std::cin, dummy, ',');) {
        if ("x" != dummy) {
            auto const schedule{static_cast<unsigned>(std::stoi(dummy))};
            times.emplace(schedule, (schedule - offset % schedule) % schedule);
        }
        ++offset;
    }

    // Invariant: m + k * n === desired modulus for all k.
    std::size_t m{0};
    std::size_t n{1};
    for (auto const& pair : times) {
        if (1 == n) {
            n = pair.first;
            m = pair.second;
        } else {
            while (pair.second != m % pair.first) {
                m += n;
            }
            n *= pair.first;
        }
    }
    std::cout << m << '\n';

    return 0;
}
