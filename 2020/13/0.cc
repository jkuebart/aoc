#include <iostream>
#include <string>
#include <vector>

int main()
{
    unsigned start{};
    std::cin >> start;
    std::vector<unsigned> times{};
    for (std::string dummy{}; std::getline(std::cin, dummy, ',');) {
        if ("x" != dummy) {
            times.push_back(static_cast<unsigned>(std::stoi(dummy)));
        }
    }
    unsigned id{0};
    unsigned minWait{0};
    for (unsigned schedule : times) {
        unsigned const wait{schedule - start % schedule};
        if (0 == id || wait < minWait) {
            id = schedule;
            minWait = wait;
        }
    }
    std::cout << "id=" << id << " minWait=" << minWait << ' ' << id * minWait
              << '\n';
    return 0;
}
