#include "streamer/stream.h"

#include <algorithm>
#include <array>
#include <cassert>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

namespace
{

constexpr unsigned kMaxDifference{3};

/// Compute the number of ways that a run of adapters with a difference
/// of 1 can be combined.
///
/// combinations(length) returns
///     length
///         if length < 3
///     1 + combinations(length - 1) + combinations(length - 2)
///         otherwise
///
/// @param length Number of adapters with a difference of 1.
/// @return Number of ways to combine these adaptors.
unsigned combinations(unsigned length)
{
    std::array<unsigned, 2> paths{0, 1};
    while (2 <= length) {
        unsigned const next{1 + paths[0] + paths[1]};
        paths[0] = paths[1];
        paths[1] = next;
        --length;
    }
    return paths[length];
}

} // namespace

int main()
{
    std::vector<unsigned> adaptors{
        std::istream_iterator<unsigned>(std::cin),
        std::istream_iterator<unsigned>()};

    std::sort(adaptors.begin(), adaptors.end());
    adaptors.push_back(kMaxDifference + adaptors.back());

    std::array<unsigned, kMaxDifference> histogram{};
    unsigned last{0};
    unsigned runLength{0};
    std::uint64_t npaths{1};
    for (auto j : adaptors) {
        assert(last < j);
        assert(j - last <= kMaxDifference);
        ++histogram[j - last - 1];
        if (1 == j - last) {
            ++runLength;
        } else if (0 < runLength) {
            npaths *= combinations(runLength);
            runLength = 0;
        }
        last = j;
    }

    std::cout << "histogram=" << streamer::stream(histogram, " ", "{", "}")
              << '\n';
    std::cout << "npaths=" << npaths << '\n';

    return 0;
}
