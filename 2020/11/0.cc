#include "streamer/getlines.h"

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

namespace
{

unsigned
count(std::vector<std::string> const& seats, unsigned const x, unsigned const y)
{
    unsigned occupied{0};
    for (unsigned yy{y - 1}; yy != 2 + y; ++yy) {
        if (seats.size() <= yy) {
            continue;
        }
        for (unsigned xx{x - 1}; xx != 2 + x; ++xx) {
            if ((x != xx || y != yy) && xx < seats[yy].size() &&
                '#' == seats[yy][xx]) {
                ++occupied;
            }
        }
    }
    return occupied;
}

} // namespace

int main()
{
    std::vector<std::string> seats{streamer::getlines(std::cin)};

    for (std::vector<std::string> nextSeats{seats};; seats = nextSeats) {
        for (unsigned y{0}; y != seats.size(); ++y) {
            auto const& row{seats[y]};
            for (unsigned x{0}; x != row.size(); ++x) {
                auto const occupied{count(seats, x, y)};
                if ('L' == row[x] && 0 == occupied) {
                    nextSeats[y][x] = '#';
                } else if ('#' == row[x] && 4 <= occupied) {
                    nextSeats[y][x] = 'L';
                }
            }
        }
        if (seats == nextSeats) {
            break;
        }
    }
    unsigned occupied{0};
    for (auto const& row : seats) {
        occupied += std::count(row.begin(), row.end(), '#');
    }
    std::cout << "occupied=" << occupied << '\n';

    return 0;
}
