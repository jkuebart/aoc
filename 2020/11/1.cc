#include "geo/direction.h"
#include "geo/point.h"
#include "streamer/getlines.h"

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using Direction = geo::Direction<2, unsigned>;
using Point = geo::Point<Direction>;

namespace
{

unsigned
count(std::vector<std::string> const& seats, unsigned const x, unsigned const y)
{
    unsigned occupied{0};
    for (unsigned yy{0}; 3 != yy; ++yy) {
        for (unsigned xx{0}; 3 != xx; ++xx) {
            Direction const direction{xx - 1, yy - 1};
            if (Direction{} == direction) {
                continue;
            }
            for (Point pos{Point{x, y} + direction};
                 pos[1] < seats.size() && pos[0] < seats[pos[1]].size();
                 pos = pos + direction)
            {
                auto const& seat{seats[pos[1]][pos[0]]};
                if ('#' == seat) {
                    ++occupied;
                }
                if ('.' != seat) {
                    break;
                }
            }
        }
    }
    return occupied;
}

} // namespace

int main()
{
    std::vector<std::string> seats{streamer::getlines(std::cin)};

    for (std::vector<std::string> nextSeats{seats};; seats = nextSeats) {
        for (unsigned y{0}; y != seats.size(); ++y) {
            auto const& row{seats[y]};
            for (unsigned x{0}; x != row.size(); ++x) {
                auto const occupied{count(seats, x, y)};
                if ('L' == row[x] && 0 == occupied) {
                    nextSeats[y][x] = '#';
                } else if ('#' == row[x] && 5 <= occupied) {
                    nextSeats[y][x] = 'L';
                }
            }
        }
        if (seats == nextSeats) {
            break;
        }
    }
    unsigned occupied{0};
    for (auto const& row : seats) {
        occupied += std::count(row.begin(), row.end(), '#');
    }
    std::cout << "occupied=" << occupied << '\n';

    return 0;
}
