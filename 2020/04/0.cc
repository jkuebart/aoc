#include <algorithm>
#include <cctype>
#include <iostream>
#include <iterator>
#include <string>
#include <unordered_map>

namespace
{

char const* const kRequired[]{"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"};

char const* const
    kEyeColours[]{"amb", "blu", "brn", "gry", "grn", "hzl", "oth"};

class Passport
{
public:
    bool valid() const;
    bool validData() const;

    friend std::istream& operator>>(std::istream& is, Passport& passport);

private:
    bool between(std::string const& key, unsigned size, int low, int high)
        const;

    std::unordered_map<std::string, std::string> mFields;
};

bool Passport::valid() const
{
    return std::all_of(
        std::begin(kRequired),
        std::end(kRequired),
        [this](auto const& field)
        {
            return 0 != mFields.count(field);
        });
}

bool Passport::between(std::string const& key, unsigned size, int low, int high)
    const
{
    auto const& value{mFields.at(key)};

    return size == value.size() && low <= std::stoi(value) &&
        std::stoi(value) <= high;
}

bool Passport::validData() const
{
    if (!valid() || !between("byr", 4, 1920, 2002) ||
        !between("iyr", 4, 2010, 2020) || !between("eyr", 4, 2020, 2030))
    {
        return false;
    }

    auto const& hgt{mFields.at("hgt")};
    if (hgt.size() < 3) {
        return false;
    }
    if ("cm" == hgt.substr(hgt.size() - 2)) {
        if (std::stoi(hgt) < 150 || 193 < std::stoi(hgt)) {
            return false;
        }
    } else if ("in" == hgt.substr(hgt.size() - 2)) {
        if (std::stoi(hgt) < 59 || 76 < std::stoi(hgt)) {
            return false;
        }
    } else {
        return false;
    }

    auto const& hcl{mFields.at("hcl")};
    if (7 != hcl.size() || '#' != hcl[0] ||
        !std::all_of(
            1 + hcl.begin(),
            hcl.end(),
            [](char const chr)
            {
                return std::isdigit(chr) || ('a' <= chr && chr <= 'f');
            }))
    {
        return false;
    }

    if (!std::any_of(
            std::begin(kEyeColours),
            std::end(kEyeColours),
            [this](char const* colour)
            {
                return colour == mFields.at("ecl");
            }))
    {
        return false;
    }

    auto const& pid{mFields.at("pid")};
    if (9 != pid.size() ||
        !std::all_of(
            pid.begin(),
            pid.end(),
            [](char const chr)
            {
                return std::isdigit(chr);
            }))
    {
        return false;
    }
    return true;
}

std::istream& operator>>(std::istream& is, Passport& passport)
{
    passport.mFields.clear();
    for (std::string line{}; std::getline(is, line);) {
        if (line.empty()) {
            break;
        }

        std::string::size_type pos{0};
        while (pos < line.size()) {
            auto const space{std::min(line.size(), line.find(' ', pos))};
            auto const colon{line.find(':', pos)};
            passport.mFields.emplace(
                line.substr(pos, colon - pos),
                line.substr(1 + colon, space - colon - 1));
            pos = 1 + space;
        }
    }
    if (!passport.mFields.empty()) {
        is.clear();
    }
    return is;
}

} // namespace

int main()
{
    unsigned valid{0};
    unsigned validData{0};
    Passport passport{};
    while (std::cin >> passport) {
        if (passport.valid()) {
            ++valid;
        }
        if (passport.validData()) {
            ++validData;
        }
    }
    std::cout << "valid=" << valid << "\nvalidData=" << validData << '\n';
    return 0;
}
