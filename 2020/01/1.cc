#include <iostream>
#include <iterator>
#include <unordered_set>

constexpr int kTarget{2020};

int main()
{
    std::unordered_set<int> const report{
        std::istream_iterator<int>(std::cin),
        std::istream_iterator<int>()};
    for (auto const r : report) {
        for (auto const s : report) {
            if (r == s) {
                continue;
            }
            if (0 != report.count(kTarget - r - s)) {
                std::cout << (r * s * (kTarget - r - s)) << '\n';
                return 0;
            }
        }
    }
    return 1;
}
