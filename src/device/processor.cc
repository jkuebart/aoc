#include "device/processor.h"

namespace device
{
int op_add(int const lhs, int const rhs)
{
    return lhs + rhs;
}

int op_and(int const lhs, int const rhs)
{
    return lhs & rhs;
}

int op_assign(int const lhs, int)
{
    return lhs;
}

int op_eq(int const lhs, int const rhs)
{
    return lhs == rhs ? 1 : 0;
}

int op_gt(int const lhs, int const rhs)
{
    return lhs > rhs ? 1 : 0;
}

int op_mul(int const lhs, int const rhs)
{
    return lhs * rhs;
}

int op_or(int const lhs, int const rhs)
{
    return lhs | rhs;
}
} // namespace device
