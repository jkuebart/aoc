#include "intcode/processor.h"

#include "streamer/stream.h"

#include <algorithm>
#include <iterator>
#include <sstream>

using streamer::stream;

namespace intcode
{
Processor& Processor::run()
{
    for (;;) {
        int const opcode{static_cast<int>(get(mIp))};
        int const mode{opcode / 100};
        switch (opcode % 100) {
        case 1:
            set(get(3 + mIp), param(0, mode) + param(1, mode / 10), mode / 100);
            mIp += 4;
            break;
        case 2:
            set(get(3 + mIp), param(0, mode) * param(1, mode / 10), mode / 100);
            mIp += 4;
            break;
        case 3:
            if (mInput.empty()) {
                return *this;
            }
            set(get(1 + mIp), mInput.front(), mode);
            mInput.erase(mInput.begin());
            mIp += 2;
            break;
        case 4:
            mOutput.push_back(param(0, mode));
            mIp += 2;
            break;
        case 5:
            if (param(0, mode)) {
                mIp = static_cast<int>(param(1, mode / 10));
            } else {
                mIp += 3;
            }
            break;
        case 6:
            if (!param(0, mode)) {
                mIp = static_cast<int>(param(1, mode / 10));
            } else {
                mIp += 3;
            }
            break;
        case 7:
            set(get(3 + mIp), param(0, mode) < param(1, mode / 10), mode / 100);
            mIp += 4;
            break;
        case 8:
            set(get(3 + mIp),
                param(0, mode) == param(1, mode / 10),
                mode / 100);
            mIp += 4;
            break;
        case 9:
            mBase += param(0, mode);
            mIp += 2;
            break;
        case 99:
            return *this;
        default:
            std::cout << "Something went wrong: ip=" << mIp
                      << " opcode=" << opcode << '\n';
            return *this;
        }
    }
}

void Processor::set(
    std::intmax_t const i,
    std::intmax_t const v,
    int const mode)
{
    uintmax_t const ui{static_cast<uintmax_t>(mode % 10 ? mBase + i : i)};
    if (mMemory.size() <= ui) {
        mMemory.resize(1 + ui);
    }
    mMemory[ui] = v;
}

std::intmax_t Processor::get(std::intmax_t const i) const
{
    uintmax_t const ui{static_cast<uintmax_t>(i)};
    return ui < mMemory.size() ? mMemory[ui] : 0;
}

bool Processor::reading() const
{
    return 3 == get(mIp);
}

Processor& Processor::input(std::vector<std::intmax_t> input)
{
    mInput = std::move(input);
    return *this;
}

std::vector<std::intmax_t> Processor::output()
{
    std::vector<std::intmax_t> result{std::move(mOutput)};
    mOutput.clear();
    return result;
}

std::intmax_t Processor::param(std::intmax_t const i, int const mode) const
{
    std::intmax_t const operand{get(1 + i + mIp)};
    switch (mode % 10) {
    case 0:
        return get(operand);
    case 1:
        return operand;
    case 2:
        return get(mBase + operand);
    default:
        std::cerr << "Something went wrong: " << mIp << '\n';
        return 0;
    }
}

std::istream& operator>>(std::istream& is, Processor& rhs)
{
    rhs.mMemory.clear();
    rhs.mIp = 0;
    std::string line{};
    if (std::getline(is, line)) {
        std::replace(line.begin(), line.end(), ',', ' ');
        std::istringstream stringstream{line};
        std::copy(
            std::istream_iterator<std::intmax_t>(stringstream),
            std::istream_iterator<std::intmax_t>(),
            std::back_inserter(rhs.mMemory));
    }
    return is;
}

std::ostream& operator<<(std::ostream& os, Processor const& rhs)
{
    return os << stream(rhs.mMemory, ",");
}
} // namespace intcode
