#include "streamer/stream.h"

#include <string>

namespace streamer
{
std::istream& consume(std::istream& is, std::string_view const& str)
{
    if (!is || str.empty()) {
        return is;
    }

    std::string dummy(str.size(), 0);

    is.read(&dummy[0], static_cast<std::streamsize>(dummy.size()));
    if (dummy != str) {
        throw std::ios_base::failure(
            "Expected '" + std::string{str} + "', got '" + dummy + "'");
    }

    return is;
}
} // namespace streamer
