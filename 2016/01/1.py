#!/usr/bin/env python3

import sys

for directions in sys.stdin:
    x0 = 0
    x1 = 0
    d0 = 0
    d1 = 1

    visited = set()
    visited.add((x0, x1))
    for d in directions[:-1].split(", "):
        if "L" == d[0]:
            d0, d1 = -d1, d0
        elif "R" == d[0]:
            d0, d1 = d1, -d0

        for i in range(int(d[1:])):
            x0 += d0
            x1 += d1
            if (x0, x1) in visited:
                break
            visited.add((x0, x1))
        else:
            continue

        print("already visited", abs(x0) + abs(x1))
        break
