#include "streamer/stream.h"

#include <iostream>
#include <string>

using streamer::stream;

using position = std::pair<int, int>;

namespace
{
std::ostream& operator<<(std::ostream& os, position const& pos)
{
    return os << stream(pos, ", ", "(", ")");
}

position maybe_adjust(
    position const& pos,
    int const d0,
    int const d1,
    bool (&valid)(position const&))
{
    position const next{pos.first + d0, pos.second + d1};
    return valid(next) ? next : pos;
}

struct Keypad0
{
    static bool valid(position const& pos)
    {
        return std::abs(pos.first) <= 1 && std::abs(pos.second) <= 1;
    }

    Keypad0& adjust(int const d0, int const d1)
    {
        pos = maybe_adjust(pos, d0, d1, valid);
        return *this;
    }

    constexpr int num() const
    {
        return 5 + pos.first + 3 * pos.second;
    }

    friend std::ostream& operator<<(std::ostream& os, Keypad0 const& rhs)
    {
        return os << '@' << rhs.pos << '=' << rhs.num();
    }

    position pos{};
};

struct Keypad1
{
    static bool valid(position const& pos)
    {
        return std::abs(pos.first) + std::abs(pos.second) <= 2;
    }

    Keypad1& adjust(int const d0, int const d1)
    {
        pos = maybe_adjust(pos, d0, d1, valid);
        return *this;
    }

    constexpr int num() const
    {
        return 7 + pos.first + 2 * (2 * pos.second - pos.second / 2);
    }

    friend std::ostream& operator<<(std::ostream& os, Keypad1 const& rhs)
    {
        return os << '@' << rhs.pos << '=' << rhs.num();
    }

    position pos{-2, 0};
};
} // namespace

int main()
{
    Keypad0 kp0{};
    Keypad1 kp1{};

    for (std::string line{}; std::getline(std::cin, line);) {
        for (auto const d : line) {
            switch (d) {
            case 'D':
                kp0.adjust(0, 1);
                kp1.adjust(0, 1);
                break;
            case 'L':
                kp0.adjust(-1, 0);
                kp1.adjust(-1, 0);
                break;
            case 'R':
                kp0.adjust(1, 0);
                kp1.adjust(1, 0);
                break;
            case 'U':
                kp0.adjust(0, -1);
                kp1.adjust(0, -1);
                break;
            }
        }
        std::cout << "kp0=" << kp0 << " kp1=" << kp1 << '\n';
    }

    return 0;
}
