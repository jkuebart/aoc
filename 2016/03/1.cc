#include "triangle.h"

#include "rng/all.h"
#include "rng/countIf.h"
#include "streamer/stream.h"

#include <algorithm>
#include <functional>
#include <iostream>

using streamer::stream;
using triangle::Triangle;
using triangle::Triplet;

int main()
{
    using triplets = Triplet[3];

    std::ptrdiff_t count{0};
    triplets t;
    while (std::cin >> stream(t, "\n")) {
        // Transpose the read triplets.
        Triangle transposed[]{
            {t[0].ints[0], t[1].ints[0], t[2].ints[0]},
            {t[0].ints[1], t[1].ints[1], t[2].ints[1]},
            {t[0].ints[2], t[1].ints[2], t[2].ints[2]},
        };
#ifdef notdef
        std::cerr << stream(transposed, ",") << '\n';
#endif
        count +=
            rng::all(transposed) | rng::CountIf{std::mem_fn(&Triangle::valid)};
    }
    std::cout << count << '\n';

    return 0;
}
