#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "streamer/stream.h"

#include <algorithm>
#include <cassert>
#include <iostream>

namespace triangle
{
struct Triplet
{
    friend std::istream& operator>>(std::istream& is, Triplet& rhs)
    {
        return is >> streamer::stream(rhs.ints);
    }

    unsigned int ints[3]{};
};

struct Triangle
{
    Triangle() = default;

    explicit Triangle(unsigned int const (&sds)[3])
    : Triangle(sds[0], sds[1], sds[2])
    {}

    Triangle(
        unsigned int const s0,
        unsigned int const s1,
        unsigned int const s2)
    : sides{s0, s1, s2}
    {
        using std::begin;
        using std::end;
        std::sort(begin(this->sides), end(this->sides));
    }

    constexpr bool valid() const
    {
        assert(sides[0] <= sides[1]);
        assert(sides[1] <= sides[2]);
        return sides[2] < sides[0] + sides[1];
    }

#ifdef notdef
    friend std::ostream& operator<<(std::ostream& os, Triangle const& rhs)
    {
        return os << streamer::stream(rhs.sides, ",", "<", ">");
    }
#endif

    unsigned int sides[3]{};
};
} // namespace triangle

#endif // TRIANGLE_H
