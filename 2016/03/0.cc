#include "triangle.h"

#include <algorithm>
#include <iostream>
#include <iterator>

using namespace triangle;

int main()
{
    std::ptrdiff_t const count{std::count_if(
        std::istream_iterator<Triplet>(std::cin),
        std::istream_iterator<Triplet>(),
        [](Triplet const& trplt)
        {
            return Triangle{trplt.ints}.valid();
        })};
    std::cout << count << '\n';

    return 0;
}
