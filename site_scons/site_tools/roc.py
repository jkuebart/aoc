from SCons.Util import WhereIs

def generate(env):
    env["ROC"] = WhereIs("roc")

def exists(env):
    return WhereIs("roc")
