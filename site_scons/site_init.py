import filecmp
import os
import subprocess

def RegressionTest(env):
    def check_output(target, source, env):
        """
        Run the program with the given input and arguments, succeeding only
        if it produces the expected output.
        """
        if env["CAPTURE_INPUT"]:
            inputFile = str(env["CAPTURE_INPUT"][0])
        else:
            inputFile = os.devnull

        with \
            open(os.devnull, "w") as devnull, \
            open(inputFile) as input, \
            open(str(target[0]), "w") as output:
            subprocess.check_call(
                map(str, env["CAPTURE_COM"] + env["CAPTURE_FLAGS"]),
                stderr=devnull,
                stdin=input,
                stdout=output,
            )

        return not filecmp.cmp(str(source[0]), str(target[0]))

    def regression_test(env, program, output, arguments=None, input=None):
        """
        Compare the output of running the program, passing the arguments
        and standard input.
        """
        program = File(Split(program))
        arguments = Split(arguments) if arguments else []
        input = File(Split(input)) if input else []
        captured = env.Capture(
            output,
            CAPTURE_COM=program,
            CAPTURE_FLAGS=arguments,
            CAPTURE_INPUT=input,
        )
        env.Depends(captured, program + input)
        return captured

    env.Append(BUILDERS={
        "Capture": Builder(
            action=Action(check_output, varlist=["CAPTURE_FLAGS"]),
            suffix=".captured",
        ),
    })
    env.AddMethod(regression_test, "RegressionTest")

def Roc(env):
    env.Append(BUILDERS={
        "Roc": Builder(
            action=[["$ROC", "build", "--output", "$TARGET", "$SOURCES"]],
            prefix="$PROGPREFIX",
            src_suffix=".roc",
            suffix="$PROGSUFFIX",
        ),
    })
