#include "geo/bounds.h"
#include "geo/direction.h"
#include "geo/point.h"
#include "rng/all.h"
#include "rng/count.h"
#include "streamer/getlines.h"

#include <sysexits.h>

#include <cassert>
#include <iostream>
#include <string>
#include <vector>

using streamer::getlines;

using Point = geo::Point<geo::Direction<2>>;
using Bounds = geo::Bounds<Point>;

static int Main(std::vector<std::string_view> args)
{
    if (2 != args.size()) {
        std::cerr << "usage: " << args[0] << R"( minutes

    Run the simulation for the given number of minutes.
)";
        return EX_USAGE;
    }

    std::vector<std::string> yard{getlines(std::cin)};
    auto const get{[&yard](Point const& point)
                   {
                       unsigned const row{static_cast<unsigned>(point[0])};
                       unsigned const column{static_cast<unsigned>(point[1])};
                       if (row < yard.size() && column < yard[row].size()) {
                           return yard[row][column];
                       }
                       return '.';
                   }};

    for (unsigned long minute{0}, lastMinute{std::stoul(std::string{args[1]})};
         minute != lastMinute;
         ++minute)
    {
        std::vector<std::string> newYard{};
        newYard.reserve(yard.size());
        for (std::size_t row{0}; row != yard.size(); ++row) {
            std::string newRow{};
            for (std::size_t column{0}; column != yard[row].size(); ++column) {
                Point const point{
                    static_cast<int>(row),
                    static_cast<int>(column)};
                unsigned int lumber{0};
                unsigned int wood{0};

                Bounds{point}.inflate(1).eachBoundaryPoint(
                    [&lumber, &wood, &get](Point const& pnt)
                    {
                        switch (get(pnt)) {
                        case '#':
                            lumber += 1;
                            break;
                        case '|':
                            wood += 1;
                            break;
                        }
                    });

                switch (get(point)) {
                case '#':
                    newRow.push_back(1 <= lumber && 1 <= wood ? '#' : '.');
                    break;
                case '.':
                    newRow.push_back(3 <= wood ? '|' : '.');
                    break;
                case '|':
                    newRow.push_back(3 <= lumber ? '#' : '|');
                    break;
                default:
                    assert(false);
                    return 1;
                }
            }
            newYard.push_back(std::move(newRow));
        }

        yard = std::move(newYard);

        unsigned int wood{0};
        unsigned int lumber{0};

        for (std::string const& row : yard) {
            wood += rng::all(row) | rng::Count{'|'};
            lumber += rng::all(row) | rng::Count{'#'};
        }

        std::cout << "minute=" << minute << " wood=" << wood
                  << " lumber=" << lumber << " result=" << wood * lumber
                  << '\n';
    }

    return 0;
}

int main(int argc, char** argv)
{
    return Main(std::vector<std::string_view>(argv, argc + argv));
}
