#include "streamer/stream.h"

#include <iostream>
#include <vector>

using streamer::streamFromTo;

int main()
{
    std::size_t rounds;
    while (std::cin >> rounds) {
        std::vector<unsigned int> recipes{3, 7};
        std::size_t i{0}, j{1};
        while (recipes.size() < 10 + rounds) {
            unsigned int const recipe{recipes[i] + recipes[j]};
            if (10 <= recipe) {
                recipes.push_back(1);
            }
            recipes.push_back(recipe % 10);
            i = (i + 1 + recipes[i]) % recipes.size();
            j = (j + 1 + recipes[j]) % recipes.size();
        }
        std::cout << "rounds=" << rounds << " recipes="
                  << streamFromTo(&recipes[rounds], &recipes[10 + rounds], "")
                  << '\n';
    }

    return 0;
}
