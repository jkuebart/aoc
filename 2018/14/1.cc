#include <iostream>
#include <vector>

namespace
{
/**
 * Check whether the sequence matches the tail of the vector
 * interperted as digits.
 *
 * @param sequence The sequence.
 * @param recipes The digits.
 * @return The position of a match with the sequence, or 0 if none.
 * @note You are expected to check yourself whether your sequence is at
 *       very start of the vector.
 */
std::size_t match(unsigned int sequence, std::vector<unsigned char>& recipes)
{
    std::size_t pos{recipes.size()};
    unsigned int tail{0};
    unsigned int significance{1};
    while (0 < pos && tail < sequence) {
        tail += significance * recipes[pos - 1];
        significance *= 10;
        pos -= 1;
    }
    return tail == sequence ? pos : 0;
}
} // namespace

int main()
{
    unsigned int sequence;
    while (std::cin >> sequence) {
        std::vector<unsigned char> recipes{3, 7};
        std::size_t i{0}, j{1};
        for (;;) {
            int const recipe{recipes[i] + recipes[j]};
            if (10 <= recipe) {
                recipes.push_back(1);
                if (match(sequence, recipes)) {
                    break;
                }
            }
            recipes.push_back(recipe % 10);
            if (match(sequence, recipes)) {
                break;
            }
            i = (i + 1 + recipes[i]) % recipes.size();
            j = (j + 1 + recipes[j]) % recipes.size();
        }
        std::cout << "sequence=" << sequence
                  << " pos=" << match(sequence, recipes) << '\n';
    }

    return 0;
}
