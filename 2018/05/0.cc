#include "rng/all.h"
#include "rng/as.h"
#include "rng/filter.h"
#include "rng/minElement.h"
#include "rng/sort.h"
#include "rng/transform.h"
#include "rng/unique.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <string>

namespace
{
constexpr char kCase{'a' ^ 'A'};

std::string react(std::string line)
{
    std::size_t i{0};
    while (1 + i < line.size()) {
        if (kCase == (line[i] ^ line[1 + i])) {
            line.erase(i, 2);
            i = std::max(std::size_t{1}, i) - 1;
        } else {
            i += 1;
        }
    }

    return line;
}
} // namespace

int main()
{
    for (std::string line{}; std::getline(std::cin, line);) {
        std::size_t minLength{react(line).size()};
        std::cout << "standard=" << minLength << '\n';

        // Find shortest possible with one removed polymer.
        auto const shorter{
            rng::all(line) |
            rng::Transform{[](char const ch)
                           {
                               return static_cast<char>(ch | kCase);
                           }} |
            rng::sort | rng::unique |
            rng::Transform{[&line](char const p)
                           {
                               auto const simpler{
                                   rng::all(line) |
                                   rng::Filter{[p](char const ch)
                                               {
                                                   return p != (ch | kCase);
                                               }} |
                                   rng::as<std::string>};
#ifdef notdef
                               std::cerr << p << ": " << react(simpler).size()
                                         << '\n';
#endif
                               return react(simpler).size();
                           }}};
        if (!shorter.empty()) {
            minLength =
                std::min(minLength, (shorter | rng::minElement).front());
        }
        std::cout << "improved=" << minLength << '\n';
    }

    return 0;
}
