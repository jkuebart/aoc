#include "rng/accumulate.h"
#include "rng/all.h"

#include <iostream>
#include <vector>

namespace
{
struct Node
{
    int metasum() const
    {
        return rng::all(metadata) | rng::Accumulate{0};
    }

    int sum() const
    {
        return metasum() +
            (rng::all(children) |
             rng::Accumulate{
                 0,
                 [](int s, auto const& node)
                 {
                     return s + node->sum();
                 }});
    }

    int value() const
    {
        if (children.empty()) {
            return metasum();
        }

        int val{0};
        for (int const index : metadata) {
            if (1 <= index &&
                static_cast<unsigned int>(index) - 1 < children.size()) {
                val += children[static_cast<unsigned int>(index) - 1]->value();
            }
        }
        return val;
    }

    friend std::istream& operator>>(std::istream& is, Node& rhs)
    {
        std::size_t descendents{0};
        std::size_t information{0};

        is >> descendents >> information;

        rhs.children.clear();
        while (rhs.children.size() < descendents) {
            rhs.children.push_back(std::make_unique<Node>());
            is >> *rhs.children.back();
        }

        rhs.metadata.clear();
        while (rhs.metadata.size() < information) {
            rhs.metadata.push_back(0);
            is >> rhs.metadata.back();
        }

        return is;
    }

#ifdef notdef
    friend std::ostream& operator<<(std::ostream& os, Node const& rhs)
    {
        return os << '<' << stream(rhs.metadata) << ' '
                  << stream(rhs.children, ", ") << '>';
    }
#endif

    std::vector<std::unique_ptr<Node>> children;
    std::vector<int> metadata;
};
} // namespace

int main()
{
    Node tree;
    while (std::cin >> tree) {
        std::cout << "sum=" << tree.sum() << " value=" << tree.value() << '\n';
    }

    return 0;
}
