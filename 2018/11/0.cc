#include "geo/bounds.h"
#include "geo/direction.h"
#include "geo/point.h"

#include <sysexits.h>

#include <iostream>
#include <string>
#include <vector>

using Direction = geo::Direction<2>;
using Point = geo::Point<Direction>;
using Bounds = geo::Bounds<Point>;

namespace
{
constexpr Bounds gridBounds{{1, 1}, {300, 300}};

int power(int serial, Point const& pnt)
{
    int const rack_id{pnt[0] + 10};
    return (rack_id * pnt[1] + serial) * rack_id / 100 % 10 - 5;
}

int power(int serial, Bounds const& bnds)
{
    int total{0};
    bnds.each(
        [serial, &total](Point const& pnt)
        {
            total += power(serial, pnt);
        });
    return total;
}
} // namespace

static int Main(std::vector<std::string_view> args)
{
    if (3 != args.size()) {
        std::cerr << "usage: " << args[0] << R"( minExtent maxExtent

    Compute the coordinates and size of the square with the largest total
    power, using sizes between minExtent and maxExtent.
)";
        return EX_USAGE;
    }

    int const minExtent{std::stoi(std::string{args[1]})};
    int const maxExtent{std::stoi(std::string{args[2]})};

    int serial;
    while (std::cin >> serial) {
        int level{std::numeric_limits<int>::min()};
        Point optimum{};
        int optimum_extent{};

        for (int extent{minExtent}; extent < 1 + maxExtent; ++extent) {
            Bounds const bnds{gridBounds.deflateUpper(extent)};
            Direction const squareOffset{extent - 1, extent - 1};

            bnds.each(
                [&](Point const& pnt)
                {
                    int const lvl{
                        power(serial, Bounds{pnt, pnt + squareOffset})};
                    if (level < lvl) {
                        level = lvl;
                        optimum = pnt;
                        optimum_extent = extent;
                    }
                });
        }

        std::cout << "serial=" << serial << " level=" << level
                  << " optimum=" << optimum
                  << " optimum_extent=" << optimum_extent << '\n';
    }

    return 0;
}

int main(int argc, char** argv)
{
    return Main(std::vector<std::string_view>(argv, argc + argv));
}
