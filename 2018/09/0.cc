#include "rng/all.h"
#include "rng/maxElement.h"
#include "streamer/stream.h"

#include <iostream>
#include <list>
#include <string>
#include <utility>
#include <vector>

using streamer::stream;

namespace
{
using Game = std::pair<std::size_t, unsigned int>;

std::istream& operator>>(std::istream& is, Game& rhs)
{
    std::string dummy{};

    if (is >> rhs.first) {
        while ("worth" != dummy) {
            is >> dummy;
        }
        is >> rhs.second;
        getline(is, dummy);
    }

    return is;
}
} // namespace

int main()
{
    using std::begin;
    using std::end;

    Game game{};
    while (std::cin >> game) {
        std::list<unsigned int> marbles{0};
        std::vector<unsigned int> scores(game.first, 0);

        auto pos{begin(marbles)};
        for (unsigned int current{1}; current <= game.second; ++current) {
            if (0 != current % 23) {
                ++pos;
                if (pos == end(marbles)) {
                    pos = begin(marbles);
                }
                pos = marbles.insert(++pos, current);
            } else {
                for (int i{0}; i != 7; ++i) {
                    if (pos == begin(marbles)) {
                        pos = end(marbles);
                    }
                    --pos;
                }
                scores[(current - 1) % scores.size()] += current + *pos;
                pos = marbles.erase(pos);
            }
        }

        auto const highscore{(rng::all(scores) | rng::maxElement).front()};
        std::cout << stream(game, ", ", "<", ">") << '=' << highscore << '\n';
    }

    return 0;
}
