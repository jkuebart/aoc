#include "geo/bounds.h"
#include "geo/direction.h"
#include "geo/point.h"
#include "rng/accumulate.h"
#include "rng/all.h"
#include "rng/each.h"
#include "rng/maxElement.h"

#include <sysexits.h>

#include <cstdlib>
#include <iostream>
#include <iterator>
#include <map>
#include <string>
#include <vector>

using Point = geo::Point<geo::Direction<2>>;
using Bounds = geo::Bounds<Point>;

namespace
{
/**
 * Given a list of points and a point, return the nearest one(s).
 */
template<typename Container = std::vector<Point>, typename Range>
Container nearest(Range const& range, Point const& point)
{
    Container minima{};
    int minimum{};

    // Determine which points are closest to this point.
    range |
        rng::Each{[&](Point const& pnt)
                  {
                      int const dist{rectilinearDistance(point, pnt)};
                      if (minima.empty() || dist < minimum) {
                          minima.clear();
                          minimum = dist;
                      }
                      if (dist == minimum) {
                          minima.insert(minima.end(), pnt);
                      }
                  }};

    return minima;
}
} // namespace

static int Main(std::vector<std::string_view> args)
{
    if (2 != args.size()) {
        std::cerr << "usage: " << args[0] << R"( max_distance

    Compute the largest safe area, i.e. the area where the sum of distances
    to all points is less than max_distance.
)";
        return EX_USAGE;
    }

    std::vector<Point> const points{
        std::istream_iterator<Point>(std::cin),
        std::istream_iterator<Point>()};

    Bounds bounds{};
    for (Point const& point : points) {
        bounds.include(point);
    }

    std::map<Point, unsigned int> areas{};
    bounds.deflate(1).each(
        [&areas, &points](Point const& innerPoint)
        {
            auto const minima{nearest(rng::all(points), innerPoint)};

            // If this inner point has a unique nearest neighbour, it counts
            // towards that neighbour's area.
            if (1 == minima.size()) {
                areas[minima[0]] += 1;
            }
        });

    // Remove points whose area extends to the boundary and is hence
    // infinite.
    bounds.eachBoundaryPoint(
        [&areas, &points](Point const& boundaryPoint)
        {
            auto const minima{nearest(rng::all(points), boundaryPoint)};

            // If this boundary point has a unique nearest neighbour, that
            // neighbour's area is infinite.
            if (1 == minima.size()) {
                areas.erase(minima[0]);
            }
        });

    auto const largest_area{
        (rng::all(areas) |
         rng::MaxElement{[](auto const& lhs, auto const& rhs)
                         {
                             return lhs.second < rhs.second;
                         }})
            .front()};

    std::cout << "largest_area=" << largest_area.second << '\n';

    int const max_distance{std::stoi(std::string{args[1]})};

    Bounds const larger_bounds{
        bounds.inflate(1 + max_distance / static_cast<int>(points.size()))};

    unsigned int safe_area{};
    larger_bounds.deflate(1).each(
        [&max_distance, &safe_area, &points](Point const& innerPoint)
        {
            int const total_distance{
                rng::all(points) |
                rng::Accumulate{
                    0,
                    [&innerPoint](int d, Point const& point)
                    {
                        return d + rectilinearDistance(innerPoint, point);
                    }}};
            if (total_distance < max_distance) {
                safe_area += 1;
            }
        });

    std::cout << "safe_area=" << safe_area << '\n';

    return 0;
}

int main(int argc, char** argv)
{
    return Main(std::vector<std::string_view>(argv, argc + argv));
}
