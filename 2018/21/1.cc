#include <iostream>
#include <set>

int main()
{
    std::set<unsigned int> seen{};
    unsigned int last_r1{0};

    for (unsigned int r1{0};;) {
        unsigned int r3{r1 | 0x10000};

        for (r1 = 0xa668b0; 0 != r3; r3 /= 256) {
            r1 = (r1 + (r3 & 0xff)) & 0xffffff;
            r1 = (r1 * 65899) & 0xffffff;
        }

        if (seen.empty()) {
            std::cout << "first=" << r1 << '\n';
        }

        // Stop after detecting a cycle. The previous r1 value is the longest
        // run that still terminates.

        if (!seen.insert(r1 | 0x10000).second) {
            break;
        }
        last_r1 = r1;
    }

    std::cout << "last=" << last_r1 << '\n';

    return 0;
}
