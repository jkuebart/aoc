#include "device/program.h"

#include <sysexits.h>

#include <array>
#include <iostream>
#include <string>
#include <vector>

using streamer::stream;

using Registers = std::array<int, 6>;
using Program = device::Program<Registers>;

static int Main(std::vector<std::string_view> args)
{
    if (2 != args.size()) {
        std::cerr << "usage: " << args[0] << R"( r0

    Read a program from standard input and run it with the given initial
    value for r0.
)";
        return EX_USAGE;
    }

    // Assemble the instructions.

    Program const program{Program::assemble(std::cin)};

    // Run the instructions.

    Registers const registers{program.run({stoi(std::string{args[1]})}, true)};
    std::cout << "registers=" << stream(registers) << '\n';

    return 0;
}

int main(int argc, char** argv)
{
    return Main(std::vector<std::string_view>(argv, argc + argv));
}
