#ip 2
;   for (;;) {
;       if (0x48 == (0x7b & 0x1c8)) {
;           break;
;       }
;   }
;
 0 seti 123 0 1
 1 bani 1 456 1
 2 eqri 1 72 1      ; jmp r1 == 0x48 ? 5 : 1
 3 addr 1 2 2

 4 seti 0 0 2

;   r1 = 0
 5 seti 0 4 1

;   for (;;) {
;       r3 = r1 | 0x10000;
;       r1 = 0xa668b0;
 6 bori 1 65536 3
 7 seti 10905776 4 1

;       for (;;) {
;           r1 = (r1 + (r3 & 0xff)) & 0xffffff;
;           r1 = (r1 * 65899) & 0xffffff;
 8 bani 3 255 4
 9 addr 1 4 1
10 bani 1 16777215 1
11 muli 1 65899 1
12 bani 1 16777215 1

;           if (r3 < 256) {
;               break;
;           }
13 gtir 256 3 4
14 addr 4 2 2

15 addi 2 1 2

16 seti 27 1 2

;           r3 = r3 / 256;
17 seti 0 6 4       ; r4 = 0
18 addi 4 1 5       ; r5 = 1 + r4
19 muli 5 256 5     ; r5 = 256 * r5
20 gtrr 5 3 5       ; jmp (r5 > r3) ? 26 : 24
21 addr 5 2 2
22 addi 2 1 2
23 seti 25 1 2
24 addi 4 1 4       ; r4 += 1
25 seti 17 9 2      ; jmp 18
26 setr 4 7 3       ; r3 = r4

;       }
27 seti 7 4 2

;       if (r0 == r1) {
;           halt;
;       }
;   }
28 eqrr 1 0 4
29 addr 4 2 2

30 seti 5 1 2
