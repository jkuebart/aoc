#include "streamer/stream.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <string>
#include <unordered_set>

using streamer::stream;

namespace
{
struct Claim
{
    template<typename Func>
    constexpr Func each(Func func) const
    {
        for (unsigned int z0{0}; z0 != y0; ++z0) {
            for (unsigned int z1{0}; z1 != y1; ++z1) {
                func(id, x0 + z0, x1 + z1);
            }
        }
        return func;
    }

    friend std::istream& operator>>(std::istream& is, Claim& rhs)
    {
        std::string dummy;

        getline(is, dummy, '#');
        is >> rhs.id;
        getline(is, dummy, '@');
        is >> rhs.x0;
        getline(is, dummy, ',');
        is >> rhs.x1;
        getline(is, dummy, ':');
        is >> rhs.y0;
        getline(is, dummy, 'x');
        is >> rhs.y1;

        return is;
    }

    unsigned int id{0};
    unsigned int x0{0};
    unsigned int x1{0};
    unsigned int y0{0};
    unsigned int y1{0};
};

class Matrix
{
public:
    unsigned int
    set(unsigned int const x0, unsigned int const x1, unsigned int const val)
    {
        if (mWidth <= x0 || mHeight <= x1) {
            using std::max;
            resize(max(8 * mWidth / 5, 1 + x0), max(8 * mHeight / 5, 1 + x1));
        }

        unsigned int& elem{element(x0, x1)};
        unsigned int const res{elem};
        elem = val;
        return res;
    }

    unsigned int at(unsigned int const x0, unsigned int const x1) const
    {
        return (x0 < mWidth && x1 < mHeight ? element(x0, x1) : 0);
    }

    void resize(unsigned int const width, unsigned int const height)
    {
        std::unique_ptr<unsigned int[]> new_data{
            new unsigned int[width * height]};

        for (unsigned int x0{0}; x0 != width; ++x0) {
            for (unsigned int x1{0}; x1 != height; ++x1) {
                new_data[x0 * height + x1] = at(x0, x1);
            }
        }

        mWidth = width;
        mHeight = height;
        mData = std::move(new_data);
    }

    unsigned int count() const
    {
        return mWidth * mHeight -
            static_cast<unsigned int>(
                   std::count(&mData[0], &mData[mWidth * mHeight], 0));
    }

private:
    unsigned int& element(unsigned int const x0, unsigned int const x1)
    {
        return mData[x0 * mHeight + x1];
    }

    unsigned int const& element(unsigned int const x0, unsigned int const x1)
        const
    {
        return const_cast<Matrix&>(*this).element(x0, x1);
    }

    unsigned int mWidth{0};
    unsigned int mHeight{0};
    std::unique_ptr<unsigned int[]> mData{};
};
} // namespace

int main()
{
    Matrix smpl{};
    Matrix dbl{};
    std::unordered_set<unsigned int> ids{};

    Claim claim{};
    while (std::cin >> claim) {
        assert(0 != claim.id);
        ids.insert(claim.id);
        claim.each(
            [&smpl, &dbl, &ids](
                unsigned int const id,
                unsigned int const x0,
                unsigned int const x1)
            {
                unsigned int const prev{smpl.set(x0, x1, id)};
                if (0 != prev) {
                    ids.erase(prev);
                    ids.erase(id);
                    dbl.set(x0, x1, id);
                }
            });
    }

    std::cout << "smpl=" << smpl.count() << " dbl=" << dbl.count()
              << " ids=" << stream(ids, ", ", "{", "}") << '\n';

    return 0;
}
