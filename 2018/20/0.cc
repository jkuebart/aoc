#include "geo/direction.h"
#include "geo/point.h"
#include "streamer/stream.h"

#include <sysexits.h>

#include <chrono>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <string_view>
#include <vector>

using streamer::stream;

using Direction = geo::Direction<2>;
using Room = geo::Point<Direction>;
using Graph = std::multimap<Room, Room>;

namespace
{
Graph parseGraph(std::string_view regex)
{
    Graph graph{};
    std::vector<Room> positions{{0, 0}};
    auto const pathDirection = [&graph, &positions](Direction const& direction)
    {
        Room const next{positions.back() + direction};
        graph.emplace(positions.back(), next);
        graph.emplace(next, positions.back());
        positions.back() = next;
    };

    while (!regex.empty()) {
        char const ch{regex.front()};
        regex.remove_prefix(1);
        switch (ch) {
        case '(':
            positions.push_back(positions.back());
            break;
        case ')':
            if (positions.size() < 2) {
                std::cerr << "Unmatched close parenthesis at '" << regex
                          << "'\n";
                return {};
            }
            positions.pop_back();
            break;
        case 'E':
            pathDirection({1, 0});
            break;
        case 'N':
            pathDirection({0, 1});
            break;
        case 'S':
            pathDirection({0, -1});
            break;
        case 'W':
            pathDirection({-1, 0});
            break;
        case '|':
            if (1 < positions.size()) {
                positions.back() = positions[positions.size() - 2];
            } else {
                positions.back() = {0, 0};
            }
            break;
        default:
            std::cerr << "Parse error at '" << regex.front() << "'\n";
            return {};
        }
    }
    if (1 < positions.size()) {
        std::cerr << "Unmatched open parenthesis.\n";
        return {};
    }
    return graph;
}
} // namespace

int main()
{
    for (std::string line{}; std::getline(std::cin, line);) {
        auto const parsing{std::chrono::steady_clock::now()};
        if ('^' != line.front() || '$' != line.back()) {
            std::cerr << "Invalid regex: begins with '" << line.front()
                      << "' and ends with '" << line.back() << "'\n";
            return EX_DATAERR;
        }
        std::string_view regex{line};
        regex.remove_prefix(1);
        regex.remove_suffix(1);
        Graph const graph{parseGraph(regex)};

        auto const walking{std::chrono::steady_clock::now()};
        int maxDistance{0};
        int rooms1000{0};
        std::queue<std::pair<Room, int>> seen{{{{0, 0}, 0}}};
        std::set<Room> visited{{0, 0}};
        while (!seen.empty()) {
            auto const& [room, distance]{seen.front()};
            maxDistance = distance;
            if (1000 <= maxDistance) {
                rooms1000 += 1;
            }
            auto const edges{graph.equal_range(room)};
            for (auto it{edges.first}; it != edges.second; ++it) {
                if (0 == visited.count(it->second)) {
                    visited.insert(it->second);
                    seen.push({it->second, 1 + distance});
                }
            }
            seen.pop();
        }
        std::cerr << "parsing="
                  << std::chrono::duration_cast<std::chrono::milliseconds>(
                         walking - parsing)
                         .count()
                  << "ms"
                  << " walking="
                  << std::chrono::duration_cast<std::chrono::milliseconds>(
                         std::chrono::steady_clock::now() - walking)
                         .count()
                  << "ms" << '\n';
        std::cout << "maxDistance=" << maxDistance << " rooms1000=" << rooms1000
                  << '\n';
    }
    return 0;
}
