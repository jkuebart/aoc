#include "rng/accumulate.h"
#include "rng/all.h"
#include "rng/maxElement.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <iterator>
#include <numeric>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

// 32 bits cover about ±4000 years.
using TickType = long;

namespace
{
/**
 * Floor division with remainder.
 *
 *    lhs == qr.quot * rhs + qr.rem
 * &&
 *    sign(qr.rem) == sign(rhs)
 * &&
 *    0 <= abs(qr.rem) < abs(rhs)
 */
template<typename T>
constexpr auto divmod(T lhs, T rhs)
{
    struct
    {
        T quot;
        T rem;
    } qr{lhs / rhs, lhs % rhs};
    if ((0 < rhs && qr.rem < 0) || (rhs < 0 && 0 < qr.rem)) {
        qr.quot -= 1;
        qr.rem += rhs;
    }
    assert(lhs == rhs * qr.quot + qr.rem);
    return qr;
}

class Timestamp
{
    static constexpr TickType mktick(int y, int m, int d, int H, int M)
    {
        auto const mnth{divmod(m - 3, 12)};
        y += mnth.quot;
        m = mnth.rem;
        d -= 1;
        auto const prd{divmod(m, 5)};
        return M +
            60 *
            (H +
             24 *
                 (static_cast<TickType>(y) * 365 + y / 4 - y / 100 + y / 400 +
                  153 * prd.quot + 31 * prd.rem - prd.rem / 2 + d));
    }

public:
    constexpr explicit Timestamp(
        int const y = 0,
        int const m = 3,
        int const d = 1,
        int const H = 0,
        int const M = 0)
    : mTick{mktick(y, m, d, H, M)}
    {}

    constexpr TickType tick() const
    {
        return mTick;
    }

    friend constexpr bool operator<(Timestamp const& lhs, Timestamp const& rhs)
    {
        return lhs.mTick < rhs.mTick;
    }

private:
    TickType mTick{};
};

enum class EventType
{
    takeDuty,
    asleep,
    wakeUp
};

#ifdef notdef
std::ostream& operator<<(std::ostream& os, EventType const rhs)
{
    switch (rhs) {
    case EventType::takeDuty:
        return os << "takeDuty";
    case EventType::asleep:
        return os << "asleep";
    case EventType::wakeUp:
        return os << "wakeUp";
    }
}
#endif

struct Event
{
    friend std::istream& operator>>(std::istream& is, Event& rhs)
    {
        std::string dummy{}, guard{};
        int y{0}, m{0}, d{0}, H{0}, M{0};

        getline(is, dummy, '[');
        is >> y;
        getline(is, dummy, '-');
        is >> m;
        getline(is, dummy, '-');
        is >> d;
        getline(is, dummy, ' ');
        is >> H;
        getline(is, dummy, ':');
        is >> M;
        getline(is, dummy, ']');
        getline(is, guard);

        if (is.eof()) {
            return is;
        }

        rhs.time = Timestamp{y, m, d, H, M};
        if (" falls asleep" == guard) {
            rhs.type = EventType::asleep;
        } else if (" wakes up" == guard) {
            rhs.type = EventType::wakeUp;
        } else {
            rhs.type = EventType::takeDuty;
            std::stringstream str{guard};
            getline(str, dummy, '#');
            str >> rhs.id;
            assert(0 != rhs.id);
        }

#ifdef notdef
        std::cerr << '[' << y << '-' << m << '-' << d << ' ' << H << ':' << M
                  << ']' << rhs.type << '#' << rhs.id << '\n';
#endif

        return is;
    }

#ifdef notdef
    friend std::ostream& operator<<(std::ostream& os, Event const& rhs)
    {
        return os << '@' << rhs.time.tick() << ':' << rhs.type << '#' << rhs.id;
    }
#endif

    Timestamp time{};
    EventType type{};
    int id{0};
};

constexpr Event EMPTY{Timestamp{}, EventType::takeDuty, 0};
} // namespace

int main()
{
    using std::begin;
    using std::end;

    std::vector<Event> events{
        std::istream_iterator<Event>(std::cin),
        std::istream_iterator<Event>()};

    std::sort(
        begin(events),
        end(events),
        [](auto const& lhs, auto const& rhs)
        {
            return lhs.time < rhs.time;
        });

    // For each guard, a map from minutes to times they spent asleep.
    std::unordered_map<int, std::unordered_map<int, int>> asleep{};

    // The state machine for walking through the log.
    Event state{EMPTY};
    for (auto const& evt : events) {
        assert(EventType::takeDuty == evt.type || 0 != state.id);
#ifdef notdef
        std::cerr << "state=" << state << " evt=" << evt << '\n';
#endif

        switch (state.type) {
        case EventType::takeDuty:
            assert(EventType::takeDuty == evt.type);
            state.id = evt.id;
            state.type = EventType::wakeUp;
            break;

        case EventType::wakeUp:
            switch (evt.type) {
            case EventType::takeDuty:
                state.id = evt.id;
                break;

            case EventType::asleep:
                state.time = evt.time;
                state.type = evt.type;
                break;

            case EventType::wakeUp:
                assert(false);
            }
            break;

        case EventType::asleep:
            assert(EventType::wakeUp == evt.type);
            for (auto tck{state.time.tick()}; tck != evt.time.tick(); ++tck) {
                asleep[state.id][tck % (24 * 60)] += 1;
#ifdef notdef
                std::cerr << '#' << state.id << '@' << tck % (24 * 60) << '='
                          << asleep[state.id][tck % (24 * 60)] << '\n';
#endif
            }
            state.type = evt.type;
            break;
        }
    }

    // The guard's total minutes.
    auto const total_guard_minutes{[](auto const& guard)
                                   {
                                       return rng::all(guard.second) |
                                           rng::Accumulate{
                                               0,
                                               [](const auto& a, const auto& b)
                                               {
                                                   return a + b.second;
                                               }};
                                   }};

    // The guard with the most minutes.
    auto const max_guard{
        (rng::all(asleep) |
         rng::MaxElement{
             [&total_guard_minutes](auto const& lhs, auto const& rhs)
             {
                 return total_guard_minutes(lhs) < total_guard_minutes(rhs);
             }})
            .front()};

    // The guard's highest minute.
    auto const max_guard_minute{
        [](auto const& guard)
        {
            return (rng::all(guard.second) |
                    rng::MaxElement{[](auto const& lhs, auto const& rhs)
                                    {
                                        return lhs.second < rhs.second;
                                    }})
                .front();
        }};
    auto const mgm{max_guard_minute(max_guard)};

    std::cerr << '#' << max_guard.first << '@' << mgm.first << '=' << mgm.second
              << '\n';

    std::cout << max_guard.first * mgm.first << '\n';

    // The guard with the highest overall minute.
    auto const max_minute_guard{
        (rng::all(asleep) |
         rng::MaxElement{[&max_guard_minute](auto const& lhs, auto const& rhs)
                         {
                             return max_guard_minute(lhs).second <
                                 max_guard_minute(rhs).second;
                         }})
            .front()};
    auto const mmgm{max_guard_minute(max_minute_guard)};

    std::cerr << '#' << max_minute_guard.first << '@' << mmgm.first << '='
              << mmgm.second << '\n';

    std::cout << max_minute_guard.first * mmgm.first << '\n';

    return 0;
}
