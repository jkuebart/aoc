#include "rng/all.h"
#include "rng/anyOf.h"

#include <iostream>
#include <string>
#include <unordered_map>

namespace
{
class IdChecker
{
public:
    void add(std::string const& s)
    {
        for (char const c : s) {
            mHist[c] += 1;
        }
    }

    bool has(unsigned int const n) const
    {
        return rng::all(mHist) |
            rng::AnyOf{[n](auto const& h)
                       {
                           return n == h.second;
                       }};
    }

private:
    std::unordered_map<char, unsigned int> mHist{};
};
} // namespace

int main()
{
    std::string s{};
    unsigned int c2{0};
    unsigned int c3{0};

    while (std::cin >> s) {
        IdChecker checker{};
        checker.add(s);

        if (checker.has(2)) {
            c2 += 1;
        }

        if (checker.has(3)) {
            c3 += 1;
        }
    }

    std::cout << c2 * c3 << '\n';
    return 0;
}
