#include <iostream>
#include <string>
#include <unordered_set>
#include <vector>

int main()
{
    std::vector<std::unordered_set<std::string>> stubs{};

    std::string s{};
    while (std::cin >> s) {
        if (stubs.size() < s.size()) {
            stubs.resize(s.size());
        }

        for (std::size_t i{0}; i != s.size(); ++i) {
            auto stub{s};
            stub.erase(i, 1);

            // If the same stub was found earlier, we're finished.

            if (!stubs[i].insert(stub).second) {
                std::cout << stub << '\n';
                return 0;
            }
        }
    }

    return 1;
}
