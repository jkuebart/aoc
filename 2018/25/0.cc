#include "geo/direction.h"
#include "geo/point.h"
#include "rng/all.h"
#include "rng/allOf.h"

#include <algorithm>
#include <iostream>
#include <list>
#include <vector>

using Point = geo::Point<geo::Direction<4>>;

int main()
{
    std::vector<std::list<Point>> constellations{};

    Point point{};
    while (std::cin >> point) {

        // Move all constellations this point is a part of to the end of the
        // vector.

        auto const firstReplaced{std::partition(
            begin(constellations),
            end(constellations),
            [&point](auto const& constellation)
            {
                return rng::all(constellation) |
                    rng::AllOf{[&point](auto const& pnt)
                               {
                                   return 3 < rectilinearDistance(point, pnt);
                               }};
            })};

        // Merge final constellations into a new one.

        std::list<Point> newConstellation{point};
        for (auto it{firstReplaced}; it != end(constellations); ++it) {
            newConstellation.splice(end(newConstellation), *it);
        }

        // Replace final constellations by new one.

        constellations.erase(firstReplaced, end(constellations));
        constellations.push_back(std::move(newConstellation));
    }

    std::cout << "nconstellations=" << constellations.size() << '\n';

    return 0;
}
