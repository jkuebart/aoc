#include "device/processor.h"
#include "streamer/stream.h"

#include <array>
#include <cassert>
#include <iostream>
#include <string>
#include <vector>

using device::Instruction;
using streamer::stream;

using Registers = std::array<int, 4>;
using Opcode = device::Opcode<Registers>;

int main()
{

    // Find beginning of instructions after end of effects.

    std::string line[2]{};
    getline(std::cin, line[0]);
    while (getline(std::cin, line[1]) && ("" != line[0] || "" != line[1])) {
        line[0] = std::move(line[1]);
    }

    std::vector<Instruction> instructions{};
    Instruction instruction{};
    while (std::cin >> stream(instruction, " ", "", "\n")) {
        instructions.push_back(instruction);
    }

    Registers registers{};
    for (auto const& instrctn : instructions) {
        assert(
            0 <= instrctn[0] &&
            static_cast<std::size_t>(instrctn[0]) < std::size(Opcode::opcodes));
        registers = Opcode::opcodes[instrctn[0]].operation(registers, instrctn);
    }
    std::cout << "registers=" << stream(registers) << '\n';

    return 0;
}
