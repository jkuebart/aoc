#include "device/processor.h"
#include "rng/all.h"
#include "rng/as.h"
#include "rng/setIntersection.h"
#include "streamer/stream.h"

#include <array>
#include <cassert>
#include <iostream>
#include <map>
#include <set>
#include <string>

using device::Instruction;
using streamer::stream;

using Registers = std::array<int, 4>;
using Opcode = device::Opcode<Registers>;

namespace
{
struct Effect
{
    friend std::istream& operator>>(std::istream& is, Effect& rhs)
    {
        std::string dummy{};

        Registers bfr{};
        getline(is, dummy, '[');
        is >> stream(bfr, ",");
        getline(is, dummy);

        Instruction instrctn{};
        is >> stream(instrctn);

        Registers aftr{};
        getline(is, dummy, '[');
        is >> stream(aftr, ",");
        getline(is, dummy);

        rhs = Effect{bfr, instrctn, aftr};
        return is;
    }

#ifdef notdef
    friend std::ostream& operator<<(std::ostream& os, Effect const& rhs)
    {
        return os << "Before: " << stream(rhs.before, ", ", "[", "]") << '\n'
                  << stream(rhs.instruction) << '\n'
                  << "After: " << stream(rhs.after, ", ", "[", "]") << '\n';
    }
#endif

    Registers before{};
    Instruction instruction{};
    Registers after{};
};
} // namespace

int main()
{
    std::map<int, std::set<std::string_view>> opcodeMnemonics{};
    std::set<int> uniqueOpcodes{};
    std::size_t at_least_three{0};

    while (std::cin && 'B' == std::cin.peek()) {
        std::string dummy{};
        Effect effect{};
        std::cin >> effect;
        getline(std::cin, dummy);

#ifdef notdef
        std::cerr << effect << '\n';
#endif

        std::set<std::string_view> mnemonics{};
        for (auto const& opcode : Opcode::opcodes) {
            if (effect.after ==
                opcode.operation(effect.before, effect.instruction)) {
#ifdef notdef
                std::cerr
                    << opcode.mnemonic << ": "
                    << stream(
                           opcode.operation(effect.before, effect.instruction),
                           ", ",
                           "[",
                           "]")
                    << '\n';
#endif
                mnemonics.insert(opcode.mnemonic);
            }
        }

#ifdef notdef
        std::cerr << "matches=" << mnemonics.size() << '\n';
#endif
        if (3 <= mnemonics.size()) {
            at_least_three += 1;
        }

        int const opcode{effect.instruction[0]};
        auto const opMnem{opcodeMnemonics.emplace(opcode, mnemonics)};

        // If the opcode was seen before, intersect the sets of possible
        // mnemonics.

        if (!opMnem.second) {
            opMnem.first->second =
                rng::SetIntersectionRange{
                    rng::all(opMnem.first->second),
                    rng::all(mnemonics)} |
                rng::as<std::set<std::string_view>>;
        }

        if (opcodeMnemonics[opcode].size() <= 1) {
            uniqueOpcodes.insert(opcode);
        }
    }

    std::cout << "at_least_three=" << at_least_three << '\n';

    // Remove unique mnemonics from all other opcodes.

    while (!uniqueOpcodes.empty()) {
        int const opcode{*uniqueOpcodes.begin()};
        uniqueOpcodes.erase(uniqueOpcodes.begin());

        assert(1 == opcodeMnemonics[opcode].size());
        std::string_view const& mnemonic{*opcodeMnemonics[opcode].begin()};
        for (auto& mnem : opcodeMnemonics) {
            if (opcode != mnem.first && 0 != mnem.second.erase(mnemonic) &&
                mnem.second.size() <= 1)
            {
                uniqueOpcodes.insert(mnem.first);
            }
        }
    }

    for (auto const& op : opcodeMnemonics) {
        std::cout << op.first << ": " << stream(op.second) << '\n';
    }

    return 0;
}
