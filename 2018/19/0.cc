#include "device/program.h"
#include "streamer/stream.h"

#include <array>
#include <cassert>
#include <iostream>
#include <map>
#include <vector>

using streamer::stream;

using Registers = std::array<int, 6>;
using Program = device::Program<Registers>;

int main()
{

    // Assemble the instructions.

    Program const program{Program::assemble(std::cin)};

    // Run the instructions.

    std::cout << "registers=" << stream(program.run()) << '\n';

    return 0;
}
