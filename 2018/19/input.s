#ip 4
 0 addi 4 16 4  ; jmp 17 = * + 17

    ; for (r3 = 1; r3 <= r5; r3 += 1) {
    ;   for (r2 = 1; r2 <= r5; r2 += 1) {
    ;       if (r2 * r3 == r5) {
    ;           r0 += r3;
    ;       }
    ;   }
    ; }

    ; Compute sum of the factors:
    ; 939 = 3 * 313
    ; 1 + 3 + 313 + 939
    ; = (1 + 3)(1 + 313)
    ; = 1256
    ;
    ; 10551339 = 3^2 * 17 * 68963
    ; (1 + 3 + 9)(1 + 17)(1 + 68963)
    ; = 13 * 18 * 68964
    ; = 16137576
    ; 13 18*68964*p

    ; r3 := 1
    ; r2 := 1
 1 seti 1 3 3   ; r3 := 1
 2 seti 1 4 2   ; r2 := 1

    ; r1 := r2 * r3
 3 mulr 3 2 1   ; r1 := r2 * r3

    ; jmp r1 == r5 ? 7 : 8
 4 eqrr 1 5 1   ; r1 := r1 == r5
 5 addr 1 4 4   ; jmp 6 + r1
 6 addi 4 1 4   ; jmp 8

    ; r0 += r3
 7 addr 3 0 0   ; r0 += r3

    ; r2 += 1
 8 addi 2 1 2   ; r2 += 1

    ; jmp r2 > r5 ? 12 : 3
 9 gtrr 2 5 1   ; r1 := r2 > r5
10 addr 4 1 4   ; jmp 11 + r1
11 seti 2 2 4   ; jmp 3

    ; r3 += 1
12 addi 3 1 3   ; r3 += 1

    ; jmp r3 > r5 ? 16 : 2
13 gtrr 3 5 1   ; r1 := r3 > r5
14 addr 1 4 4   ; jmp 15 + r1
15 seti 1 6 4   ; jmp 2

    ; halt
16 mulr 4 4 4   ; jmp 257

    ; r1 := 103 = (4 + r1) * 22 + 15
    ; r5 := 939 = (2 + r5) * (2 + r5) * 19 * 11 + r1
17 addi 5 2 5   ; r5 += 2
18 mulr 5 5 5   ; r5 *= r5
19 mulr 4 5 5   ; r5 *= r4
20 muli 5 11 5  ; r5 *= 11
21 addi 1 4 1   ; r1 += 4
22 mulr 1 4 1   ; r1 *= r4
23 addi 1 15 1  ; r1 += 15
24 addr 5 1 5   ; r5 += r1
25 addr 4 0 4   ; jmp 26 + r0

    ; jmp 1
26 seti 0 9 4   ; jmp 1

    ; r1 := 10550400 = (27 * 28 + 29) * 30 * 14 * 32
27 setr 4 2 1   ; r1 := r4
28 mulr 1 4 1   ; r1 *= r4
29 addr 4 1 1   ; r1 += r4
30 mulr 4 1 1   ; r1 *= r4
31 muli 1 14 1  ; r1 *= 14
32 mulr 1 4 1   ; r1 *= r4

    ; r5 := 10551339 = r1 + r5
33 addr 5 1 5   ; r5 += r1

    ; r0 := 0
34 seti 0 8 0   ; r0 := 0

    ; jmp 1
35 seti 0 4 4   ; jmp 1
