#include "geo/direction.h"
#include "geo/point.h"
#include "rng/accumulate.h"
#include "rng/all.h"
#include "rng/as.h"
#include "rng/countIf.h"
#include "rng/each.h"
#include "rng/transform.h"
#include "streamer/getlines.h"
#include "streamer/stream.h"

#include <sysexits.h>

#include <algorithm>
#include <cassert>
#include <iostream>
#include <list>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <vector>

#undef LOG_ATTACK
#undef LOG_ROUND
#undef LOG_ROUTE
#undef LOG_TURN

using streamer::getlines;
using streamer::stream;

using Direction = geo::Direction<2>;
using Point = geo::Point<Direction>;

namespace
{
struct Unit
{
    unsigned long hitPoints{200};
};

class Maze
{
    static std::map<Point, Unit> mkunits(std::vector<std::string> const& maze)
    {
        std::map<Point, Unit> units{};

        for (std::size_t row{0}; row != maze.size(); ++row) {
            for (std::size_t col{0}; col != maze[row].size(); ++col) {
                char const type{maze[row][col]};
                if ('E' == type || 'G' == type) {
                    units[Point{static_cast<int>(row), static_cast<int>(col)}];
                }
            }
        }

        return units;
    }

public:
    explicit Maze(std::vector<std::string> const& maze = {})
    : mMaze(maze)
    , mUnits(mkunits(mMaze))
    {}

    explicit Maze(std::vector<std::string>&& maze)
    : mMaze(std::move(maze))
    , mUnits(mkunits(mMaze))
    {}

    char at(Point const& pnt) const
    {
        return at(
            static_cast<std::size_t>(pnt[0]),
            static_cast<std::size_t>(pnt[1]));
    }

    char at(std::size_t row, std::size_t column) const
    {
        return (
            row < mMaze.size() && column < mMaze[row].size()
                ? mMaze[row][column]
                : '#');
    }

    /**
     * @return A range of all unit positions.
     */

    auto unitPositions() const
    {
        return rng::all(mUnits) |
            rng::Transform{[](auto const& unit)
                           {
                               return unit.first;
                           }};
    }

    /**
     * @return A range of all units.
     */

    auto units() const
    {
        return rng::all(mUnits) |
            rng::Transform{[](auto const& unit)
                           {
                               return unit.second;
                           }};
    }

    /**
     * @param pnt The point at which to look for units.
     * @param func The functor to invoke for every unit.
     * @return The functor.
     */

    template<typename Func>
    Func eachUnitAt(Point const& pnt, Func func) const
    {
        auto const unit{mUnits.find(pnt)};
        if (end(mUnits) != unit) {
            func(unit->second);
        }
        return func;
    }

    /**
     * @param type The type of unit to count.
     * @return The number of units of that type.
     */

    std::size_t unitsOfType(char const type)
    {
        return unitPositions() |
            rng::CountIf{[this, &type](Point const& pnt)
                         {
                             return type == at(pnt);
                         }};
    }

    /**
     * @param pnt Starting point.
     * @param targets Possible destinations.
     * @return The first step towards the closest target, first in reading
     *         order if tied.
     */

    Point closestTarget(Point const& pnt, std::set<Point> const& targets) const;

    /**
     * @param pnt Unit to move.
     * @param target Target position.
     */

    void move(Point const& pnt, Point const& target)
    {
        assert('.' == at(target));
        assert(1 == rectilinearDistance(pnt, target));
        std::swap(mutableAt(pnt), mutableAt(target));

        auto const unit{mUnits.find(pnt)};
        assert(end(mUnits) != unit);
        assert(end(mUnits) == mUnits.find(target));
        mUnits.emplace(target, unit->second);
        mUnits.erase(unit);
    }

    /**
     * @param enemy Unit position to attack.
     * @param attackPoints Strength of the attack.
     * @return True if the enemy was killed.
     */

    bool attack(Point const& enemy, unsigned long const attackPoints)
    {
        auto const unit{mUnits.find(enemy)};
        assert(end(mUnits) != unit);

#ifdef LOG_ATTACK
        std::cerr << "enemy=" << enemy
                  << " hitPoints=" << unit->second.hitPoints << '\n';
#endif // LOG_ATTACK

        if (attackPoints < unit->second.hitPoints) {
            unit->second.hitPoints -= attackPoints;
            return false;
        }

        mutableAt(enemy) = '.';
        mUnits.erase(unit);
        return true;
    }

    friend std::istream& operator>>(std::istream& is, Maze& maze)
    {
        maze = Maze{getlines(is)};
        return is;
    }

    friend std::ostream& operator<<(std::ostream& os, Maze const& maze)
    {
        return os << stream(maze.mMaze, "\n") << '\n';
    }

private:
    char& mutableAt(Point const& pnt)
    {
        return mutableAt(
            static_cast<std::size_t>(pnt[0]),
            static_cast<std::size_t>(pnt[1]));
    }

    char& mutableAt(std::size_t row, std::size_t column)
    {
        assert(row < mMaze.size() && column < mMaze[row].size());
        return mMaze[row][column];
    }

    std::vector<std::string> mMaze{};
    std::map<Point, Unit> mUnits{};
};

Point Maze::closestTarget(Point const& pnt, std::set<Point> const& targets)
    const
{

    // The tuple contains the cost to the position, the target position and
    // the first step along the path to the target, so tuples are compared
    // according to the game's tie-breaking rules.

    struct CostAndTargetAndFirstStep
    {
        unsigned int cost;
        Point target;
        Point firstStep;

        bool operator<(CostAndTargetAndFirstStep const& rhs) const
        {
            return std::tie(rhs.cost, rhs.target, rhs.firstStep) <
                std::tie(cost, target, firstStep);
        }
    };
    std::priority_queue<CostAndTargetAndFirstStep> unexplored{
        {},
        {{0, pnt, pnt}}};
    std::set<Point> visited{};

    // Explore while there are unexplored fields.

    while (!unexplored.empty()) {

        // Pick the unexplored point with lowest cost.

        auto const [cost, target, firstStep]{unexplored.top()};
        unexplored.pop();

#ifdef LOG_ROUTE
        std::cerr << "at " << target << " cost=" << cost
                  << " firstStep=" << firstStep << '\n';
#endif // LOG_ROUTE

        // We are finished when the lowest unexplored point is a target.

        if (0 != targets.count(target)) {
            return firstStep;
        }

        // Skip duplicates in priority queue.

        if (0 != visited.count(target)) {
            continue;
        }
        visited.insert(target);

        target.adjacent(
            [this, cost = cost, firstStep = firstStep, &unexplored](
                Point const& dst)
            {
                if ('.' == at(dst)) {
                    Point const nextFirstStep{0 == cost ? dst : firstStep};
                    unexplored.push({1 + cost, dst, nextFirstStep});
                }
            });
    }

    return pnt;
}

#if defined(LOG_ROUND) || defined(LOG_TURN)
void logScore(Maze const& maze)
{
    std::cerr << maze << '\n';
    maze.unitPositions() |
        rng::Each{[&maze](Point const& position)
                  {
                      std::cerr << "unit=" << position;
                      maze.eachUnitAt(
                          position,
                          [](Unit const& unit)
                          {
                              std::cerr << " hitPoints=" << unit.hitPoints;
                          });
                      std::cerr << '\n';
                  }};
    std::cerr << '\n';
}
#endif // defined(LOG_ROUND) || defined(LOG_TURN)
} // namespace

static int Main(std::vector<std::string_view> args)
{
    if (3 != args.size()) {
        std::cerr << "usage: " << args[0]
                  << R"( elf_attack_points goblin_attack_points

    Run the simulation with the given number of attack points for elves and
    goblins, respectively.
)";
        return EX_USAGE;
    }
    unsigned long const elfAttackPoints{stoul(std::string{args[1]})};
    unsigned long const goblinAttackPoints{stoul(std::string{args[2]})};

    Maze maze{};
    std::cin >> maze;

    std::cout << "elves=" << maze.unitsOfType('E')
              << " goblins=" << maze.unitsOfType('G') << '\n';

    unsigned int round{0};
    for (;;) {

        // Every unit takes a turn unless they die before it's their turn.

        auto unitsToMove{maze.unitPositions() | rng::as<std::set<Point>>};

        while (!unitsToMove.empty()) {
            Point const player{*unitsToMove.begin()};

            // Enumerate the attack positions.

            std::set<Point> attackPositions{};
            char const type{maze.at(player)};
            maze.unitPositions() |
                rng::Each{[&maze, &attackPositions, &type](Point const& unit)
                          {
                              if (type != maze.at(unit)) {
                                  unit.adjacent(
                                      [&attackPositions](Point const& pnt)
                                      {
                                          attackPositions.insert(pnt);
                                      });
                              }
                          }};

            // End the turn when there are no more enemies.

            if (attackPositions.empty()) {
                break;
            }
            unitsToMove.erase(player);

            // Find the closest attack positions.

            Point const bestAttackPosition{
                maze.closestTarget(player, attackPositions)};

            // There are three general cases:
            //
            //   - bestAttackPosition.cost == 0
            //     We are in an attack position.
            //   - bestAttackPosition.cost == infinity
            //     There is no reachable attack position (or no enemy).
            //   - otherwise, we perform the first step of the path to the
            //     optimal position.
            //
            // The first two result in target == player.

            Point const& target{bestAttackPosition};
            if (player != target) {
                maze.move(player, target);
            }

            // Find weakest adjacent enemy that is first in reading order. The
            // tuple is compared according to the game's tie-breaking rules.

            using CostAndPosition = std::tuple<unsigned int, Point>;
            CostAndPosition attacking{
                std::numeric_limits<unsigned int>::max(),
                {}};
            target.adjacent(
                [&maze, &type, &attacking](Point const& pnt)
                {
                    maze.eachUnitAt(
                        pnt,
                        [&maze, &type, &attacking, &pnt](Unit const& unit)
                        {
                            if (type != maze.at(pnt)) {
                                CostAndPosition const enemy{
                                    unit.hitPoints,
                                    pnt};
                                if (enemy < attacking) {
                                    attacking = enemy;
                                }
                            }
                        });
                });

            // Attack if we are able.

            if (std::get<0>(attacking) <
                std::numeric_limits<unsigned int>::max()) {
                Point const& enemy{std::get<1>(attacking)};

                assert('E' == maze.at(enemy) || 'G' == maze.at(enemy));
                unsigned long const attackPoints{
                    'E' == maze.at(enemy) ? goblinAttackPoints
                                          : elfAttackPoints};

                if (maze.attack(enemy, attackPoints)) {
                    unitsToMove.erase(enemy);
                }
            }

#ifdef LOG_TURN
            std::cerr << "round=" << round << '\n';
            logScore(maze);
#endif // LOG_TURN
        }

#ifdef LOG_ROUND
        std::cerr << "round=" << round << " enemyExists=" << unitsToMove.empty()
                  << '\n';
        logScore(maze);
#endif // LOG_ROUND

        // If the last turn ended for lack of enemies, end the game.

        if (!unitsToMove.empty()) {
            break;
        }

        ++round;
    }

    auto const score{
        maze.units() |
        rng::Accumulate{
            0ul,
            [](auto const sc, Unit const& unit)
            {
                return sc + unit.hitPoints;
            }}};

    std::cerr << maze << '\n';
    std::cout << "elves=" << maze.unitsOfType('E')
              << " goblins=" << maze.unitsOfType('G') << '\n'
              << "round=" << round << " score=" << score
              << " result=" << round * score << '\n';

    return 0;
}

int main(int argc, char** argv)
{
    return Main(std::vector<std::string_view>(argv, argc + argv));
}
