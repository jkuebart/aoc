#include "geo/direction.h"
#include "geo/point.h"
#include "rng/all.h"
#include "rng/countIf.h"
#include "rng/maxElement.h"

#include <algorithm>
#include <array>
#include <iostream>
#include <string>
#include <vector>

using Point = geo::Point<geo::Direction<3>>;

int main()
{
    std::vector<std::pair<Point, int>> nanobots{};

    for (std::string dummy{}; std::getline(std::cin, dummy, '<');) {
        Point pos{};
        std::cin >> pos;
        std::getline(std::cin, dummy, '=');
        int radius;
        std::cin >> radius;
        std::getline(std::cin, dummy);

        nanobots.emplace_back(pos, radius);
    }

    auto const largest{(rng::all(nanobots) |
                        rng::MaxElement{[](auto const& lhs, auto const& rhs)
                                        {
                                            return lhs.second < rhs.second;
                                        }})
                           .front()};

    auto const count{
        rng::all(nanobots) |
        rng::CountIf{[&largest](auto const& bot)
                     {
                         return rectilinearDistance(bot.first, largest.first) <=
                             largest.second;
                     }}};

    std::cout << "nbots=" << nanobots.size() << " largest=<" << largest.first
              << "> r=" << largest.second << " count=" << count << '\n';

    return 0;
}
