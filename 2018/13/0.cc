#include "geo/direction.h"
#include "geo/point.h"

#include <cassert>
#include <iostream>
#include <map>
#include <string>

using Direction = geo::Direction<2>;
using Point = geo::Point<Direction>;

namespace
{
constexpr Direction DOWN{0, 1};
constexpr Direction LEFT{-1, 0};
constexpr Direction RIGHT{1, 0};
constexpr Direction UP{0, -1};

Direction left(Direction const& direction)
{
    return {direction[1], -direction[0]};
}

Direction right(Direction const& direction)
{
    return {-direction[1], direction[0]};
}

struct Cart
{
    Direction direction{};
    unsigned int intersection{};
};

Cart intersection(Cart const& cart)
{
    Direction direction{};
    switch (cart.intersection % 3) {
    case 0:
        direction = left(cart.direction);
        break;
    case 1:
        direction = cart.direction;
        break;
    case 2:
        direction = right(cart.direction);
        break;
    }
    return {direction, 1 + cart.intersection};
}

Cart backslash(Cart const& cart)
{
    return {{cart.direction[1], cart.direction[0]}, cart.intersection};
}

Cart slash(Cart const& cart)
{
    return {{-cart.direction[1], -cart.direction[0]}, cart.intersection};
}
} // namespace

int main()
{
    using std::begin;
    using std::end;

    std::map<Point, Cart> carts{};
    std::map<Point, Cart (*)(Cart const&)> tracks{};

    int y{0};
    for (std::string trackLine{}; std::getline(std::cin, trackLine);) {
        for (std::size_t x{0}; x != trackLine.size(); ++x) {
            Point const point{static_cast<int>(x), y};
            switch (trackLine[x]) {
            case ' ':
            case '-':
            case '|':
                break;
            case '+':
                tracks[point] = intersection;
                break;
            case '/':
                tracks[point] = slash;
                break;
            case '\\':
                tracks[point] = backslash;
                break;
            case '<':
                carts[point] = {LEFT, 0};
                break;
            case '>':
                carts[point] = {RIGHT, 0};
                break;
            case '^':
                carts[point] = {UP, 0};
                break;
            case 'v':
                carts[point] = {DOWN, 0};
                break;
            default:
                assert(false);
                return 1;
            }
        }
        y += 1;
    }

    /*
     * Move carts.
     * There's a tradeoff to be made here between keeping track of which carts
     * were updated and where each cart is. This code simply checks twice.
     */

    unsigned int tick{0};
    while (1 < carts.size()) {
        std::map<Point, Cart> newCarts{};
        while (!carts.empty()) {

            // Move cart along track and remove from unmoved carts.

            auto const firstCart{begin(carts)};
            Cart const& cart{firstCart->second};
            Point const position{firstCart->first + cart.direction};

            // Apply track action.

            auto const track{tracks.find(position)};
            Cart const newCart{
                end(tracks) != track ? track->second(cart) : cart};
            carts.erase(firstCart);

            // Check for collision.

            if (end(carts) == carts.find(position) &&
                end(newCarts) == newCarts.find(position))
            {
                newCarts.emplace(position, newCart);
            } else {
                std::cout << "tick=" << tick << " position=" << position
                          << '\n';
                newCarts.erase(position);
                carts.erase(position);
            }
        }
        carts = std::move(newCarts);
        ++tick;
    }

    if (1 <= carts.size()) {
        std::cout << "single remaining cart\n"
                  << "tick=" << tick << " position=" << begin(carts)->first
                  << '\n';
    }

    return 0;
}
