#include <sysexits.h>

#include <cassert>
#include <iostream>
#include <set>
#include <string>
#include <vector>

static int Main(std::vector<std::string_view> args)
{
    if (2 != args.size()) {
        std::cerr << "usage: " << args[0] << R"( generations

    Run the simulation for the given number of generations.
)";
        return EX_USAGE;
    }

    std::string plants{};
    int leftmost{0};

    std::getline(std::cin, plants);
    assert("initial state: " == plants.substr(0, 15));
    plants.erase(0, 15);

    std::set<std::string> spread{};
    for (std::string line{}; std::getline(std::cin, line);) {
        if (10 != line.size() || '#' != line[9]) {
            continue;
        }

        spread.insert(line.substr(0, 5));
    }

    std::size_t const generations{std::stoul(std::string{args[1]})};
    for (std::size_t generation{0}; generation != generations; ++generation) {
        auto const lftmst{plants.find('#')};
        auto const rghtmst{plants.find_last_of('#')};
        leftmost += lftmst;
        leftmost -= 3;
        plants = "....." + plants.substr(lftmst, 1 + rghtmst) + ".....";

        std::string nextPlants{};
        for (std::size_t i{0}; 5 + i < plants.size(); ++i) {
            bool const spreads{end(spread) != spread.find(plants.substr(i, 5))};
            nextPlants.push_back(spreads ? '#' : '.');
        }
        plants = std::move(nextPlants);

        if (generations < 5 + generation) {
            std::cerr << "generation=" << generation << " leftmost=" << leftmost
                      << " plants=" << plants << '\n';
        }
    }

    int sum{0};
    for (std::size_t i{0}; i != plants.size(); ++i) {
        if ('#' == plants[i]) {
            sum += leftmost + static_cast<int>(i);
        }
    }

    std::cout << sum << '\n';

    return 0;
}

int main(int argc, char** argv)
{
    return Main(std::vector<std::string_view>(argv, argc + argv));
}
