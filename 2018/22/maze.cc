#include "maze.h"

#include <cassert>
#include <string>

constexpr unsigned int NX{16807};
constexpr unsigned int NY{48271};
constexpr unsigned int N{20183};

namespace maze
{
Maze::Maze() = default;

Maze::Maze(unsigned int depth, Point const& target)
: mMaze{{depth}}
, mTarget{target}
{}

unsigned int Maze::depth() const
{
    return mMaze[0][0];
}

Maze::Point const& Maze::target() const
{
    return mTarget;
}

unsigned int Maze::at(Point const& pnt)
{
    assert(0 <= pnt[0] && 0 <= pnt[1]);
    std::size_t const x0{static_cast<std::size_t>(pnt[0])};
    std::size_t const x1{static_cast<std::size_t>(pnt[1])};
    while (mMaze.size() <= x1) {
        produceRow();
    }
    while (mMaze[x1].size() <= x0) {
        produceColumn();
    }
    return mMaze[x1][x0];
}

std::istream& operator>>(std::istream& is, Maze& rhs)
{
    std::string dummy{};
    unsigned int depth{0};
    Maze::Point target{};

    is >> dummy;
    assert("depth:" == dummy);
    is >> depth >> dummy;
    assert("target:" == dummy);
    is >> target;
    getline(is, dummy);

    rhs = Maze{depth, target};

    return is;
}

void Maze::produceRow()
{
    auto const y{static_cast<unsigned int>(mMaze.size())};
    std::vector<unsigned int> const& lastRow = mMaze.back();
    std::vector<unsigned int> row{(y * NY + depth()) % N};

    for (std::size_t x{1}; x < lastRow.size(); ++x) {
        row.push_back(
            mTarget == Point{static_cast<int>(x), static_cast<int>(y)}
                ? depth()
                : (row[x - 1] * lastRow[x] + depth()) % N);
    }

    mMaze.push_back(std::move(row));
}

void Maze::produceColumn()
{
    auto const x{static_cast<unsigned int>(mMaze[0].size())};
    mMaze[0].push_back((x * NX + depth()) % N);

    for (std::size_t y{1}; y < mMaze.size(); ++y) {
        mMaze[y].push_back(
            mTarget == Point{static_cast<int>(x), static_cast<int>(y)}
                ? depth()
                : (mMaze[y].back() * mMaze[y - 1].back() + depth()) % N);
    }
}

} // namespace maze
