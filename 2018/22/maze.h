#ifndef MAZE_H
#define MAZE_H

#include "geo/direction.h"
#include "geo/point.h"

#include <iosfwd>
#include <vector>

namespace maze
{
class Maze
{
public:
    using Point = geo::Point<geo::Direction<2>>;

    Maze();

    Maze(unsigned int depth, Point const& target);

    unsigned int depth() const;
    Point const& target() const;
    unsigned int at(Point const& pnt);

    friend std::istream& operator>>(std::istream& is, Maze& rhs);

private:
    void produceRow();
    void produceColumn();

private:
    std::vector<std::vector<unsigned int>> mMaze{};
    Point mTarget{};
};
} // namespace maze

#endif // MAZE_H
