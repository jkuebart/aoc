#include "maze.h"

#include "geo/bounds.h"

#include <iostream>

using maze::Maze;

using Point = Maze::Point;
using Bounds = geo::Bounds<Point>;

namespace
{
constexpr unsigned int NTYPES{3};
}

int main()
{
    Maze maze;
    std::cin >> maze;

    Bounds const bnds{{0, 0}, maze.target()};

    unsigned int total_risk{0};
    bnds.each(
        [&maze, &total_risk](Point const& pnt)
        {
            total_risk += maze.at(pnt) % NTYPES;
        });

    std::cout << "target=" << maze.target() << " total_risk=" << total_risk
              << '\n';

    return 0;
}
