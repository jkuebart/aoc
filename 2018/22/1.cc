#include "maze.h"

#include <iostream>
#include <queue>
#include <set>

using maze::Maze;

using Point = Maze::Point;

constexpr unsigned int NTYPES{3};
constexpr unsigned int STEP_COST{1};
constexpr unsigned int TOOL_COST{7};

namespace
{
struct PositionAndTool
{
    Point position{};
    unsigned int tool{};

    friend bool operator<(
        PositionAndTool const& lhs,
        PositionAndTool const& rhs)
    {
        return std::tie(lhs.position, lhs.tool) <
            std::tie(rhs.position, rhs.tool);
    }
};
} // namespace

int main()
{
    Maze maze;
    std::cin >> maze;

    // For each position and tool we store a cost of reaching that position
    // equipped with the tool. Tools are designated by the type in which
    // they can't be used, i.e. 0 neither, 1 torch and 2 climbing gear. We
    // start and end in a rocky region (0) equipped with the torch (1).

    struct Node
    {
        PositionAndTool positionAndTool;
        unsigned cost;

        bool operator<(Node const& rhs) const
        {
            return rhs.cost < cost;
        }
    };
    std::priority_queue<Node> unexplored{{}, {{{Point{0, 0}, 1}, 0}}};
    std::set<PositionAndTool> visited{};
    while (!unexplored.empty()) {

        // Pick the unexplored point with the lowest cost.

        auto const [positionAndTool, startCost]{unexplored.top()};
        auto const [position, startTool]{positionAndTool};
        unexplored.pop();

        // Finish when the target is the cheapest unvisited node.

        if (maze.target() == position && 1 == startTool) {
            std::cout << "target=" << position << " cost=" << startCost << '\n';
            break;
        }

        // Skip duplicates in the priority queue.

        if (0 != visited.count(positionAndTool)) {
            continue;
        }
        visited.insert(positionAndTool);

        // Visit the otherTool neighbour at the current position. The other
        // usable tool for a region of a certain type is given by
        //
        //          otherTool + type + tool = 0  (mod 3)

        unsigned int const otherTool{
            (NTYPES - startTool - maze.at(position) % NTYPES) % NTYPES};
        unexplored.push({{position, otherTool}, TOOL_COST + startCost});

        // Visit the neighbouring positions.

        position.adjacent(
            [&maze,
             &unexplored,
             cost = STEP_COST + startCost,
             tool = startTool](Point const& dst)
            {
                // Skip nonexistent positions and impossible transitions, i.e.
                // where the current tool is unusable at the destination (tool
                // = type).

                if (0 <= dst[0] && 0 <= dst[1] && tool != maze.at(dst) % NTYPES)
                {
                    unexplored.push({{dst, tool}, cost});
                }
            });
    }

    return 0;
}
