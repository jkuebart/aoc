#include "rng/all.h"
#include "rng/each.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <utility>
#include <vector>

using dependency = std::pair<char, char>;

namespace
{
std::istream& operator>>(std::istream& is, dependency& rhs)
{
    std::string line{};
    if (getline(is, line)) {
        assert("Step " == line.substr(0, 5));
        rhs.second = line[5];
        assert(" must be finished before step " == line.substr(6, 30));
        rhs.first = line[36];
    }

    return is;
}

#ifdef notdef
std::ostream& operator<<(std::ostream& os, dependency const& rhs)
{
    return os << stream(rhs, ", ", "<", ">");
}
#endif
} // namespace

int main()
{
    using std::begin;
    using std::end;

    std::map<char, std::set<char>> requirements{};

    dependency dpndncy{};
    while (std::cin >> dpndncy) {
        requirements[dpndncy.first].insert(dpndncy.second);
        requirements[dpndncy.second];
    }

    while (!requirements.empty()) {
        auto const next{std::find_if(
            begin(requirements),
            end(requirements),
            [](auto const& rqrmnt)
            {
                return rqrmnt.second.empty();
            })};

        if (end(requirements) == next) {
            std::cerr << "circular dependency.\n";
            return 1;
        }

        rng::all(requirements) |
            rng::Each{[&next](auto& rqrmnt)
                      {
                          rqrmnt.second.erase(next->first);
                      }};

        std::cout << next->first;
        requirements.erase(next);
    }

    std::cout << '\n';

    return 0;
}
