#include "rng/all.h"
#include "rng/each.h"

#include <sysexits.h>

#include <algorithm>
#include <cassert>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <utility>
#include <vector>

using dependency = std::pair<char, char>;

namespace
{
std::istream& operator>>(std::istream& is, dependency& rhs)
{
    std::string line{};
    if (getline(is, line)) {
        assert("Step " == line.substr(0, 5));
        rhs.second = line[5];
        assert(" must be finished before step " == line.substr(6, 30));
        rhs.first = line[36];
    }

    return is;
}

#ifdef notdef
std::ostream& operator<<(std::ostream& os, dependency const& rhs)
{
    return os << stream(rhs, ", ", "<", ">");
}
#endif

class Queue
{
public:
    Queue(int duration, std::size_t workerCount)
    : mWorkerCount{workerCount}
    , mDuration{duration}
    , mCurrentTime{0}
    , mWorkers{}
    {}

    std::size_t capacity() const
    {
        return mWorkerCount - mWorkers.size();
    }

    constexpr int finished(char task) const
    {
        return mCurrentTime + mDuration + 1 + task - 'A';
    }

    Queue& enqueue(char task)
    {
        assert(0 < capacity());
        mWorkers.emplace(finished(task), task);
        return *this;
    }

    /**
     * Pass finished tasks to functor and remove from queue.
     */
    template<typename Func>
    constexpr Func results(Func func)
    {
        using std::begin;
        using std::end;

        auto it{begin(mWorkers)};
        for (; it != end(mWorkers) && it->first <= mCurrentTime; ++it) {
            func(it->second);
        }
        mWorkers.erase(begin(mWorkers), it);

        return func;
    }

    Queue& advance()
    {
        if (!mWorkers.empty()) {
            mCurrentTime = mWorkers.begin()->first;
        }
        return *this;
    }

    constexpr int now() const
    {
        return mCurrentTime;
    }

private:
    std::size_t mWorkerCount;
    int mDuration;
    int mCurrentTime;
    std::multimap<int, char> mWorkers;
};
} // namespace

static int Main(std::vector<std::string_view> args)
{
    if (3 != args.size()) {
        std::cerr << "usage: " << args[0] << R"( duration workerCount

    Run the simulation with the given number of workers and duration.
)";
        return EX_USAGE;
    }

    using std::begin;
    using std::end;

    std::map<char, std::set<char>> requirements{};

    dependency dpndncy{};
    while (std::cin >> dpndncy) {
        requirements[dpndncy.first].insert(dpndncy.second);
        requirements[dpndncy.second];
    }

    Queue queue{stoi(std::string{args[1]}), stoul(std::string{args[2]})};
    while (!requirements.empty()) {
        // While there are tasks and free workers, assign.
        while (0 != queue.capacity()) {
            auto const next{std::find_if(
                begin(requirements),
                end(requirements),
                [](auto const& rqrmnt)
                {
                    return rqrmnt.second.empty();
                })};
            if (end(requirements) == next) {
                break;
            }
#ifdef notdef
            std::cerr << '[' << queue.now() << "] assigning task "
                      << next->first << '\n';
#endif

            queue.enqueue(next->first);
            requirements.erase(next);
        }

        // Advance the time.
        queue.advance();

        // Resolve tasks which are finished.
        queue.results(
            [&requirements](char const task)
            {
#ifdef notdef
                std::cerr << "finished task " << task << '\n';
#endif

                rng::all(requirements) |
                    rng::Each{[task](auto& rqrmnt)
                              {
                                  rqrmnt.second.erase(task);
                              }};

                std::cout << task;
            });
    }

    std::cout << '\n' << queue.now() << '\n';

    return 0;
}

int main(int argc, char** argv)
{
    return Main(std::vector<std::string_view>(argv, argc + argv));
}
