#include "geo/bounds.h"
#include "geo/direction.h"
#include "geo/point.h"
#include "rng/all.h"
#include "rng/as.h"
#include "rng/transform.h"

#include <functional>
#include <iostream>
#include <iterator>
#include <set>
#include <string>
#include <utility>
#include <vector>

using Direction = geo::Direction<2>;
using Point = geo::Point<Direction>;
using Bounds = geo::Bounds<Point>;

namespace
{
struct Light
{
    Light advance() const
    {
        return Light{position + velocity, velocity};
    }

    Point position{};
    Direction velocity{};
};

std::istream& operator>>(std::istream& is, Light& rhs)
{
    std::string dummy{};

    if (getline(is, dummy, '<')) {
        is >> rhs.position;
        getline(is, dummy, '<');
        is >> rhs.velocity;
        getline(is, dummy);
    }

    return is;
}

#ifdef notdef
std::ostream& operator<<(std::ostream& os, Light const& rhs)
{
    return os << "position=<" << rhs.position << "> velocity=<" << rhs.velocity
              << '>';
}
#endif

/**
 * Compute the area covered by some lights.
 */
template<typename Range>
Bounds makeBounds(Range lights)
{
    Bounds bnds{};
    for (; !lights.empty(); lights.popFront()) {
        bnds.include(lights.front().position);
    }
    return bnds;
}

template<typename Range>
std::ostream& display(std::ostream& os, Range lights)
{
    Bounds bnds{};
    std::set<Point> points{};
    for (; !lights.empty(); lights.popFront()) {
        bnds.include(lights.front().position);
        points.insert(lights.front().position);
    }

    for (auto x1{bnds.lowerBound()[1]}; x1 != bnds.upperBound()[1]; ++x1) {
        for (auto x0{bnds.lowerBound()[0]}; x0 != bnds.upperBound()[0]; ++x0) {
            bool const light{end(points) != points.find({x0, x1})};
            os << (light ? '#' : '.');
        }
        os << '\n';
    }

    return os;
}
} // namespace

int main()
{
    std::vector<Light> lights{
        std::istream_iterator<Light>(std::cin),
        std::istream_iterator<Light>()};
    Bounds bnds{makeBounds(rng::all(lights))};

    for (int generation{0};; ++generation) {
        std::vector<Light> nextLights{
            rng::all(lights) | rng::Transform{std::mem_fn(&Light::advance)} |
            rng::as<std::vector<Light>>};
        Bounds nextBounds{makeBounds(rng::all(nextLights))};

        if (bnds.area() < 10000 && bnds.area() < nextBounds.area()) {
            std::cout << "generation=" << generation << '\n';
            break;
        }

        lights = std::move(nextLights);
        bnds = std::move(nextBounds);
    }

    display(std::cout, rng::all(lights));

    return 0;
}
