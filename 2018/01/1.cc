#include <iostream>
#include <iterator>
#include <unordered_set>
#include <vector>

int main()
{
    std::vector<int> const sequence{
        std::istream_iterator<int>(std::cin),
        std::istream_iterator<int>()};

    std::unordered_set<int> seen;
    int n{0};
    for (;;) {
        for (auto const i : sequence) {
            n += i;

            // If this number was seen before, we're finished.

            if (!seen.insert(n).second) {
                std::cout << n << std::endl;
                return 0;
            }
        }
    }
}
