app [main] {
    pf: platform "https://github.com/roc-lang/basic-cli/releases/download/0.17.0/lZFLstMUCUvd5bjnnpYromZJXkQUrdhbva4xdBInicE.tar.br",
}

import pf.Stdin
import pf.Stdout

import Report

main =
    safe =
        readLines!
        |> List.map Report.parse
        |> List.countIf Report.checkDampened
    Stdout.line "There are $(safe |> Num.toStr) safe dampened reports."

readLines : Task (List Str) _
readLines =
    Task.loop [] \lines ->
        when Stdin.line |> Task.result! is
            Ok line -> Step (List.append lines line) |> Task.ok
            Err (StdinErr EndOfFile) -> Done lines |> Task.ok
            Err err -> Task.err err
