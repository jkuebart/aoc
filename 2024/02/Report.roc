module [
    Report,
    checkDampened,
    check,
    parse,
]

Report : List I64

parse : Str -> Report
parse = \str ->
    str
    |> Str.splitOn " "
    |> List.keepOks Str.toI64

expect parse "" == []
expect parse " " == []
expect parse "0 " == [0]
expect parse " 1" == [1]
expect parse "0 1" == [0, 1]

check : Report -> Bool
check = \report ->
    diffs = diffList report
    when (List.min diffs, List.max diffs) is
        (Ok min, Ok max) -> ((-3 <= min && max <= -1) || (1 <= min && max <= 3))
        _ -> Bool.false

expect !(check [])
expect !(check [0])
expect !(check [0, 1, 0])
expect !(check [0, 4])
expect check [0, 3]
expect check [0, 1]
expect !(check [0, 0])
expect check [1, 0]
expect check [3, 0]
expect !(check [4, 0])

checkDampened : Report -> Bool
checkDampened = \report ->
    if check report then
        Bool.true
    else
        List.range { start: At 0, end: Length (List.len report) }
        |> List.map \i -> List.dropAt report i
        |> List.any check

expect !(checkDampened [])
expect !(checkDampened [0])
expect !(checkDampened [0, 1, 0, 1])
expect checkDampened [0, 1, 0]
expect !(checkDampened [0, 4, 0])
expect checkDampened [0, 4, 3]

zip : List a, List b -> List (a, b)
zip = \lhs, rhs -> List.map2 lhs rhs \l, r -> (l, r)

expect zip [] [] == []
expect zip [0] [] == []
expect zip [0] [2] == [(0, 2)]
expect zip [0, 1] [2, 3] == [(0, 2), (1, 3)]

## Turn a list into a list of differences between consecutive elements.
diffList : List (Num a) -> List (Num a)
diffList = \list ->
    zip list (List.dropFirst list 1)
    |> List.map \(a, b) -> b - a

expect diffList [] == []
expect diffList [0] == []
expect diffList [0, 1] == [1]
expect diffList [0, 1, 3] == [1, 2]
