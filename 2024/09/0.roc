app [main] {
    pf: platform "https://github.com/roc-lang/basic-cli/releases/download/0.17.0/lZFLstMUCUvd5bjnnpYromZJXkQUrdhbva4xdBInicE.tar.br",
}

import pf.Stdin
import pf.Stdout

import State exposing [State]

main =
    input =
        Stdin.readToEnd! {}
        |> List.dropLast 1
        |> List.map \digit -> digit - zero

    # Walk through the disk layout by file ID and compute the checksum
    # sum(file_id * block_num).
    # Whenever there is a gap, fill it by taking data from the back of the
    # disk.
    sum =
        when input is
            [] ->
                0

            [first, .. as rest] ->
                defrag rest 0 |> State.eval (Num.toU64 first)

    Stdout.line "$(Num.toStr sum)"

zero = 0x30

## Take a list starting with a gap length, the current file_id and a
## position. Compute the checksum of the remainder of the disk.
defrag : List U8, U64 -> Checksum
defrag = \list, file_id ->
    last_id = file_id + (List.len list) // 2
    when list is
        [] | [_] ->
            State.pure 0

        [_, len] ->
            checksum last_id (Num.toU64 len)

        [gap, len, .. as rest, last] if gap < last ->
            checksum last_id (Num.toU64 gap)
            |> State.bind \s0 ->
                checksum (1 + file_id) (Num.toU64 len)
                |> State.bind \s1 ->
                    defrag (List.append rest (last - gap)) (1 + file_id)
                    |> State.map \s2 -> s0 + s1 + s2

        [gap, .. as rest, _, last] ->
            checksum last_id (Num.toU64 last)
            |> State.bind \s0 ->
                defrag (List.concat [gap - last] rest) file_id
                |> State.map \s1 -> s0 + s1

expect defrag [] 0 |> State.eval 0 == 0
expect defrag [2, 3] 0 |> State.eval 0 == State.eval (checksum 1 3) 0
expect
    defrag [2, 3, 4, 5] 0
    |> State.eval 0
    ==
    checksum 2 2
    |> State.bind \s0 ->
        checksum 1 3
        |> State.bind \s1 ->
            checksum 2 3
            |> State.map \s2 -> s0 + s1 + s2
    |> State.eval 0

Checksum : State U64 U64

## Take a file ID and a run length and return the checksum
## sum([file_id * block_num | block_num <- ...]).
checksum : U64, U64 -> Checksum
checksum = \file_id, len ->
    State.get
    |> State.bind \pos ->
        State.put (pos + len)
        |> State.replace (file_id * len * (2 * pos + len - 1) // 2)

expect State.eval (checksum 0 2) 0 == 0 * 0 + 1 * 0
expect State.eval (checksum 9 2) 2 == 2 * 9 + 3 * 9
expect State.eval (checksum 8 1) 4 == 4 * 8
expect State.eval (checksum 1 3) 5 == 5 * 1 + 6 * 1 + 7 * 1
expect State.eval (checksum 8 3) 8 == 8 * 8 + 9 * 8 + 10 * 8
