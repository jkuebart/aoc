module [
    State,
    bind,
    eval,
    get,
    map,
    pure,
    put,
    replace,
    run,
]

State s a := s -> (a, s)

run : State s a, s -> (a, s)
run = \@State s, t -> s t

eval : State s a, s -> a
eval = \s, t -> (run s t).0

pure : a -> State _ a
pure = \x -> @State \s -> (x, s)

expect pure 0 |> run "s" == (0, "s")

put : s -> State s {}
put = \s -> @State \_ -> ({}, s)

expect put "s" |> run "t" == ({}, "s")

get : State s s
get = @State \s -> (s, s)

expect get |> run "s" == ("s", "s")

replace : State s a, b -> State s b
replace = \s, x -> @State \s0 -> (x, (run s s0).1)

expect replace (pure 0) 1 |> run "s" == (1, "s")

map : State s a, (a -> b) -> State s b
map = \s, f -> @State \s0 ->
    (x, s1) = run s s0
    (f x, s1)

expect
    pure 0
    |> map Num.toStr
    |> run {}
    == ("0", {})

bind : State s a, (a -> State s b) -> State s b
bind = \s, f -> @State \s0 ->
    (x, s1) = run s s0
    run (f x) s1

expect
    pure 0
    |> bind \x -> pure (1 + x)
    |> run "s"
    == (1, "s")
expect
    pure 1
    |> bind \x ->
        get
        |> bind \s ->
            put x
            |> map \_ -> (x + s)
    |> run 2
    == (3, 1)
