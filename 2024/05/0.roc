app [main] {
    pf: platform "https://github.com/roc-lang/basic-cli/releases/download/0.17.0/lZFLstMUCUvd5bjnnpYromZJXkQUrdhbva4xdBInicE.tar.br",
}

import pf.Stdin
import pf.Stdout

main =
    (ordering, updates) =
        inputLines!
        |> ordersAndUpdates
        |> Task.fromResult!

    (sorted, unsorted) =
        List.walk updates ([], []) \(s, u), update ->
            if checkUpdate ordering update then
                (List.append s update, u)
            else
                (s, List.append u update)

    sortedSum = midSum sorted
    Stdout.line! "Sorted sum of middles $(Num.toStr sortedSum)"

    unsortedSum =
        unsorted
        |> List.map \update -> List.sortWith update \l, r ->
            if Set.contains ordering (l, r) then GT else LT
        |> midSum
    Stdout.line! "Unsorted sum of middles $(Num.toStr unsortedSum)"

## The sum of the middle elements of all lists.
midSum : List (List (Num a)) -> Num a
midSum = \list ->
    List.keepOks list \elem -> List.get elem (List.len elem // 2)
    |> List.walk 0 Num.add

expect midSum [] == 0
expect midSum [[]] == 0
expect midSum [[0, 1, 2], [3, 4, 5]] == 5

## Whether an update is consistent with the given ordering.
checkUpdate : Set (U16, U16), List U16 -> Bool
checkUpdate = \ordering, update ->
    pairs update
    |> List.all \pair -> Set.contains ordering pair

## All ordered pairs of a list.
pairs : List U16 -> List (U16, U16) _
pairs = \update ->
    List.range { start: At 1, end: Before (List.len update), step: 1 }
    |> List.mapTry \end ->
        List.range { start: At 0, end: Before end }
        |> List.mapTry \start ->
            Ok (try List.get update start, try List.get update end)
    |> Result.withDefault []
    |> List.joinMap \x -> x

expect pairs [] == []
expect pairs [0] == []
expect pairs [0, 1] == [(0, 1)]
expect pairs [0, 1, 2] == [(0, 1), (0, 2), (1, 2)]

## Parse the input lines into a set of orderings and a list of updates.
ordersAndUpdates : List Str -> Result (Set (U16, U16), List (List U16)) [InvalidNumStr, NotFound]
ordersAndUpdates = \lines ->
    { before: orders, after: updates } = try List.splitFirst lines ""

    ordering =
        orders
        |> try List.mapTry \line -> Str.splitFirst line "|"
        |> try List.mapTry \{ before: x, after: y } ->
            Ok (try Str.toU16 x, try Str.toU16 y)
        |> Set.fromList

    updating =
        updates
        |> try List.mapTry \line -> Str.splitOn line "," |> List.mapTry Str.toU16

    Ok (ordering, updating)

expect ordersAndUpdates [] == Err NotFound
expect ordersAndUpdates [""] == Ok (Set.empty {}, [])
expect ordersAndUpdates ["0", ""] == Err NotFound
expect ordersAndUpdates ["0|", ""] == Err InvalidNumStr
expect ordersAndUpdates ["0|1", ""] == Ok (Set.single (0, 1), [])
expect ordersAndUpdates ["0|1", ""] == Ok (Set.single (0, 1), [])
expect ordersAndUpdates ["", ""] == Err InvalidNumStr
expect ordersAndUpdates ["", "0,1", "2,3"] == Ok (Set.empty {}, [[0, 1], [2, 3]])

inputLines : Task (List Str) _
inputLines =
    Task.loop [] \lines ->
        when Stdin.line |> Task.result! is
            Ok line -> Step (List.append lines line) |> Task.ok
            Err (StdinErr EndOfFile) -> Done lines |> Task.ok
            Err err -> Task.err err
