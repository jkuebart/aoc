module [
    parse,
]

## Read two operands \d{1,3},\d{1,3}) from the start of the string.
parse : Str -> Result (I32, I32) [InvalidNumStr, NotAnOperand, NotFound]
parse = \str ->
    (lhs, rest) = try parseOperand str ","
    (rhs, _) = try parseOperand rest ")"
    Ok (lhs, rhs)

expect parse "" == Err NotFound
expect parse "," == Err InvalidNumStr
expect parse "0,1" == Err NotFound
expect parse "0,a)" == Err InvalidNumStr
expect parse "0,1)" == Ok (0, 1)
expect parse "0000,1)" == Err NotAnOperand
expect parse "0,1111)" == Err NotAnOperand

## Read a delimited operand \d{1,3} from the start of the string.
parseOperand : Str, Str -> Result (I32, Str) [InvalidNumStr, NotAnOperand, NotFound]
parseOperand = \str, del ->
    { before: num, after: rest } = try Str.splitFirst str del
    if Str.countUtf8Bytes num <= 3 then
        Ok (try Str.toI32 num, rest)
    else
        Err NotAnOperand

expect parseOperand "" "" == Err InvalidNumStr
expect parseOperand "" "," == Err NotFound
expect parseOperand "0" "," == Err NotFound
expect parseOperand "a," "," == Err InvalidNumStr
expect parseOperand "0,rest" "," == Ok (0, "rest")
expect parseOperand "999," "," == Ok (999, "")
expect parseOperand "0000," "," == Err NotAnOperand
