app [main] {
    pf: platform "https://github.com/roc-lang/basic-cli/releases/download/0.17.0/lZFLstMUCUvd5bjnnpYromZJXkQUrdhbva4xdBInicE.tar.br",
}

import pf.Stdin
import pf.Stdout

import Mul

main =
    sum =
        Stdin.readToEnd! {}
        |> Str.fromUtf8
        |> Task.fromResult!
        |> Str.splitOn "do()" # The beginning of each string is "enabled"
        |> List.map \str ->
            Str.splitOn str "don't()"
            |> List.first
            |> Result.withDefault ""
        |> List.joinMap \str -> Str.splitOn str "mul("
        |> List.dropFirst 1
        |> List.keepOks Mul.parse
        |> List.map \(lhs, rhs) -> lhs * rhs
        |> List.walk 0 Num.add
    Stdout.line (Num.toStr sum)
