import Control.Monad (guard)
import Data.List.Extra (stripInfix)
import Data.List.Split (splitOn)
import Data.Maybe (mapMaybe)

main = do
    input <- getContents
    let
        parts = splitOn "mul(" input
        muls = mapMaybe parseMul $ drop 1 parts
        result = sum [lhs * rhs | (lhs, rhs) <- muls]
    print result

-- Read two operands \d{1,3},\d{1,3}) from the start of the string.
parseMul :: String -> Maybe (Int, Int)
parseMul str = do
    (lhs, rest) <- parseOperand "," str
    (rhs, _) <- parseOperand ")" rest
    return (lhs, rhs)

-- Read a delimited operand \d{1,3} from the start of the string.
parseOperand :: String -> String -> Maybe (Int, String)
parseOperand del str = do
    (before, after) <- stripInfix del str
    guard $ length before <= 3
    num <- toInt before
    return (num, after)

toInt :: String -> Maybe Int
toInt str = case reads str of
    [(n, "")] -> return n
    _ -> Nothing
