module [
    MultiDict,
    fromList,
    insert,
]

MultiDict k v : Dict k (List v)

# empty : {} -> MultiDict * *
# empty = \{} -> Dict.empty {}

# expect empty {} == Dict.empty {}

single : k, v -> MultiDict k v
single = \key, val -> Dict.single key [val]

expect single 0 1 == Dict.single 0 [1]

insert : MultiDict k v, k, v -> MultiDict k v
insert = \dict, key, val ->
    previous = Dict.get dict key |> Result.withDefault []
    Dict.insert dict key (List.append previous val)

expect insert (Dict.empty {}) 0 1 == Dict.single 0 [1]
expect insert (single 0 1) 0 2 == Dict.single 0 [1, 2]

fromList : List (k, v) -> MultiDict k v
fromList = \list ->
    List.walk list (Dict.empty {}) \state, (k, v) -> insert state k v

# expect fromList [] == Dict.empty {}
expect fromList [(0, 1)] == single 0 1
expect fromList [(0, 1), (0, 2), (3, 4)] == Dict.fromList [(0, [1, 2]), (3, [4])]
expect fromList [("a", 1), ("a", 2), ("b", 4)] == Dict.fromList [("a", [1, 2]), ("b", [4])]
