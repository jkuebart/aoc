app [main] {
    pf: platform "https://github.com/roc-lang/basic-cli/releases/download/0.17.0/lZFLstMUCUvd5bjnnpYromZJXkQUrdhbva4xdBInicE.tar.br",
}

import pf.Stdin
import pf.Stdout

import MultiDict

main =
    input =
        Stdin.readToEnd! {}
        |> List.splitOn linefeed
        |> List.dropLast 1

    # Compute the bounds of the input map.
    bounds = {
        min: { lat: 0, lon: 0 },
        max: {
            lat: List.len input |> Num.toI16,
            lon: List.first input |> Result.withDefault [] |> List.len |> Num.toI16,
        },
    }

    # Turn the input lines into a dict where each character is mapped to the
    # set of positions where it appears in the input.
    antennae =
        input
        |> List.mapWithIndex \line, lat ->
            List.mapWithIndex line \char, lon -> (char, { lat: Num.toI16 lat, lon: Num.toI16 lon })
        |> List.join
        |> MultiDict.fromList
        |> Dict.remove dot

    # For each pair of positions of each type of antennae, compute the
    # antinodes and keep the ones that are within bounds.
    antinodes =
        Dict.walk antennae (Set.empty {}) \state, _, positions ->
            combinations positions
            |> List.joinMap \(pos0, pos1) ->
                # Compute the antinodes of each pair of positions.
                diff = minus pos1 pos0
                [minus pos0 diff, plus pos1 diff]
            |> List.keepIf \pos -> withinBounds bounds pos
            |> List.walk state Set.insert

    Stdout.line "$(Num.toStr (Set.len antinodes)) antinodes."

linefeed = 0xa
dot = 0x2e

combinations : List a -> List (a, a)
combinations = \list ->
    List.mapWithIndex list \a, i ->
        List.map (List.dropFirst list (1 + i)) \b -> (a, b)
    |> List.join

expect combinations [] == []
expect combinations [1] == []
expect combinations [0, 1, 2] == [(0, 1), (0, 2), (1, 2)]

Pos a : { lat : Num a, lon : Num a }

map2 : Pos a, Pos b, (Num a, Num b -> Num c) -> Pos c
map2 = \{ lat: lat0, lon: lon0 }, { lat: lat1, lon: lon1 }, f -> { lat: f lat0 lat1, lon: f lon0 lon1 }

plus : Pos a, Pos a -> Pos a
plus = \pos0, pos1 -> map2 pos0 pos1 Num.add

minus : Pos a, Pos a -> Pos a
minus = \pos0, pos1 -> map2 pos0 pos1 Num.sub

withinBounds : { min : Pos a, max : Pos a }, Pos a -> Bool
withinBounds = \{ min, max }, { lat, lon } ->
    (min.lat <= lat && lat < max.lat) && (min.lon <= lon && lon < max.lon)
