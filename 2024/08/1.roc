app [main] {
    pf: platform "https://github.com/roc-lang/basic-cli/releases/download/0.17.0/lZFLstMUCUvd5bjnnpYromZJXkQUrdhbva4xdBInicE.tar.br",
}

import pf.Stdin
import pf.Stdout

import MultiDict

main =
    input =
        Stdin.readToEnd! {}
        |> List.splitOn linefeed
        |> List.dropLast 1

    # Compute the bounds of the input map.
    bounds = {
        min: { lat: 0, lon: 0 },
        max: {
            lat: List.len input |> Num.toI16,
            lon: List.first input |> Result.withDefault [] |> List.len |> Num.toI16,
        },
    }

    # Turn the input lines into a dict where each character is mapped to the
    # set of positions where it appears in the input.
    antennae =
        input
        |> List.mapWithIndex \line, lat ->
            List.mapWithIndex line \char, lon -> (char, { lat: Num.toI16 lat, lon: Num.toI16 lon })
        |> List.join
        |> MultiDict.fromList
        |> Dict.remove dot

    # Build a list of those positions that are in line with at least two
    # antennae of the same frequency.
    antinodes = keepIf bounds \pos ->
        Dict.walkUntil antennae Bool.false \_, _, positions ->
            combinations positions
            |> List.any \(pos0, pos1) ->
                diff0 = minus pos pos0
                diff1 = minus pos pos1
                diff0.lat * diff1.lon == diff0.lon * diff1.lat
            |> breakIf

    Stdout.line "$(Num.toStr (List.len antinodes)) antinodes."

linefeed = 0xa
dot = 0x2e

breakIf : Bool -> [Break Bool, Continue Bool]
breakIf = \b -> if b then Break b else Continue b

## Iterate all positions within the given bounds as a list.
keepIf : { min : Pos a, max : Pos a }, (Pos a -> Bool) -> List (Pos a)
keepIf = \{ min, max }, f ->
    List.range { start: At min.lat, end: Before max.lat }
    |> List.joinMap \lat ->
        List.range { start: At min.lon, end: Before max.lon }
        |> List.map \lon -> { lat, lon }
        |> List.keepIf f

combinations : List a -> List (a, a)
combinations = \list ->
    List.mapWithIndex list \a, i ->
        List.map (List.dropFirst list (1 + i)) \b -> (a, b)
    |> List.join

expect combinations [] == []
expect combinations [1] == []
expect combinations [0, 1, 2] == [(0, 1), (0, 2), (1, 2)]

Pos a : { lat : Num a, lon : Num a }

map2 : Pos a, Pos b, (Num a, Num b -> Num c) -> Pos c
map2 = \{ lat: lat0, lon: lon0 }, { lat: lat1, lon: lon1 }, f -> { lat: f lat0 lat1, lon: f lon0 lon1 }

minus : Pos a, Pos a -> Pos a
minus = \pos0, pos1 -> map2 pos0 pos1 Num.sub
