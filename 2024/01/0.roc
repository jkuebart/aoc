app [main] { pf: platform "https://github.com/roc-lang/basic-cli/releases/download/0.17.0/lZFLstMUCUvd5bjnnpYromZJXkQUrdhbva4xdBInicE.tar.br" }

import pf.Stdin
import pf.Stdout

import NumberPair exposing [unzip]

main =
    (first, second) = readNumbers! |> unzip
    diffs =
        List.map2 (List.sortAsc first) (List.sortAsc second) Num.sub
        |> List.map Num.abs
        |> List.walk 0 Num.add
    Stdout.line "distance $(Num.toStr diffs)"

readNumbers : Task (List (I64, I64)) _
readNumbers =
    when readLines |> Task.result! is
        Ok lines -> NumberPair.parse lines |> Task.fromResult
        Err err -> Task.err err

readLines : Task (List Str) _
readLines =
    Task.loop [] \list ->
        when Stdin.line |> Task.result! is
            Ok line -> Step (List.append list line) |> Task.ok
            Err (StdinErr EndOfFile) -> Done list |> Task.ok
            Err err -> Task.err err
