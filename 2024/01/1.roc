app [main] { pf: platform "https://github.com/roc-lang/basic-cli/releases/download/0.17.0/lZFLstMUCUvd5bjnnpYromZJXkQUrdhbva4xdBInicE.tar.br" }

import pf.Stdin
import pf.Stdout

import NumberPair exposing [unzip]

main =
    (first, second) = readNumbers! |> unzip
    counts = List.walk second (Dict.empty {}) \dict, num ->
        count = Result.withDefault (Dict.get dict num) 0
        Dict.insert dict num (1 + count)
    similarity = List.walk first 0 \sim, num ->
        sim + num * (Result.withDefault (Dict.get counts num) 0)
    Stdout.line "similarity $(Num.toStr similarity)"

readNumbers : Task (List (I64, I64)) _
readNumbers =
    when readLines |> Task.result! is
        Ok lines -> NumberPair.parse lines |> Task.fromResult
        Err err -> Task.err err

readLines : Task (List Str) _
readLines =
    Task.loop [] \list ->
        when Stdin.line |> Task.result! is
            Ok line -> Step (List.append list line) |> Task.ok
            Err (StdinErr EndOfFile) -> Done list |> Task.ok
            Err err -> Task.err err
