module [
    parse,
    unzip
]

parse : List Str -> Result (List (I64, I64)) [InvalidNumStr, NotFound]
parse = \strings ->
    List.walk strings (Ok []) \list, str ->
        when numberPair str is
            Ok p -> Result.map list \l -> List.append l p
            Err err -> Err err

expect parse [] == Ok []
expect parse ["", "2 3"] == Err NotFound
expect parse ["0 ", "2 3"] == Err InvalidNumStr
expect parse ["0 1", "2 3"] == Ok [(0, 1), (2, 3)]

numberPair : Str -> Result (I64, I64) [InvalidNumStr, NotFound]
numberPair = \input ->
    { before: lhs, after: rhs } = try Str.splitFirst input " "
    Ok (try Str.toI64 lhs, Str.trim rhs |> try Str.toI64)

expect numberPair "" == Err NotFound
expect numberPair " " == Err InvalidNumStr
expect numberPair "0 " == Err InvalidNumStr
expect numberPair " 1" == Err InvalidNumStr
expect numberPair "0  1" == Ok (0, 1)

unzip : List (a, b) -> (List a, List b)
unzip = \list ->
    List.walk list ([], []) \(first, second), (lhs, rhs) ->
        (List.append first lhs, List.append second rhs)

expect unzip [] == ([], [])
expect unzip [(0, 1), (2, 3)] == ([0, 2], [1, 3])
