app [main] {
    pf: platform "https://github.com/roc-lang/basic-cli/releases/download/0.17.0/lZFLstMUCUvd5bjnnpYromZJXkQUrdhbva4xdBInicE.tar.br",
}

import pf.Stdin
import pf.Stdout

import Matrix exposing [Matrix]
import Maze exposing [guardPos]
import Pos exposing [Pos]

main =
    maze = Stdin.readToEnd! {} |> Matrix.parse
    { dir: dir, pos: pos } = guardPos maze |> Task.fromResult!
    locations = guardWalk maze (Set.empty {}) pos dir
    Stdout.line "Guard visited $(Num.toStr (Set.len locations)) locations."

hash = 0x23

guardWalk : Matrix U8, Set (Pos I16), Pos I16, Pos I16 -> Set (Pos I16)
guardWalk = \maze, visited, pos, dir ->
    newVisited = Set.insert visited pos
    nextPos = Pos.add pos dir

    # Recursion ends when the next position is out of bounds.
    when Matrix.get maze nextPos is
        Err OutOfBounds -> newVisited
        Ok cell ->
            if hash == cell then
                guardWalk maze newVisited pos (Pos.turn dir)
            else
                guardWalk maze newVisited nextPos dir
