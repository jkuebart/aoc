app [main] {
    pf: platform "https://github.com/roc-lang/basic-cli/releases/download/0.17.0/lZFLstMUCUvd5bjnnpYromZJXkQUrdhbva4xdBInicE.tar.br",
}

import pf.Stdin
import pf.Stdout

import Matrix exposing [Matrix]
import Maze exposing [guardPos]
import Pos exposing [Pos]

main =
    maze = Stdin.readToEnd! {} |> Matrix.parse
    { dir: dir, pos: pos } = guardPos maze |> Task.fromResult!
    obstacleLocations =
        when guardWalk maze pos dir is
            Cyclic -> Set.empty {}
            Finite locations ->
                # Keep the positions along the guard's path that cause
                # cyclic walks.
                Set.keepIf locations \obstacle ->
                    guardWalk (Matrix.set maze obstacle hash) pos dir
                    |> isCyclic
    Stdout.line "$(Num.toStr (Set.len obstacleLocations)) obstacle locations cause cyclic walks."

## Whether the given walk is cyclic.
isCyclic : [Cyclic]_ -> Bool
isCyclic = \test ->
    when test is
        Cyclic -> Bool.true
        _ -> Bool.false

## Contains all visited positions along with the set of headings at those
## positions.
Visited a : Set { dir : Pos a, pos : Pos a }

guardWalk : Matrix U8, Pos I16, Pos I16 -> [Finite (Set (Pos I16)), Cyclic]
guardWalk = \maze, startPos, startDir ->
    walk : Visited I16, Pos I16, Pos I16 -> [Finite (Visited I16), Cyclic]
    walk = \visited, pos, dir ->
        newVisited = Set.insert visited { dir, pos }
        nextPos = Pos.add pos dir

        # Recursion ends when the walk is cyclic or the next position is out of
        # bounds.
        when cellType maze newVisited nextPos dir is
            Cyclic -> Cyclic
            Empty -> walk newVisited nextPos dir
            OutOfBounds -> Finite newVisited
            Wall -> walk newVisited pos (Pos.turn dir)

    when walk (Set.empty {}) startPos startDir is
        Cyclic -> Cyclic
        Finite locations -> Finite (Set.map locations .pos)

hash = 0x23

cellType : Matrix U8, Visited I16, Pos I16, Pos I16 -> [Cyclic, Empty, OutOfBounds, Wall]
cellType = \maze, visited, pos, dir ->
    # Recursion ends when the walk is cyclic.
    if Set.contains visited { dir, pos } then
        Cyclic
    else
        when Matrix.get maze pos is
            Err OutOfBounds -> OutOfBounds
            Ok cell ->
                if hash == cell then
                    Wall
                else
                    Empty
