module [
    Pos,
    map,
    add,
    turn,
]

Pos a : { lat : a, lon : a }

add : Pos (Num a), Pos (Num a) -> Pos (Num a)
add = \{ lat: lat0, lon: lon0 }, { lat: lat1, lon: lon1 } -> { lat: lat0 + lat1, lon: lon0 + lon1 }

expect add { lat: 0, lon: 1 } { lat: 1, lon: 0 } == { lat: 1, lon: 1 }

## Turn 90 degrees clockwise.
turn : Pos (Num a) -> Pos (Num a)
turn = \{ lat, lon } -> { lat: lon, lon: -lat }

expect turn { lat: 0, lon: 1 } == { lat: 1, lon: 0 }
expect turn { lat: 1, lon: 0 } == { lat: 0, lon: -1 }

map : Pos a, (a -> b) -> Pos b
map = \{ lat, lon }, f -> { lat: f lat, lon: f lon }
