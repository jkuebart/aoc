module [
    Matrix,
    get,
    parse,
    set,
]

import Pos exposing [Pos]

linefeed = 0xa

Matrix a : List (List a)

get : Matrix a, Pos (Int b) -> Result a [OutOfBounds]
get = \matrix, { lat: lat, lon: lon } ->
    if lat < 0 || lon < 0 then
        Err OutOfBounds
    else
        try List.get matrix (Num.toU64 lat) |> List.get (Num.toU64 lon)

expect get [] { lat: 0, lon: 0 } == Err OutOfBounds
expect get [[]] { lat: 0, lon: 0 } == Err OutOfBounds
expect get [[0]] { lat: -1, lon: 0 } == Err OutOfBounds
expect get [[0]] { lat: 0, lon: -1 } == Err OutOfBounds
expect get [[0]] { lat: 0, lon: 0 } == Ok 0
expect get [[0]] { lat: 1, lon: 0 } == Err OutOfBounds
expect get [[0]] { lat: 0, lon: 1 } == Err OutOfBounds
expect get [[0, 1], [2, 3]] { lat: 1, lon: 1 } == Ok 3

## Set the value at the given position. If the position is out of bounds,
## return the original matrix.
set : Matrix a, Pos (Int b), a -> Matrix a
set = \matrix, { lat: lat, lon: lon }, value ->
    if lat < 0 || lon < 0 then
        matrix
    else
        when List.get matrix (Num.toU64 lat) is
            Err OutOfBounds ->
                matrix

            Ok row ->
                List.set
                    matrix
                    (Num.toU64 lat)
                    (List.set row (Num.toU64 lon) value)

expect set [] { lat: 0, lon: 0 } 0 == []
expect set [[]] { lat: 0, lon: 0 } 0 == [[]]
expect set [[0]] { lat: 0, lon: 0 } 1 == [[1]]
expect set [[0, 1], [2, 3]] { lat: 1, lon: 0 } 4 == [[0, 1], [4, 3]]
expect set [[0, 1], [2, 3]] { lat: 0, lon: 1 } 4 == [[0, 4], [2, 3]]

parse : List U8 -> Matrix U8
parse = \bytes ->
    List.splitOn bytes linefeed
    |> List.dropLast 1
