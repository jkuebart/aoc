module [
    guardPos,
]

import Pos exposing [Pos]
import Matrix exposing [Matrix]

chevron = 0x5e

## Determine the position and heading of the guard in a given maze.
guardPos : Matrix U8 -> Result { dir : Pos I16, pos : Pos I16 } [NotFound, OutOfBounds]
guardPos = \maze ->
    lat = try List.findFirstIndex maze \line -> List.contains line chevron
    lon =
        try List.get maze lat
        |> try List.findFirstIndex \cell -> cell == chevron
    Ok {
        dir: { lat: -1, lon: 0 },
        pos: { lat: Num.toI16 lat, lon: Num.toI16 lon },
    }

expect guardPos [] == Err NotFound
expect guardPos [[]] == Err NotFound
expect guardPos [[0, 0, 0], [0, 0, chevron]] == Ok { dir: { lat: -1, lon: 0 }, pos: { lat: 1, lon: 2 } }
