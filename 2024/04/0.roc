app [main] {
    pf: platform "https://github.com/roc-lang/basic-cli/releases/download/0.17.0/lZFLstMUCUvd5bjnnpYromZJXkQUrdhbva4xdBInicE.tar.br",
}

import pf.Stdin
import pf.Stdout

main =
    lines = inputLines!
    rotations = [
        lines,
        transpose lines,
        diagonals lines,
        List.map lines List.reverse |> diagonals,
    ]
    count =
        rotations
        |> List.joinMap id
        |> List.joinMap \line -> [line, List.reverse line]
        |> List.map \line -> ocurrences line (Str.toUtf8 "XMAS")
        |> List.walk 0 Num.add
    Stdout.line "$(Num.toStr count) ocurrences"

Matrix a : List (List a)

## Determine the number of ocurrences of the substring.
ocurrences : List U8, List U8 -> U64
ocurrences = \str, del -> (List.splitOnList str del |> List.len) - 1

expect ocurrences [] [] == 0
expect ocurrences [0] [0] == 1
expect ocurrences [1, 0, 1] [0] == 1
expect ocurrences [0, 1, 0] [0] == 2

## Rotate a matrix of strings by 90°.
transpose : Matrix U8 -> Matrix U8
transpose = \lines ->
    List.range { start: At 0, end: Length (matrixWidth lines) }
    |> List.map \idx ->
        List.keepOks lines \line -> List.get line idx

expect transpose [] == []
expect transpose [[0]] == [[0]]
expect transpose [[0, 1], [2, 3]] == [[0, 2], [1, 3]]

## Rotate a matrix of strings by 45°.
##
## This implementation crashes with an integer overflow when passed an
## empty List. This is considered acceptable behaviour.
diagonals : Matrix U8 -> Matrix U8
diagonals = \lines ->
    width = matrixWidth lines
    List.range { start: At 0, end: Length (List.len lines + width - 1) }
    |> List.map \idx ->
        diagonal idx lines

expect diagonals [[]] == []
expect diagonals [[0, 1, 2, 3]] == [[0], [1], [2], [3]]
expect diagonals [[0], [1], [2], [3]] == [[0], [1], [2], [3]]
expect diagonals [[0, 1], [2, 3]] == [[0], [2, 1], [3]]

## Compute the given diagonal from the matrix.
diagonal : U64, Matrix U8 -> List U8
diagonal = \diag, lines ->
    List.range { start: At 0, end: At diag }
    |> List.keepOks \idx ->
        try List.get lines (diag - idx)
        |> try List.get idx
        |> Ok
    |> List.walk [] List.append

expect diagonal 0 [] == []
expect diagonal 0 [[]] == []
expect diagonal 0 [[0]] == [0]
expect diagonal 1 [[0, 1], [2, 3]] == [2, 1]
expect diagonal 2 [[0, 1, 2], [3, 4, 5], [6, 7, 8]] == [6, 4, 2]

# inputLines : Task (Matrix U8) _
inputLines =
    Stdin.readToEnd! {}
    |> List.splitOn 10
    |> List.dropIf List.isEmpty
    |> Task.ok

id : a -> a
id = \x -> x

## The maximum width of a matrix.
matrixWidth : Matrix a -> U64
matrixWidth = \lines ->
    List.map lines List.len
    |> List.max
    |> Result.withDefault 0
