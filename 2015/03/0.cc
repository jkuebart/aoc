#include "rng/all.h"

#include <sysexits.h>

#include <iostream>
#include <set>
#include <string>
#include <utility>
#include <vector>

using house = std::pair<int, int>;

static int Main(std::vector<std::string_view> args)
{
    if (2 != args.size()) {
        std::cerr << "usage: " << args[0] << R"( deliverers

    Simulate deliveries with the given number of deliverers.
)";
        return EX_USAGE;
    }

    std::string directions{};

    while (std::cin >> directions) {
        std::vector<house> deliverers(std::stoul(std::string{args[1]}));

        auto hs{rng::all(deliverers)};
        std::set<house> houses{hs.front()};
        for (auto const d : directions) {
            switch (d) {
            case '<':
                hs.front().first -= 1;
                break;
            case '>':
                hs.front().first += 1;
                break;
            case '^':
                hs.front().second -= 1;
                break;
            case 'v':
                hs.front().second += 1;
                break;
            }
            houses.insert(hs.front());
            hs.popFront();
            if (hs.empty()) {
                hs = rng::all(deliverers);
            }
        }

        std::cout << houses.size() << '\n';
    }

    return 0;
}

int main(int argc, char** argv)
{
    return Main(std::vector<std::string_view>(argv, argc + argv));
}
