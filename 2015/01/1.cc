#include <iostream>

int main()
{
    char c;
    unsigned int count{0};
    int floor{0};

    while (std::cin >> c) {
        count += 1;
        switch (c) {
        case ')':
            floor -= 1;
            if (floor < 0) {
                std::cout << count << '\n';
                return 0;
            }
            break;
        case '(':
            floor += 1;
            break;
        }
    }

    return 1;
}
