#include "streamer/stream.h"

#include <algorithm>
#include <iostream>

using streamer::stream;

namespace
{
class Box
{
public:
    Box() = default;

    explicit Box(unsigned int (&d)[3]) : mD{d[0], d[1], d[2]}
    {
        using std::begin;
        using std::end;

        std::sort(begin(mD), end(mD));
    }

    constexpr unsigned int area() const
    {
        return 2 * mD[0] * mD[1] + 2 * mD[0] * mD[2] + 2 * mD[1] * mD[2];
    }

    constexpr unsigned int smallest() const
    {
        return mD[0] * mD[1];
    }

    constexpr unsigned int volume() const
    {
        return mD[0] * mD[1] * mD[2];
    }

    constexpr unsigned int ribbon() const
    {
        return 2 * (mD[0] + mD[1]) + volume();
    }

    friend std::istream& operator>>(std::istream& is, Box& rhs)
    {
        unsigned int d[3]{};
        is >> stream(d, "x");
        rhs = Box{d};

        return is;
    }

private:
    unsigned int mD[3]{};
};
} // namespace

int main()
{
    unsigned int area{0};
    unsigned int ribbon{0};

    Box box{};
    while (std::cin >> box) {
        area += box.area() + box.smallest();
        ribbon += box.ribbon();
    }

    std::cout << "area=" << area << " ribbon=" << ribbon << '\n';
    return 0;
}
