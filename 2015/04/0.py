#!/usr/bin/env python3

import hashlib
import sys

for key in sys.stdin:
    i = 1
    while "000000" != hashlib.md5(b"%s%d" % (key[:-1].encode("ascii"), i)).hexdigest()[0:6]:
        i += 1
    print(i)
