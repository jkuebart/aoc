#ifndef GEO_DIRECTION_H
#define GEO_DIRECTION_H

#include "streamer/stream.h"

#include <algorithm>
#include <iostream>

namespace geo
{
template<std::size_t N, typename Rep = int>
struct Direction
{
    constexpr static std::size_t size()
    {
        return N;
    }

    constexpr Direction& rotateLeft()
    {
        std::rotate(x, x + N - 1, x + N);
        x[0] = -x[0];
        return *this;
    }

    constexpr Direction& rotateRight()
    {
        x[0] = -x[0];
        std::rotate(x, x + 1, x + N);
        return *this;
    }

    constexpr Rep& operator[](std::size_t const i)
    {
        return x[i];
    }

    constexpr Rep operator[](std::size_t const i) const
    {
        return x[i];
    }

    friend constexpr Direction operator-(Direction const& direction)
    {
        Direction res;
        for (std::size_t i{0}; i != N; ++i) {
            res.x[i] = -direction.x[i];
        }
        return res;
    }

    friend constexpr Direction operator+(
        Direction const& lhs,
        Direction const& rhs)
    {
        Direction res;
        for (std::size_t i{0}; i != N; ++i) {
            res.x[i] = lhs.x[i] + rhs.x[i];
        }
        return res;
    }

    friend constexpr Direction operator-(
        Direction const& lhs,
        Direction const& rhs)
    {
        Direction res;
        for (std::size_t i{0}; i != N; ++i) {
            res.x[i] = lhs.x[i] - rhs.x[i];
        }
        return res;
    }

    friend constexpr Direction operator*(
        Rep const scalar,
        Direction const& direction)
    {
        Direction res{direction};
        for (auto& component : res.x) {
            component *= scalar;
        }
        return res;
    }

    friend constexpr Direction operator*(
        Direction const& direction,
        Rep const scalar)
    {
        return scalar * direction;
    }

    friend constexpr bool operator==(Direction const& lhs, Direction const& rhs)
    {
        for (std::size_t i{0}; i != N; ++i) {
            if (lhs.x[i] != rhs.x[i]) {
                return false;
            }
        }
        return true;
    }

    friend constexpr bool operator!=(Direction const& lhs, Direction const& rhs)
    {
        return !(lhs == rhs);
    }

    friend constexpr Rep abs(Direction<N> const& direction)
    {
        Rep res{0};
        using std::abs;
        for (std::size_t i{0}; i != N; ++i) {
            res += abs(direction.x[i]);
        }
        return res;
    }

    friend std::istream& operator>>(std::istream& is, Direction& rhs)
    {
        return is >> streamer::stream(rhs.x, ",");
    }

    friend std::ostream& operator<<(std::ostream& os, Direction const& rhs)
    {
        return os << streamer::stream(rhs.x, ", ");
    }

    Rep x[N]{};
};
} // namespace geo

#endif // GEO_DIRECTION_H
