#ifndef GEO_BOUNDS_H
#define GEO_BOUNDS_H

#include "streamer/stream.h"

#include <algorithm>
#include <iostream>

namespace geo
{

/**
 * Bounds represent a rectangular area. They start out empty and can be
 * enlarged by including points.
 */

template<typename Point>
class Bounds
{
public:
    explicit constexpr Bounds(std::initializer_list<Point> const& points)
    {
        for (Point const& pnt : points) {
            include(pnt);
        }
    }

    constexpr Bounds& include(Point const& p)
    {
        for (auto i{std::size(p)}; 0 != i;) {
            --i;
            if (mBounds[0][i] == mBounds[1][i]) {
                mBounds[0][i] = p[i];
                mBounds[1][i] = 1 + p[i];
            } else {
                mBounds[0][i] = std::min(mBounds[0][i], p[i]);
                mBounds[1][i] = std::max(mBounds[1][i], 1 + p[i]);
            }
        }
        return *this;
    }

    constexpr Point const& lowerBound() const
    {
        return mBounds[0];
    }

    constexpr Point const& upperBound() const
    {
        return mBounds[1];
    }

    constexpr bool contains(Point const& point) const
    {
        for (auto i{std::size(point)}; 0 != i;) {
            --i;
            if (point[i] < mBounds[0][i] || mBounds[1][i] <= point[i]) {
                return false;
            }
        }
        return true;
    }

    constexpr unsigned int area() const
    {
        unsigned int r{1};
        for (auto i{std::size(mBounds[0])}; 0 != i;) {
            --i;
            r *= static_cast<unsigned int>(mBounds[1][i] - mBounds[0][i]);
        }
        return r;
    }

    constexpr Bounds inflate(int const amount) const
    {
        Bounds bnds{};
        for (auto i{std::size(mBounds[0])}; 0 != i;) {
            --i;
            if (mBounds[1][i] + std::min(0, 2 * amount) <= mBounds[0][i]) {
                return Bounds{};
            }
            bnds.mBounds[0][i] = mBounds[0][i] - amount;
            bnds.mBounds[1][i] = mBounds[1][i] + amount;
        }
        return bnds;
    }

    constexpr Bounds deflateUpper(int const amount) const
    {
        Bounds bnds{};
        for (auto i{std::size(mBounds[0])}; 0 != i;) {
            --i;
            if (mBounds[1][i] <= mBounds[0][i] + std::max(0, amount)) {
                return Bounds{};
            }
            bnds.mBounds[1][i] = mBounds[1][i] - amount;
        }
        return bnds;
    }

    constexpr Bounds deflate(int const amount) const
    {
        Bounds bnds{};
        for (auto i{std::size(mBounds[0])}; 0 != i;) {
            --i;
            if (mBounds[1][i] <= mBounds[0][i] + std::max(0, 2 * amount)) {
                return Bounds{};
            }
            bnds.mBounds[0][i] = mBounds[0][i] + amount;
            bnds.mBounds[1][i] = mBounds[1][i] - amount;
        }
        return bnds;
    }

    template<typename Func>
    constexpr Func each(Func func) const
    {
        for (Point p{mBounds[0]}; p[0] != mBounds[1][0];) {
            func(p);
            for (auto i{std::size(p)}; 0 != i;) {
                if (i < std::size(p)) {
                    p[i] = mBounds[0][i];
                }
                --i;
                ++p[i];
                if (p[i] != mBounds[1][i]) {
                    break;
                }
            }
        }
        return func;
    }

    template<typename Func>
    constexpr Func eachBoundaryPoint(Func func) const
    {
        // We iterate hyperplanes with one fixed dimension.
        for (auto i{std::size(mBounds[0])}; 0 != i;) {
            Point lower{mBounds[0]};
            Point upper{mBounds[1]};

            // Only components less than i cover the entire bounds.
            for (auto j{i}; j != std::size(lower); ++j) {
                ++lower[j];
                --upper[j];
            }
            --i;

            // A pointer to the opposite hyperplane, inclusive.
            decltype(Point{} - Point{}) opposite{};
            opposite[i] = mBounds[1][i] - mBounds[0][i] - 1;

            // The lowest component that varies on the hyperplane.
            auto const idx{0 == i ? 1u : 0u};
            for (Point p{lower}; p[idx] != upper[idx];) {
                func(p);
                func(p + opposite);

                for (auto j{std::size(p)}; idx != j;) {
                    if (i != j && j < std::size(p)) {
                        p[j] = lower[j];
                    }
                    --j;
                    if (i == j) {
                        continue;
                    }
                    ++p[j];

                    if (p[j] < upper[j]) {
                        break;
                    }
                }
            }
        }
        return func;
    }

    friend std::ostream& operator<<(std::ostream& os, Bounds const& rhs)
    {
        return os << streamer::stream(rhs.mBounds, ">:<", "<", ">");
    }

private:
    Point mBounds[2]{};
};
} // namespace geo

#endif // GEO_BOUNDS_H
