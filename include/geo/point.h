#ifndef GEO_POINT_H
#define GEO_POINT_H

#include <iostream>

namespace geo
{
template<typename Direction>
struct Point
{
    static constexpr std::size_t size()
    {
        return Direction::size();
    }

    /**
     * Invoke functor for all four adjacent points.
     */

    template<typename Func>
    constexpr Func adjacent(Func func) const
    {
        for (Direction step{1}; 0 <= step[0]; step.rotateLeft()) {
            func(*this + step);
            func(*this - step);
        }
        return func;
    }

    constexpr auto& operator[](std::size_t const i)
    {
        return x[i];
    }

    constexpr auto operator[](std::size_t const i) const
    {
        return x[i];
    }

    friend constexpr Point operator+(Point const& lhs, Direction const& rhs)
    {
        return {lhs.x + rhs};
    }

    friend constexpr Point operator-(Point const& lhs, Direction const& rhs)
    {
        return {lhs.x - rhs};
    }

    friend constexpr Direction operator-(Point const& lhs, Point const& rhs)
    {
        return lhs.x - rhs.x;
    }

    friend constexpr bool operator==(Point const& lhs, Point const& rhs)
    {
        return lhs.x == rhs.x;
    }

    friend constexpr bool operator!=(Point const& lhs, Point const& rhs)
    {
        return !(lhs == rhs);
    }

    friend constexpr bool operator<(Point const& lhs, Point const& rhs)
    {
        for (std::size_t i{0}; i != std::size(lhs.x); ++i) {
            if (lhs.x[i] != rhs.x[i]) {
                return lhs.x[i] < rhs.x[i];
            }
        }
        return false;
    }

    friend constexpr auto rectilinearDistance(
        Point const& lhs,
        Point const& rhs)
    {
        return abs(lhs - rhs);
    }

    friend std::istream& operator>>(std::istream& is, Point& rhs)
    {
        return is >> rhs.x;
    }

    friend std::ostream& operator<<(std::ostream& os, Point const& rhs)
    {
        return os << rhs.x;
    }

    Direction x{};
};
} // namespace geo

#endif // GEO_POINT_H
