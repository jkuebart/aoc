#ifndef RNG_SORT_H
#define RNG_SORT_H

#include "all.h"
#include "as.h"
#include "traits.h"

#include <algorithm>
#include <vector>

namespace rng
{

/**
 * Sort the elements of the range.
 */

struct Sort
{
    template<typename Range>
    friend constexpr auto operator|(Range&& range, Sort)
    {
        using std::begin;
        using std::end;
        using std::sort;

        auto elements{
            std::forward<Range>(range) | as<std::vector<ValueType<Range>>>};
        sort(begin(elements), end(elements));

        // Important: std::move() makes all() return an OwningRange.
        return all(std::move(elements));
    }
};

constexpr Sort sort{};
} // namespace rng

#endif // RNG_SORT_H
