#ifndef RNG_COUNT_H
#define RNG_COUNT_H

#include "countIf.h"

namespace rng
{
template<typename Value>
struct Count
{
    template<typename Range>
    friend constexpr std::size_t operator|(Range&& range, Count const& count)
    {
        return std::forward<Range>(range) |
            CountIf{[&count](auto const& elem)
                    {
                        return count.value == elem;
                    }};
    }

    Value value;
};

template<typename Value>
explicit Count(Value) -> Count<Value>;
} // namespace rng

#endif // RNG_COUNT_H
