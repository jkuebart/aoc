#ifndef RNG_RANGE_ITERATOR_H
#define RNG_RANGE_ITERATOR_H

#include "traits.h"

#include <cassert>
#include <iterator>
#include <type_traits>

namespace rng
{

/**
 * A largely STL-compatible iterator for a range.
 *
 * It's impossible to provide fully compliant iterators, operator ->() in
 * particular cannot be implemented in general as front() might produce
 * values.
 */

template<typename Range>
class RangeIterator
{
    constexpr RangeIterator(Range range, bool end)
    : mRange{std::move(range)}
    , mEnd{end}
    {}

public:
    using difference_type = std::ptrdiff_t;
    using value_type = ValueType<Range>;
    using reference = value_type&;
    using pointer = value_type*;

    /*
     * Since some ranges do most of their work in popFront(), it's
     * undesirable to traverse them multiple times. We default to the input
     * iterator category unless the range has a known size, in which case
     * it's desiblable to support operator -() for std::distance() during
     * container construction.
     */

    using iterator_category = std::conditional_t<
        hasSize<Range>,
        std::random_access_iterator_tag,
        std::input_iterator_tag>;

    constexpr explicit RangeIterator(Range range)
    : RangeIterator{std::move(range), false}
    {}

    constexpr decltype(auto) operator*() const
    {
        return mRange.front();
    }

    constexpr RangeIterator& operator++()
    {
        mRange.popFront();
        return *this;
    }

    friend constexpr RangeIterator begin(RangeIterator const& rng)
    {
        return rng;
    }

    friend constexpr RangeIterator end(RangeIterator const& rng)
    {
        return {rng.mRange, true};
    }

    friend constexpr bool operator==(
        RangeIterator const& lhs,
        RangeIterator const& rhs)
    {

        /*
         * Two iterators compare equal if
         *   - they are both end iterators, or
         *   - one is an end iterator and the other's range is empty.
         */

        return (
            lhs.mEnd == rhs.mEnd ? lhs.mEnd
                : lhs.mEnd       ? rhs.mRange.empty()
                                 : lhs.mRange.empty());
    }

    friend constexpr bool operator!=(
        RangeIterator const& lhs,
        RangeIterator const& rhs)
    {
        return !(lhs == rhs);
    }

    friend constexpr difference_type operator-(
        RangeIterator const& lhs,
        RangeIterator const& rhs)
    {
        assert(lhs.mEnd != rhs.mEnd);
        return (lhs.mEnd ? rhs.mRange.size() : -lhs.mRange.size());
    }

    friend constexpr Range all(RangeIterator const& rng)
    {
        return rng.mRange;
    }

private:
    Range mRange{};
    bool mEnd{false};
};

template<typename Range>
RangeIterator(Range) -> RangeIterator<Range>;

} // namespace rng

#endif // RNG_RANGE_ITERATOR_H
