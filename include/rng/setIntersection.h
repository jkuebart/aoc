#ifndef RNG_SET_INTERSECTION_H
#define RNG_SET_INTERSECTION_H

#include "tpl/anyOf.h"
#include "tpl/max.h"

#include <tuple>

namespace rng
{
template<typename... Ranges>
class SetIntersectionRange
{
public:
    SetIntersectionRange(Ranges... ranges) : mRanges(std::move(ranges)...)
    {
        next();
    }

    constexpr bool empty() const
    {
        return tpl::anyOf(
            mRanges,
            [](auto const& range)
            {
                return range.empty();
            });
    }

    constexpr decltype(auto) front() const
    {
        return std::get<0>(mRanges).front();
    }

    constexpr void popFront()
    {
        std::get<0>(mRanges).popFront();
        next();
    }

private:
    void next()
    {
        bool mpty{empty()};
        if (mpty) {
            return;
        }

        auto max{std::apply(
            [](auto const&... ranges)
            {
                return tpl::max(std::make_tuple(ranges.front()...));
            },
            mRanges)};
        using M = decltype(max);

        for (bool moving{true}; moving && !mpty;) {
            moving = tpl::anyOf(
                mRanges,
                [&mpty, &max](auto& range)
                {
                    while (range.front() < max) {
                        range.popFront();
                        if (range.empty()) {
                            mpty = true;
                            return true;
                        }
                    }

                    if (max < range.front()) {
                        max.~M();
                        new (&max) M{range.front()};
                        return true;
                    }
                    return false;
                });
        }
    }

    std::tuple<Ranges...> mRanges;
};

template<typename... Ranges>
SetIntersectionRange(Ranges...) -> SetIntersectionRange<Ranges...>;

template<typename... Ranges>
struct SetIntersectionWith
{
    constexpr explicit SetIntersectionWith(Ranges... rngs)
    : ranges(std::move(rngs)...)
    {}

    template<typename Range>
    friend constexpr auto operator|(
        Range&& range,
        SetIntersectionWith const& siw)
    {
        return std::apply(
            [&range](auto&&... rngs)
            {
                return SetIntersectionRange{
                    std::forward<Range>(range),
                    rngs...};
            },
            siw.ranges);
    }

    std::tuple<Ranges...> ranges;
};
} // namespace rng

#endif // RNG_SET_INTERSECTION_H
