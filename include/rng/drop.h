#ifndef RNG_DROP_H
#define RNG_DROP_H

namespace rng
{

/**
 * Drop the given number of elements from the start of the range.
 */

struct Drop
{
    template<typename Range>
    friend constexpr Range operator|(Range range, Drop const& drop)
    {
        for (std::size_t k{drop.n}; 0 != k && !range.empty(); --k) {
            range.popFront();
        }
        return range;
    }

    std::size_t n{};
};
} // namespace rng

#endif // RNG_DROP_H
