#ifndef RNG_AS_H
#define RNG_AS_H

#include "rangeIterator.h"

namespace rng
{
template<typename Container>
struct As
{
    template<typename Range>
    friend constexpr Container operator|(Range&& range, As)
    {
        RangeIterator const rangeIterator{std::forward<Range>(range)};
        return Container{begin(rangeIterator), end(rangeIterator)};
    }
};

/**
 * Transform a range to a container of the given type.
 */

template<typename Container>
constexpr As<Container> as{};
} // namespace rng

#endif // RNG_AS_H
