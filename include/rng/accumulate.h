#ifndef RNG_ACCUMULATE_H
#define RNG_ACCUMULATE_H

#include "each.h"
#include "plus.h"

namespace rng
{
template<typename Value, typename BinaryFunction>
struct Accumulate
{
    template<typename Range>
    friend constexpr auto operator|(Range&& range, Accumulate accumulate)
    {
        auto val{accumulate.value};
        std::forward<Range>(range) |
            Each{[&accumulate, &val](auto&& elem)
                 {
                     val = accumulate.func(
                         std::move(val),
                         std::forward<decltype(elem)>(elem));
                 }};
        return val;
    }

    Value value{};
    BinaryFunction func{};
};

template<typename Range, typename BinaryFunction = Plus>
explicit Accumulate(Range, BinaryFunction = {})
    -> Accumulate<Range, BinaryFunction>;
} // namespace rng

#endif // RNG_ACCUMULATE_H
