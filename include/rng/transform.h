#ifndef RNG_TRANSFORM_H
#define RNG_TRANSFORM_H

#include <utility>

namespace rng
{
template<typename Range, typename Func>
class TransformRange
{
public:
    constexpr TransformRange(Range range, Func func)
    : mRange{std::move(range)}
    , mFunc{std::move(func)}
    {}

    constexpr bool empty() const
    {
        return mRange.empty();
    }

    constexpr auto size() const
    {
        return mRange.size();
    }

    constexpr decltype(auto) front() const
    {
        return mFunc(mRange.front());
    }

    constexpr void popFront()
    {
        mRange.popFront();
    }

private:
    Range mRange{};
    Func mFunc{};
};

template<typename Range, typename Func>
TransformRange(Range, Func) -> TransformRange<Range, Func>;

/**
 * Return the result of applying the function to each element of the range.
 */

template<typename Func>
struct Transform
{
    template<typename Range>
    friend constexpr auto operator|(Range&& range, Transform const& transform)
    {
        return TransformRange{std::forward<Range>(range), transform.func};
    }

    Func func{};
};

template<typename Func>
explicit Transform(Func) -> Transform<Func>;
} // namespace rng

#endif // RNG_TRANSFORM_H
