#ifndef RNG_ALL_H
#define RNG_ALL_H

#include <memory>
#include <type_traits>

namespace rng
{
template<typename Iterator, typename Sentinel = Iterator>
class IteratorRange
{
public:
    constexpr IteratorRange(Iterator iterator, Sentinel sentinel)
    : mIterator{std::move(iterator)}
    , mSentinel{std::move(sentinel)}
    {}

    constexpr bool empty() const
    {
        return mIterator == mSentinel;
    }

    constexpr auto size() const
    {
        return std::distance(mIterator, mSentinel);
    }

    constexpr decltype(auto) front() const
    {
        return *mIterator;
    }

    constexpr void popFront()
    {
        ++mIterator;
    }

private:
    Iterator mIterator{};
    Sentinel mSentinel{};
};

template<typename Iterator, typename Sentinel = Iterator>
IteratorRange(Iterator, Sentinel) -> IteratorRange<Iterator, Sentinel>;

template<typename Owner, typename Iterator, typename Sentinel = Iterator>
class OwningRange : public IteratorRange<Iterator, Sentinel>
{
    using Base = IteratorRange<Iterator, Sentinel>;

public:
    OwningRange(Owner&& owner, Iterator iterator, Sentinel sentinel)
    : Base{std::move(iterator), std::move(sentinel)}
    , mOwner{std::move(owner)}
    {}

private:
    Owner mOwner{};
};

template<typename Owner, typename Iterator, typename Sentinel = Iterator>
OwningRange(Owner&&, Iterator, Sentinel)
    -> OwningRange<Owner, Iterator, Sentinel>;

/**
 * Return a range representing all elements of the container. If the
 * container is an rvalue, it is moved to the heap and becomes collectively
 * owned by the ranges referring to it.
 *
 * @param cont a container that owns its elements and supports `begin()`
 *             and `end()`.
 * @return a range representing all of the container's elements which owns
 *         the elements if `cont` is an rvalue.
 */

template<typename Container>
constexpr auto all(Container&& cont)
{
    using std::begin;
    using std::end;

    if constexpr (std::is_lvalue_reference_v<Container>) {
        return IteratorRange{begin(cont), end(cont)};
    } else {
        auto holder{std::make_shared<Container>(std::move(cont))};
        const auto first{begin(*holder)};
        const auto last{end(*holder)};
        return OwningRange{std::move(holder), first, last};
    }
}
} // namespace rng

#endif // RNG_ALL_H
