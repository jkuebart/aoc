#ifndef RNG_FILTER_H
#define RNG_FILTER_H

namespace rng
{
template<typename Range, typename Pred>
class FilterRange
{
public:
    constexpr FilterRange(Range rng, Pred pred)
    : mRng{std::move(rng)}
    , mPred{std::move(pred)}
    {
        filter();
    }

    constexpr bool empty() const
    {
        return mRng.empty();
    }

    constexpr decltype(auto) front() const
    {
        return mRng.front();
    }

    constexpr void popFront()
    {
        mRng.popFront();
        filter();
    }

private:
    constexpr void filter()
    {
        while (!mRng.empty() && !mPred(mRng.front())) {
            mRng.popFront();
        }
    }

    Range mRng{};
    Pred mPred{};
};

template<typename Range, typename Pred>
FilterRange(Range, Pred) -> FilterRange<Range, Pred>;

/**
 * Return a range of all elements which satisfy the predicate.
 */

template<typename Pred>
struct Filter
{
    template<typename Range>
    friend constexpr auto operator|(Range&& range, Filter const& filter)
    {
        return FilterRange{std::forward<Range>(range), filter.pred};
    }

    Pred pred{};
};

template<typename Pred>
explicit Filter(Pred) -> Filter<Pred>;
} // namespace rng

#endif // RNG_FILTER_H
