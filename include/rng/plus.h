#ifndef RNG_PLUS_H
#define RNG_PLUS_H

#include <utility>

namespace rng
{
struct Plus
{
    template<typename T, typename U>
    constexpr auto operator()(T&& t, U&& u)
    {
        return std::forward<T>(t) + std::forward<U>(u);
    }
};
} // namespace rng

#endif // RNG_PLUS_H
