#ifndef RNG_MIN_ELEMENT_H
#define RNG_MIN_ELEMENT_H

#include "less.h"

namespace rng
{

/**
 * Reduce the range until the front is its first maximal element.
 */

template<typename Compare>
struct MinElement
{
    template<typename Range>
    friend constexpr auto operator|(Range range, MinElement minElement)
    {
        if (range.empty()) {
            return range;
        }
        Range minimum{range};
        range.popFront();
        for (; !range.empty(); range.popFront()) {
            if (minElement.compare(range.front(), minimum.front())) {
                minimum.~Range();
                new (&minimum) Range{range};
            }
        }
        return minimum;
    }

    Compare compare{};
};

template<typename Compare = Less>
explicit MinElement(Compare = {}) -> MinElement<Compare>;

constexpr MinElement minElement{};
} // namespace rng

#endif // RNG_MIN_ELEMENT_H
