#ifndef RNG_ALL_OF_H
#define RNG_ALL_OF_H

namespace rng
{
template<typename Pred>
struct AllOf
{
    template<typename Range>
    friend constexpr bool operator|(Range range, AllOf allOf)
    {
        for (; !range.empty(); range.popFront()) {
            if (!allOf.pred(range.front())) {
                return false;
            }
        }
        return true;
    }

    Pred pred;
};

template<typename Pred>
explicit AllOf(Pred) -> AllOf<Pred>;
} // namespace rng

#endif // RNG_ALL_OF_H
