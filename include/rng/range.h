#ifndef RNG_RANGE_H
#define RNG_RANGE_H

namespace rng
{

/**
 * A range-for-loop compatible iterator for a range.
 */

template<typename Rng>
struct Range
{
    struct Sentinel
    {};

    constexpr decltype(auto) operator*() const
    {
        return range.front();
    }

    constexpr Range& operator++()
    {
        range.popFront();
        return *this;
    }

    friend constexpr Range begin(Range const& rng)
    {
        return rng;
    }

    friend constexpr Sentinel end(Range const&)
    {
        return {};
    }

    friend constexpr bool operator==(Range const& lhs, Sentinel)
    {
        return lhs.range.empty();
    }

    friend constexpr bool operator!=(Range const& lhs, Sentinel)
    {
        return !lhs.range.empty();
    }

    friend constexpr bool operator==(Sentinel, Range const& rhs)
    {
        return rhs.range.empty();
    }

    friend constexpr bool operator!=(Sentinel, Range const& rhs)
    {
        return !rhs.range.empty();
    }

    friend constexpr std::ptrdiff_t operator-(Sentinel, Range const& rhs)
    {
        return rhs.size();
    }

    friend constexpr Rng all(Range const& rng)
    {
        return rng.range;
    }

    Rng range{};
};

template<typename Rng>
explicit Range(Rng) -> Range<Rng>;
} // namespace rng

#endif // RNG_RANGE_H
