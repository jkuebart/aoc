#ifndef RNG_ANY_OF_H
#define RNG_ANY_OF_H

namespace rng
{
template<typename Pred>
struct AnyOf
{
    template<typename Range>
    friend constexpr bool operator|(Range range, AnyOf anyOf)
    {
        for (; !range.empty(); range.popFront()) {
            if (anyOf.pred(range.front())) {
                return true;
            }
        }
        return false;
    }

    Pred pred;
};

template<typename Pred>
explicit AnyOf(Pred) -> AnyOf<Pred>;
} // namespace rng

#endif // RNG_ANY_OF_H
