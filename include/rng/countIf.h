#ifndef RNG_COUNT_IF_H
#define RNG_COUNT_IF_H

#include "filter.h"
#include "size.h"

namespace rng
{
template<typename Pred>
struct CountIf
{
    template<typename Range>
    friend constexpr std::size_t operator|(
        Range&& range,
        CountIf const& countIf)
    {
        return std::forward<Range>(range) | Filter{countIf.pred} | size;
    }

    Pred pred;
};

template<typename Pred>
explicit CountIf(Pred) -> CountIf<Pred>;
} // namespace rng

#endif // RNG_COUNT_IF_H
