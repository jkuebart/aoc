#ifndef RNG_SIZE_H
#define RNG_SIZE_H

#include "traits.h"

namespace rng
{
struct Size
{
    template<typename Range>
    friend constexpr std::size_t operator|(Range range, Size)
    {
        if constexpr (hasSize<Range>) {
            return range.size();
        } else {
            std::size_t size{0};
            for (; !range.empty(); range.popFront()) {
                size += 1;
            }
            return size;
        }
    }
};

constexpr Size size{};
} // namespace rng

#endif // RNG_SIZE_H
