#ifndef RNG_TRAITS_H
#define RNG_TRAITS_H

#include <type_traits>

namespace rng
{
namespace impl
{
    template<typename>
    std::false_type hasSize(...);

    template<typename Range>
    std::true_type hasSize(decltype(&Range::size));
} // namespace impl

template<typename Range>
constexpr bool hasSize{decltype(impl::hasSize<Range>(nullptr))::value};

template<typename Range>
using ValueType =
    std::remove_reference_t<decltype(std::declval<Range>().front())>;
} // namespace rng

#endif // RNG_TRAITS_H
