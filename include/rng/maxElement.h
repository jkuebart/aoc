#ifndef RNG_MAX_ELEMENT_H
#define RNG_MAX_ELEMENT_H

#include "less.h"

namespace rng
{

/**
 * Reduce the range until the front is its first maximal element.
 */

template<typename Compare>
struct MaxElement
{
    template<typename Range>
    friend constexpr auto operator|(Range range, MaxElement maxElement)
    {
        if (range.empty()) {
            return range;
        }
        Range maximum{range};
        range.popFront();
        for (; !range.empty(); range.popFront()) {
            if (maxElement.compare(maximum.front(), range.front())) {
                maximum.~Range();
                new (&maximum) Range{range};
            }
        }
        return maximum;
    }

    Compare compare{};
};

template<typename Compare = Less>
explicit MaxElement(Compare = {}) -> MaxElement<Compare>;

constexpr MaxElement maxElement{};
} // namespace rng

#endif // RNG_MAX_ELEMENT_H
