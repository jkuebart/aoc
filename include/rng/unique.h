#ifndef RNG_UNIQUE_H
#define RNG_UNIQUE_H

#include "all.h"
#include "as.h"

#include <unordered_set>

namespace rng
{
template<typename Range>
class UniqueRange
{
public:
    constexpr explicit UniqueRange(Range range) : mRange{std::move(range)} {}

    constexpr bool empty() const
    {
        return mRange.empty();
    }

    constexpr std::size_t size() const
    {
        return mRange.size();
    }

    constexpr decltype(auto) front() const
    {
        return mRange.front();
    }

    constexpr void popFront()
    {
        decltype(auto) previous{front()};
        mRange.popFront();

        while (!mRange.empty() && previous == mRange.front()) {
            mRange.popFront();
        }
    }

private:
    Range mRange{};
};

/**
 * Remove runs of identical elements.
 */

struct Unique
{
    template<typename Range>
    friend constexpr auto operator|(Range rng, Unique)
    {
        return UniqueRange{std::move(rng)};
    }
};

template<typename Range>
UniqueRange(Range) -> UniqueRange<Range>;

constexpr Unique unique{};
} // namespace rng

#endif // RNG_UNIQUE_H
