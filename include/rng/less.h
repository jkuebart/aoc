#ifndef RNG_LESS_H
#define RNG_LESS_H

#include <utility>

namespace rng
{
struct Less
{
    template<typename T, typename U>
    constexpr auto operator()(T&& t, U&& u)
    {
        return std::forward<T>(t) < std::forward<U>(u);
    }
};
} // namespace rng

#endif // RNG_LESS_H
