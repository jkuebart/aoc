#ifndef RNG_EACH_H
#define RNG_EACH_H

namespace rng
{
template<typename Func>
struct Each
{
    template<typename Range>
    friend constexpr Range operator|(Range const& range, Each each)
    {
        for (auto rng{range}; !rng.empty(); rng.popFront()) {
            each.func(rng.front());
        }
        return range;
    }

    Func func;
};

template<typename Func>
explicit Each(Func) -> Each<Func>;
} // namespace rng

#endif // RNG_EACH_H
