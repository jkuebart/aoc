#ifndef DEVICE_PROCESSOR_H
#define DEVICE_PROCESSOR_H

#include <array>
#include <cassert>
#include <string_view>

namespace device
{
using Instruction = std::array<int, 4>;

template<typename Registers>
struct Opcode
{
    constexpr static int& reg(int const i, Registers& registers)
    {
        std::size_t const ui{static_cast<std::size_t>(i)};
        assert(ui < registers.size());
        return registers[ui];
    }

    constexpr static int reg(int const i, Registers const& registers)
    {
        return reg(i, const_cast<Registers&>(registers));
    }

    template<int (&op)(int, int)>
    constexpr static Registers imm_imm(
        Registers const& before,
        Instruction const& instruction)
    {
        Registers res{before};
        reg(instruction[3], res) = op(instruction[1], instruction[2]);
        return res;
    }

    template<int (&op)(int, int)>
    constexpr static Registers imm_reg(
        Registers const& before,
        Instruction const& instruction)
    {
        Registers res{before};
        reg(instruction[3], res) =
            op(instruction[1], reg(instruction[2], before));
        return res;
    }

    template<int (&op)(int, int)>
    constexpr static Registers reg_imm(
        Registers const& before,
        Instruction const& instruction)
    {
        Registers res{before};
        reg(instruction[3], res) =
            op(reg(instruction[1], before), instruction[2]);
        return res;
    }

    template<int (&op)(int, int)>
    constexpr static Registers reg_reg(
        Registers const& before,
        Instruction const& instruction)
    {
        Registers res{before};
        reg(instruction[3], res) =
            op(reg(instruction[1], before), reg(instruction[2], before));
        return res;
    }

    static Opcode const opcodes[16];

    std::string_view mnemonic{};
    Registers (&operation)(Registers const&, Instruction const&);
};

int op_add(int lhs, int rhs);
int op_and(int lhs, int rhs);
int op_assign(int lhs, int);
int op_eq(int lhs, int rhs);
int op_gt(int lhs, int rhs);
int op_mul(int lhs, int rhs);
int op_or(int lhs, int rhs);

template<typename Registers>
Opcode<Registers> const Opcode<Registers>::opcodes[]{
    {"eqri", reg_imm<op_eq>},
    {"mulr", reg_reg<op_mul>},
    {"gtri", reg_imm<op_gt>},
    {"gtrr", reg_reg<op_gt>},
    {"banr", reg_reg<op_and>},
    {"addi", reg_imm<op_add>},
    {"seti", imm_imm<op_assign>},
    {"gtir", imm_reg<op_gt>},
    {"muli", reg_imm<op_mul>},
    {"bori", reg_imm<op_or>},
    {"setr", reg_imm<op_assign>},
    {"addr", reg_reg<op_add>},
    {"bani", reg_imm<op_and>},
    {"borr", reg_reg<op_or>},
    {"eqir", imm_reg<op_eq>},
    {"eqrr", reg_reg<op_eq>},
};
} // namespace device

#endif // DEVICE_PROCESSOR_H
