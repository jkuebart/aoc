#ifndef DEVICE_PROGRAM_H
#define DEVICE_PROGRAM_H

#include "device/processor.h"
#include "rng/all.h"
#include "rng/drop.h"
#include "streamer/stream.h"

#include <cassert>
#include <iostream>
#include <map>
#include <string>
#include <vector>

namespace device
{
template<typename Registers>
struct Program
{
    using TOpcode = Opcode<Registers>;

    static Program assemble(std::istream& is);

    Registers run(Registers registers = {}, bool log = false) const;

    std::size_t const regIp{std::size(Registers{})};
    std::vector<Instruction> const instructions{};
};

template<typename Registers>
Program<Registers> Program<Registers>::assemble(std::istream& is)
{

    // Create a table of mnemonics to opcodes.

    std::map<std::string_view, int> mnemonics{};
    for (int opcode{0};
         static_cast<std::size_t>(opcode) != std::size(TOpcode::opcodes);
         ++opcode)
    {
        mnemonics.emplace(TOpcode::opcodes[opcode].mnemonic, opcode);
    }

    // Read optional instruction pointer assignment.

    std::size_t regIp{std::size(Registers{})};
    if ('#' == is.peek()) {
        std::string str{};
        is >> str;
        assert("#ip" == str);
        is >> regIp;
        assert(regIp < std::size(Registers{}));
    }

    // Read instruction sequence.

    std::vector<Instruction> instructions{};
    Instruction instruction{};
    std::string str{};
    while (is >> str) {
        assert(end(mnemonics) != mnemonics.find(str));
        instruction[0] = mnemonics[str];
        is >> streamer::streamRange(
                  rng::all(instruction) | rng::Drop{1},
                  " ",
                  "",
                  "\n");
        instructions.push_back(instruction);
    }

    return {regIp, std::move(instructions)};
}

template<typename Registers>
Registers Program<Registers>::run(Registers registers, bool const log) const
{

    // Run the instructions.

    for (std::size_t ip{0}; ip < std::size(instructions); ++ip) {
        Instruction const& instruction{instructions[ip]};

        int const opcode{instruction[0]};
        assert(static_cast<std::size_t>(opcode) < std::size(TOpcode::opcodes));

        if (regIp < std::size(registers)) {
            registers[regIp] =
                static_cast<std::decay_t<decltype(registers[regIp])>>(ip);
        }

        if (log) {
            std::cout << "ip=" << ip << ' '
                      << streamer::stream(registers, " ", "[", "]") << ' '
                      << TOpcode::opcodes[opcode].mnemonic << ' '
                      << streamer::streamRange(
                             rng::all(instruction) | rng::Drop{1});
        }

        registers = TOpcode::opcodes[opcode].operation(registers, instruction);

        if (regIp < std::size(registers)) {
            ip = static_cast<std::size_t>(registers[regIp]);
        }

        if (log) {
            std::cout << ' ' << streamer::stream(registers, " ", "[", "]")
                      << '\n';
        }
    }

    return registers;
}
} // namespace device

#endif // DEVICE_PROGRAM_H
