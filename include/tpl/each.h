#ifndef TPL_EACH_H
#define TPL_EACH_H

#include <tuple>

namespace tpl
{
namespace detail
{
    template<typename Func>
    constexpr Func eachImpl(Func func)
    {
        return func;
    }

    template<typename Func, typename Arg0, typename... Tuple>
    constexpr Func eachImpl(Func func, Arg0&& arg0, Tuple&&... tuple)
    {
        func(std::forward<Arg0>(arg0));
        return eachImpl(func, std::forward<Tuple>(tuple)...);
    }
} // namespace detail

template<typename Tuple, typename Func>
constexpr Func each(Tuple&& tuple, Func func)
{
    return std::apply(
        [&func](auto&&... tpl)
        {
            return detail::eachImpl(func, std::forward<decltype(tpl)>(tpl)...);
        },
        std::forward<Tuple>(tuple));
}
} // namespace tpl

#endif // TPL_EACH_H
