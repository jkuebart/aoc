#ifndef TPL_MAX_H
#define TPL_MAX_H

#include <tuple>

namespace tpl
{
namespace detail
{
    template<typename Arg0>
    constexpr decltype(auto) maxImpl(Arg0 const& arg0)
    {
        return arg0;
    }

    template<typename Arg0, typename Arg1, typename... Tuple>
    constexpr decltype(auto)
    maxImpl(Arg0 const& arg0, Arg1 const& arg1, Tuple const&... tuple)
    {
        decltype(auto) remainder{maxImpl(arg1, tuple...)};
        return remainder < arg0 ? arg0 : remainder;
    }
} // namespace detail

template<typename Tuple>
constexpr auto max(Tuple&& tuple)
{
    return std::apply(
        [](auto&&... tpl)
        {
            return detail::maxImpl(std::forward<decltype(tpl)>(tpl)...);
        },
        std::forward<Tuple>(tuple));
}
} // namespace tpl

#endif // TPL_MAX_H
