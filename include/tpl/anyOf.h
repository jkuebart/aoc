#ifndef TPL_ANY_OF_H
#define TPL_ANY_OF_H

#include <tuple>

namespace tpl
{
namespace detail
{
    template<typename Pred>
    constexpr bool anyOfImpl(Pred)
    {
        return false;
    }

    template<typename Pred, typename Arg0, typename... Tuple>
    constexpr bool anyOfImpl(Pred pred, Arg0&& arg0, Tuple&&... tuple)
    {
        return pred(std::forward<Arg0>(arg0)) ||
            anyOfImpl(pred, std::forward<Tuple>(tuple)...);
    }
} // namespace detail

template<typename Tuple, typename Pred>
constexpr bool anyOf(Tuple&& tuple, Pred pred)
{
    return std::apply(
        [&pred](auto&&... tpl)
        {
            return detail::anyOfImpl(pred, std::forward<decltype(tpl)>(tpl)...);
        },
        std::forward<Tuple>(tuple));
}
} // namespace tpl

#endif // TPL_ANY_OF_H
