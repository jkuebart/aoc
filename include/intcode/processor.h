#ifndef INTCODE_PROCESSOR_H
#define INTCODE_PROCESSOR_H

#include <cstdint>
#include <iosfwd>
#include <vector>

namespace intcode
{
class Processor
{
public:
    Processor& run();
    void set(std::intmax_t i, std::intmax_t v, int mode = 0);
    std::intmax_t get(std::intmax_t i) const;

    bool reading() const;
    Processor& input(std::vector<std::intmax_t> input);
    std::vector<std::intmax_t> output();

    friend std::istream& operator>>(std::istream& is, Processor& rhs);
    friend std::ostream& operator<<(std::ostream& os, Processor const& rhs);

private:
    std::intmax_t param(std::intmax_t i, int mode) const;

    std::vector<std::intmax_t> mInput{};
    std::vector<std::intmax_t> mOutput{};
    std::vector<std::intmax_t> mMemory{};
    int mIp{0};
    int mBase{0};
};
} // namespace intcode

#endif // INTCODE_PROCESSOR_H
