#ifndef STREAMER_STREAM_H
#define STREAMER_STREAM_H

#include "rng/all.h"
#include "rng/range.h"
#include "tpl/each.h"

#include <ios>
#include <iostream>
#include <string_view>
#include <utility>

namespace streamer
{
std::istream& consume(std::istream& is, std::string_view const& str);

template<typename Range>
struct Streamer
{
    friend std::istream& operator>>(std::istream& is, Streamer&& rhs)
    {
        consume(is, rhs.left);

        std::string_view sp{};
        for (auto rng{rhs.range}; is && !rng.empty(); rng.popFront()) {
            consume(is, sp);
            is >> rng.front();
            sp = rhs.sep;
        }

        return consume(is, rhs.right);
    }

    friend std::ostream& operator<<(std::ostream& os, Streamer const& rhs)
    {
        os << rhs.left;
        std::string_view sp{};
        for (auto const& elem : rng::Range{rhs.range}) {
            os << sp << elem;
            sp = rhs.sep;
        }
        return os << rhs.right;
    }

    Range const range;
    std::string_view const sep;
    std::string_view const left;
    std::string_view const right;
};

template<typename Range>
Streamer<Range> streamRange(
    Range range,
    std::string_view const& sep = " ",
    std::string_view const& left = {},
    std::string_view const& right = {})
{
    return Streamer<Range>{std::move(range), sep, left, right};
}

template<typename From, typename To>
auto streamFromTo(
    From from,
    To to,
    std::string_view const& sep = " ",
    std::string_view const& left = {},
    std::string_view const& right = {})
{
    return streamRange(
        rng::IteratorRange{std::move(from), std::move(to)},
        sep,
        left,
        right);
}

template<typename Container>
auto stream(
    Container&& cont,
    std::string_view const& sep = " ",
    std::string_view const& left = {},
    std::string_view const& right = {})
{
    return streamRange(
        rng::all(std::forward<Container>(cont)),
        sep,
        left,
        right);
}

/**
 * A TupleStreamer can capture an rvalue reference (for streaming results),
 * an lvalue reference (for reading input) or a const lvalue reference for
 * unmodifiable lvalues.
 */

template<typename Tuple>
struct TupleStreamer
{
    friend std::ostream& operator<<(std::ostream& os, TupleStreamer const& rhs)
    {
        os << rhs.left;
        std::string_view sp{};
        tpl::each(
            rhs.tuple,
            [&os, &rhs, &sp](auto const& elem)
            {
                os << sp << elem;
                sp = rhs.sep;
            });
        return os << rhs.right;
    }

    Tuple&& tuple;
    std::string_view const sep;
    std::string_view const left;
    std::string_view const right;
};

/*
 * We need an overload for every reference type to beat the forwarding
 * reference above.
 */

template<typename... Tuple>
auto stream(
    std::tuple<Tuple...> const& tuple,
    std::string_view const& sep = " ",
    std::string_view const& left = {},
    std::string_view const& right = {})
{
    return TupleStreamer<std::tuple<Tuple...> const&>{tuple, sep, left, right};
}

template<typename T, typename U>
auto stream(
    std::pair<T, U> const& p,
    std::string_view const& sep = " ",
    std::string_view const& left = {},
    std::string_view const& right = {})
{
    return TupleStreamer<std::pair<T, U> const&>{p, sep, left, right};
}

template<typename... Tuple>
auto stream(
    std::tuple<Tuple...>& tuple,
    std::string_view const& sep = " ",
    std::string_view const& left = {},
    std::string_view const& right = {})
{
    return TupleStreamer<std::tuple<Tuple...>&>{tuple, sep, left, right};
}

template<typename T, typename U>
auto stream(
    std::pair<T, U>& p,
    std::string_view const& sep = " ",
    std::string_view const& left = {},
    std::string_view const& right = {})
{
    return TupleStreamer<std::pair<T, U>&>{p, sep, left, right};
}

template<typename... Tuple>
auto stream(
    std::tuple<Tuple...>&& tuple,
    std::string_view const& sep = " ",
    std::string_view const& left = {},
    std::string_view const& right = {})
{
    return TupleStreamer<std::tuple<Tuple...>>{
        std::move(tuple),
        sep,
        left,
        right};
}

template<typename T, typename U>
auto stream(
    std::pair<T, U>&& p,
    std::string_view const& sep = " ",
    std::string_view const& left = {},
    std::string_view const& right = {})
{
    return TupleStreamer<std::pair<T, U>>{std::move(p), sep, left, right};
}
} // namespace streamer

#endif // STREAMER_STREAM_H
