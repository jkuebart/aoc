#ifndef STREAMER_GETLINES
#define STREAMER_GETLINES

#include <iostream>
#include <string>
#include <vector>

namespace streamer
{
template<typename CharT, typename Traits>
std::vector<std::basic_string<CharT, Traits>> getlines(
    std::basic_istream<CharT, Traits>& is,
    CharT const delim)
{
    std::vector<std::basic_string<CharT, Traits>> lines{};
    for (std::basic_string<CharT, Traits> line{};
         std::getline(is, line, delim);) {
        lines.push_back(std::move(line));
    }
    return lines;
}

template<typename CharT, typename Traits>
std::vector<std::basic_string<CharT, Traits>> getlines(
    std::basic_istream<CharT, Traits>& is)
{
    return getlines(is, is.widen('\n'));
}
} // namespace streamer

#endif // STREAMER_GETLINES
