#ifndef MATHS_EUCLID_H
#define MATHS_EUCLID_H

#include <tuple>

namespace maths
{

/**
 * The extended Euclidean algorithm.
 *
 * The return value contains
 *   - the greatest common divisor gcd
 *   - the quotients p = a / gcd, q = b / gcd
 *   - Bezout coefficients a * s + b * t = gcd
 */
template<typename Int>
constexpr auto euclid(Int a, Int b)
{
    struct Euclid
    {
        Int gcd, p, q, s, t;
    };

    // Invariants (where a0 and b0 denote their initial values):
    //
    //   - a = s0 * a0 + t0 * b0
    //
    //   - b = s * a0 + t * b0
    //
    // These are easily established by
    //      s0, t0 <- 1, 0
    //      s, t <- 0, 1
    Int s0{1}, t0{0};
    Int s{0}, t{1};

    while (0 != b) {
        Int const q{a / b};

        // a' <- b
        // b' <- a - q * b
        std::tie(a, b) = std::tuple{b, a - q * b};

        // a' = b = s * a0 + t * b0
        //      s0' <- s
        //      t0' <- t
        //
        // b' = a - q * b = s0 * a0 + t0 * b0 - q * s * a0 - q * t * b0
        //                = (s0 - q * s) * a0 + (t0 - q * t) * b0:
        //      s' <- s0 - q * s
        //      t' <- t0 - q * t
        std::tie(s0, s) = std::tuple{s, s0 - q * s};
        std::tie(t0, t) = std::tuple{t, t0 - q * t};
    }

    return Euclid{a, t, s, s0, t0};
}
} // namespace maths

#endif // MATHS_EUCLID_H
