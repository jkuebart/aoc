#ifndef MATHS_GCD_H
#define MATHS_GCD_H

namespace maths
{
template<typename Int>
constexpr Int gcd(Int a, Int b)
{
    while (0 != a && 0 != b) {
        if (a <= b) {
            b %= a;
        } else {
            a %= b;
        }
    }
    return a + b;
}
} // namespace maths

#endif // MATHS_GCD_H
