#ifndef MATHS_INVERSE_H
#define MATHS_INVERSE_H

#include "maths/euclid.h"

namespace maths
{
/**
 * @return The multiplicative inverse of a (modulo n).
 */
template<typename Int>
constexpr Int inverse(Int const a, Int const n)
{
    return (n + euclid(a, n).s) % n;
}
} // namespace maths

#endif // MATHS_INVERSE_H
