Advent of Code
==============

This repository contains solutions to some [Advent of Code][AOC] puzzles.
Most code is written in »modern« C++.

Building
--------

The source code can be built using [SCons][SCONS].

    scons

Files are built in subdirectories of `.obj`.

The build mode can be specified as `release` (the default) or `debug` on
the command line. Multiple build modes can be given as one or more `MODES`
arguments.

    scons MODES=debug

In order to clean the `.obj` directory, use

    scons -c MODES=all


[AOC]: https://adventofcode.com/
[SCONS]: https://scons.org/
