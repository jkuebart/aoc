#include "intcode/processor.h"
#include "streamer/stream.h"

#include <iostream>
#include <string>
#include <vector>

using intcode::Processor;
using streamer::stream;

int main()
{
    Processor processor{};
    while (std::cin >> processor) {
        char const springscript[]{
            "NOT A J\n"
            "NOT B T\n"
            "OR T J\n"
            "NOT C T\n"
            "OR T J\n"
            "AND D J\n"
            "WALK\n"};
        processor.input({std::begin(springscript), std::end(springscript)});
        processor.run();
        for (auto c : processor.output()) {
            if (c == static_cast<char>(c)) {
                std::cerr << static_cast<char>(c);
            } else {
                std::cout << c << '\n';
            }
        }
        std::cerr << '\n';
    }
    return 0;
}
