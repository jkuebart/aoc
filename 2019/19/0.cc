#include "intcode/processor.h"
#include "streamer/stream.h"

#include <iostream>
#include <string>
#include <vector>

using intcode::Processor;
using streamer::stream;

int main()
{
    std::vector<std::string> beam{};
    Processor processor{};
    while (std::cin >> processor) {
        unsigned count{0};
        for (int y{0}; y != 50; ++y) {
            beam.push_back("");
            for (int x{0}; x != 50; ++x) {
                Processor query{processor};
                query.input({x, y});
                query.run();
                if (query.output().back()) {
                    beam.back().push_back('#');
                    ++count;
                } else {
                    beam.back().push_back('.');
                }
            }
        }
        std::cerr << stream(beam, "\n") << '\n';
        std::cout << count << '\n';
    }
    return 0;
}
