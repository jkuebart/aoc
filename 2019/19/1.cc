#include "geo/direction.h"
#include "geo/point.h"
#include "intcode/processor.h"

#include <iostream>

using intcode::Processor;

using Direction = geo::Direction<2>;
using Point = geo::Point<Direction>;

constexpr Direction kExtentDown{0, 100};
constexpr Direction kExtentLeft{100, 0};
constexpr Direction kLeft{-1, 0};
constexpr Direction kDown{0, 1};

int main()
{
    Processor processor{};
    while (std::cin >> processor) {
        auto const query{[&processor](Point const& point)
                         {
                             Processor proc{processor};
                             proc.input({point[0], point[1]});
                             proc.run();
                             return 0 != proc.output().back();
                         }};

        // Ignore upper left area where the beam is too thin.
        for (Point point{10, 0};; ++point[0]) {
            // The point diagonally traces the top edge of the beam.
            while (!query(point + kDown + kLeft)) {
                ++point[1];
            }

            if (query(point - kExtentLeft + kExtentDown)) {
                std::cout << (point + kDown - kExtentLeft) << '\n';
                break;
            }
        }
    }
    return 0;
}
