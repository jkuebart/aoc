#!/usr/bin/env python3

import sys

orbits = {}
def add(pair):
    if pair[0] not in orbits:
        orbits[pair[0]] = []
    orbits[pair[0]].append(pair[1])

# Build a full graph of orbits.
for line in sys.stdin:
    pair = line.rstrip().split(")")
    add(pair)
    add(tuple(reversed(pair)))

# Find shortest path from YOU to SAN.
seen = {"YOU": 0}
visited = set()
while "SAN" not in seen:
    current = min(seen.items(), key=lambda p: p[1])
    del seen[current[0]]
    visited.add(current[0])
    cost = 1 + current[1]
    for edge in orbits[current[0]]:
        if edge not in visited and (edge not in seen or cost < seen[edge]):
            seen[edge] = cost

print(seen["SAN"] - 2)
