#!/usr/bin/env python3

import sys

orbits = dict(reversed(line.rstrip().split(")")) for line in sys.stdin)

def orbited(b):
    """A sequence of directly and indirectly orbited bodies."""
    while b in orbits:
        b = orbits[b]
        yield b

print(sum(1 for b in orbits.keys() for _ in orbited(b)))
