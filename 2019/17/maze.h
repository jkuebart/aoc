#ifndef MAZE_H
#define MAZE_H

#include "geo/bounds.h"
#include "geo/direction.h"
#include "geo/point.h"

#include <cstdint>
#include <iosfwd>
#include <string>
#include <vector>

namespace maze
{
struct Robot
{
    using Direction = ::geo::Direction<2>;
    using Point = ::geo::Point<Direction>;

    Point location{};
    Direction heading{};

    friend std::ostream& operator<<(std::ostream& os, Robot const& robot);
};

class Maze
{
public:
    using Direction = Robot::Direction;
    using Point = Robot::Point;
    using Bounds = ::geo::Bounds<Point>;

    Maze(std::vector<std::intmax_t> const& output);

    Bounds const& bounds() const;
    Robot const& robot() const;
    char get(Point const& point) const;
    char operator[](Point const& point) const;

    /**
     * Find scaffolding in a direction that isn't the opposite of the
     * robot's current heading.
     */
    Direction scaffold(Robot const& robot) const;

    friend std::ostream& operator<<(std::ostream& os, Maze const& maze);

private:
    std::vector<std::string> mMaze{};
    Bounds mBounds{};
    Robot mRobot{};
};
} // namespace maze

#endif // MAZE_H
