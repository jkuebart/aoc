#include "maze.h"

#include "intcode/processor.h"
#include "rng/all.h"
#include "rng/transform.h"

#include <iostream>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <string>
#include <string_view>
#include <tuple>
#include <vector>

using intcode::Processor;
using maze::Maze;
using maze::Robot;
using streamer::streamRange;

using Direction = Robot::Direction;

constexpr unsigned kLength{20};
constexpr unsigned kRoutines{3};

namespace
{
bool startsWith(std::string_view const& string, std::string_view const& start)
{
    return string.substr(0, start.size()) == start;
}

/**
 * Left turns are represented by "RRR" to avoid missing optimisation
 * opportunities which require splitting a single turn between two
 * subroutines.
 *
 * @return A command string for the change of direction.
 */
std::string turnCommand(Direction const& from, Direction const& to)
{
    if (from == to) {
        return "";
    }
    if (Direction{from}.rotateLeft() == to) {
        return "R";
    }
    if (Direction{from}.rotateRight() == to) {
        return "RRR";
    }
    return "RR";
}

/**
 * @param commands A string of 'F' and 'R'.
 * @return Instructions suitable as input for the processor.
 */
std::string instructionsForCommands(std::string_view const& commands)
{
    std::ostringstream os{};
    char const* sep = "";
    for (unsigned i{0}; i != commands.size();) {
        os << sep;
        sep = ",";

        unsigned count{0};
        for (; i != commands.size() && 'F' == commands[i]; ++i) {
            ++count;
        }
        if (0 != count) {
            os << count;
        } else if ("RRR" == commands.substr(i, 3)) {
            os << 'L';
            i += 3;
        } else {
            os << commands[i];
            ++i;
        }
    }
    return os.str();
}

struct Solution
{
    /**
     * @return The main routine followed by the subroutines separated by
     *         newlines. Suitable as input to the processor.
     */
    std::string input() const
    {
        std::ostringstream os{};
        os << main << '\n'
           << streamRange(
                  rng::all(routines) | rng::Transform{instructionsForCommands},
                  "\n")
           << '\n';
        return os.str();
    }

    friend bool operator<(Solution const& lhs, Solution const& rhs)
    {
        std::size_t const leftRemaining{lhs.program.size()};
        std::size_t const rightRemaining{rhs.program.size()};
        return std::tie(rightRemaining, rhs.main) <
            std::tie(leftRemaining, lhs.main);
    }

    friend std::ostream& operator<<(std::ostream& os, Solution const& solution)
    {
        return os << "remaining=" << solution.program.size() << '\n'
                  << "program: " << instructionsForCommands(solution.program)
                  << '\n'
                  << solution.input();
    }

    std::string_view program{};
    std::string main{};
    std::vector<std::string_view> routines{};
};
} // namespace

int main()
{
    Processor processor{};
    while (std::cin >> processor) {
        processor.set(0, 2);
        processor.run();
        Maze const maze{processor.output()};
        Robot robot{maze.robot()};
        std::cerr << maze << '\n' << "robot=" << robot << '\n';

        // Find path along the scaffolding.

        std::string commands{};
        for (;;) {
            while ('.' != maze[robot.location + robot.heading]) {
                robot.location = robot.location + robot.heading;
                commands.push_back('F');
            }
            auto const nextHeading{maze.scaffold(robot)};
            if (Direction{0, 0} == nextHeading) {
                break;
            }
            commands.append(turnCommand(robot.heading, nextHeading));
            robot.heading = nextHeading;
        }
        std::cerr << commands << '\n';

        // Split commands into subroutines.

        std::string input{};
        std::priority_queue<Solution> seen{{}, {{Solution{{commands}}}}};
        std::set<Solution> visited{};
        while (!seen.empty()) {
            Solution const current{seen.top()};
            seen.pop();

            // Ignore duplicates in priority queue.

            if (0 != visited.count(current)) {
                continue;
            }
            visited.insert(current);
            std::cerr << "current:\n" << current << '\n';

            // Ignore solutions that exceed the maximum length for main and
            // therefore also cannot be refined further.

            if (kLength < current.main.size()) {
                continue;
            }

            // Stop if solution is complete.

            if (current.program.empty()) {
                input = current.input();
                std::cout << "Program:\n" << input;
                break;
            }

            auto const routine{std::find_if(
                current.routines.begin(),
                current.routines.end(),
                [&current](std::string_view const& r)
                {
                    return startsWith(current.program, r);
                })};
            if (current.routines.end() != routine) {

                // If one of the routines matches the beginning of the
                // remaining program, create a solution with the routine
                // applied.

                Solution solution{current};
                solution.program = current.program.substr(routine->size());
                solution.main.push_back(',');
                solution.main.push_back(
                    'A' +
                    static_cast<char>(routine - current.routines.begin()));
                seen.push(solution);

            } else if (current.routines.size() < kRoutines) {

                // Create a new routine for every prefix of the remaining
                // program, up to the maximum viable subroutine length.

                for (unsigned last{0}; last != current.program.size() &&
                     instructionsForCommands(
                         current.program.substr(0, 1 + last))
                             .size() <= kLength;
                     ++last)
                {
                    Solution solution{current};
                    solution.program = current.program.substr(1 + last);
                    if (!solution.main.empty()) {
                        solution.main.push_back(',');
                    }
                    solution.main.push_back(
                        'A' + static_cast<char>(solution.routines.size()));
                    solution.routines.push_back(
                        current.program.substr(0, 1 + last));
                    seen.push(solution);
                }
            }
        }

        // Run the program.

        input.append("n\n");
        processor.input({input.begin(), input.end()});
        processor.run();

        auto output{processor.output()};
        std::cout << "dust=" << output.back() << '\n';

        output.pop_back();
        std::cerr << std::string{output.begin(), output.end()};
    }
    return 0;
}
