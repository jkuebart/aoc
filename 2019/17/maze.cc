#include "maze.h"

#include "streamer/stream.h"

using streamer::stream;

namespace maze
{
using Direction = Robot::Direction;

constexpr Direction kDown{0, 1};
constexpr Direction kLeft{-1, 0};
constexpr Direction kRight{1, 0};
constexpr Direction kUp{0, -1};

std::ostream& operator<<(std::ostream& os, Robot const& robot)
{
    return os << "{location=" << robot.location << ", heading=" << robot.heading
              << '}';
}

Maze::Maze(std::vector<std::intmax_t> const& output)
{
    mMaze.push_back("");
    for (auto const ch : output) {
        if ('\n' == ch) {
            mMaze.push_back("");
        } else {
            mMaze.back().push_back(static_cast<char>(ch));
            Point const here{
                static_cast<int>(mMaze.back().size() - 1),
                static_cast<int>(mMaze.size() - 1)};
            mBounds.include(here);
            switch (ch) {
            case '<':
                mRobot = {here, kLeft};
                break;
            case '>':
                mRobot = {here, kRight};
                break;
            case '^':
                mRobot = {here, kUp};
                break;
            case 'v':
                mRobot = {here, kDown};
                break;
            }
        }
    }
}

Maze::Bounds const& Maze::bounds() const
{
    return mBounds;
}

Robot const& Maze::robot() const
{
    return mRobot;
}

char Maze::get(Point const& point) const
{
    auto const x{static_cast<unsigned>(point[0])};
    auto const y{static_cast<unsigned>(point[1])};
    if (y < mMaze.size() && x < mMaze[y].size()) {
        return mMaze[y][x];
    }
    return '.';
}

char Maze::operator[](Point const& point) const
{
    return get(point);
}

Direction Maze::scaffold(Robot const& robot) const
{
    Direction heading{0, 0};
    robot.location.adjacent(
        [this, &robot, &heading](Point const& pnt)
        {
            if ('.' != get(pnt) && (robot.location - pnt) != robot.heading) {
                heading = pnt - robot.location;
            }
        });
    return heading;
}

std::ostream& operator<<(std::ostream& os, Maze const& maze)
{
    return os << stream(maze.mMaze, "\n");
}
} // namespace maze
