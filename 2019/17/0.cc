#include "maze.h"

#include "intcode/processor.h"

#include <iostream>

using intcode::Processor;
using maze::Maze;

using Point = Maze::Point;

int main()
{
    Processor processor{};
    while (std::cin >> processor) {
        processor.run();
        Maze const maze{processor.output()};
        std::cerr << maze << '\n';
        int checksum{0};
        maze.bounds().deflate(1).each(
            [&checksum, &maze](Point const& point)
            {
                if ('.' != maze[point]) {
                    bool intersection{true};
                    point.adjacent(
                        [&intersection, &maze](Point const& pnt)
                        {
                            if ('.' == maze[pnt]) {
                                intersection = false;
                            }
                        });
                    if (intersection) {
                        checksum += point[0] * point[1];
                    }
                }
            });
        std::cout << checksum << '\n';
    }
    return 0;
}
