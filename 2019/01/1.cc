#include <cstdint>
#include <iostream>

int main()
{
    std::intmax_t sum{0};
    std::intmax_t v;
    while (std::cin >> v) {
        while (6 <= v) {
            v = (v / 3) - 2;
            sum += v;
        }
    }
    std::cout << sum << '\n';
    return 0;
}
