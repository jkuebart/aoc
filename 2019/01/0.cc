#include <cassert>
#include <cstdint>
#include <iostream>

int main()
{
    std::intmax_t sum{0};
    std::intmax_t v;
    while (std::cin >> v) {
        assert(6 <= v);
        sum += (v / 3) - 2;
    }
    std::cout << sum << '\n';
    return 0;
}
