#include "geo/direction.h"
#include "geo/point.h"
#include "streamer/stream.h"

#include <algorithm>
#include <iostream>
#include <set>
#include <string>
#include <vector>

using streamer::stream;

using Direction = geo::Direction<2>;
using Point = geo::Point<Direction>;

namespace
{
struct Heading
{
    friend bool operator<(Heading const& lhs, Heading const& rhs)
    {
        if (0 == rhs.heading[0] && rhs.heading[1] <= 0) {
            return false;
        }
        if (0 == lhs.heading[0] && lhs.heading[1] <= 0) {
            return true;
        }
        if (0 <= rhs.heading[0] && lhs.heading[0] < 0) {
            return false;
        }
        if (rhs.heading[0] < 0 && 0 <= lhs.heading[0]) {
            return true;
        }
        // lhs[1] / lhs[0] < rhs[1] / rhs[0]
        return lhs.heading[1] * rhs.heading[0] <
            rhs.heading[1] * lhs.heading[0];
    }

    Direction heading{};
};
} // namespace

int main()
{
    std::vector<Point> asteroids{};
    int y{0};
    for (std::string line; std::getline(std::cin, line);) {
        int x{0};
        for (auto ch : line) {
            if ('#' == ch) {
                asteroids.push_back({x, y});
            }
            ++x;
        }
        ++y;
    }

    // Find the asteroid with the view of the most asteroids.

    Point bestBase{0, 0};
    std::size_t maxVisible{0};
    for (Point const& base : asteroids) {
        std::set<Heading> headings{};
        for (Point const& asteroid : asteroids) {
            if (&base != &asteroid) {
                headings.insert({asteroid - base});
            }
        }
        if (maxVisible < headings.size()) {
            std::cerr << base << ": " << headings.size() << '\n';
            bestBase = base;
            maxVisible = headings.size();
        }
    }
    std::cout << "base=" << bestBase << " visible=" << maxVisible << '\n';

    // Remove the base asteroid and order the remaining asteroids clockwise
    // with increasing distance from the base.

    asteroids.erase(std::find(asteroids.begin(), asteroids.end(), bestBase));
    std::sort(
        asteroids.begin(),
        asteroids.end(),
        [bestBase](Point const& lhs, Point const& rhs)
        {
            Heading const h0{lhs - bestBase};
            Heading const h1{rhs - bestBase};
            if (h0 < h1) {
                return true;
            }
            if (h1 < h0) {
                return false;
            }
            return abs(h0.heading) < abs(h1.heading);
        });

    // Iterate clockwise, vaporising one asteroid in each direction per
    // round.

    std::size_t const targetSize{
        std::max(std::size_t{199}, asteroids.size()) - 199};
    auto current{asteroids.begin()};
    while (targetSize < asteroids.size()) {
        Heading const heading{*current - bestBase};
        current = asteroids.erase(current);
        while (asteroids.end() != current &&
               !(heading < Heading{*current - bestBase})) {
            ++current;
        }
        if (asteroids.end() == current) {
            current = asteroids.begin();
        }
    }
    if (asteroids.end() != current) {
        std::cout << "vaporised=" << *current << '\n';
    }

    return 0;
}
