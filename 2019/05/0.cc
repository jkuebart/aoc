#include "intcode/processor.h"
#include "rng/all.h"
#include "rng/as.h"
#include "rng/drop.h"
#include "rng/transform.h"
#include "streamer/stream.h"

#include <iostream>
#include <string>
#include <string_view>
#include <vector>

using streamer::stream;

namespace
{
int Main(std::vector<std::string_view> args)
{
    auto const input{
        rng::all(args) | rng::Drop{1} |
        rng::Transform{[](auto const& arg)
                       {
                           return std::stoi(std::string{arg});
                       }} |
        rng::as<std::vector<std::intmax_t>>};
    intcode::Processor processor{};
    processor.input(input);
    while (std::cin >> processor) {
        std::cerr << "program " << processor << '\n';
        processor.run();
        std::cout << stream(processor.output(), "\n") << '\n';
        std::cerr << "result " << processor << '\n';
    }
    return 0;
}
} // namespace

int main(int argc, char** argv)
{
    return Main(std::vector<std::string_view>{argv, argc + argv});
}
