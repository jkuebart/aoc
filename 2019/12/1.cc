#include "moon.h"

#include "maths/gcd.h"
#include "rng/accumulate.h"
#include "rng/all.h"
#include "streamer/stream.h"

#include <iostream>
#include <iterator>
#include <vector>

using maths::gcd;
using moon::Moon;
using streamer::stream;

namespace
{
template<typename Rng>
std::uintmax_t lcm(Rng numbers)
{
    return numbers |
        rng::Accumulate{
            std::uintmax_t{1},
            [](std::uintmax_t const p, std::uintmax_t const n)
            {
                return (p / gcd(p, n)) * n;
            }};
}
} // namespace

int main()
{
    std::vector<Moon> const moons{
        std::istream_iterator<Moon>(std::cin),
        std::istream_iterator<Moon>()};

    // Find cycles in each coordinate independently.

    unsigned cycles[decltype(Moon::position)::size()]{};
    for (unsigned k{0}; k != moons[0].position.size(); ++k) {
        std::vector<Moon> simulation{moons};

        unsigned cycle{0};
        while (0 == cycle || moons != simulation) {
            // Apply gravity.

            for (unsigned i{0}; 1 + i != simulation.size(); ++i) {
                for (unsigned j{1 + i}; j != simulation.size(); ++j) {
                    if (simulation[i].position[k] < simulation[j].position[k]) {
                        ++simulation[i].velocity[k];
                        --simulation[j].velocity[k];
                    } else if (
                        simulation[j].position[k] < simulation[i].position[k]) {
                        ++simulation[j].velocity[k];
                        --simulation[i].velocity[k];
                    }
                }
            }

            // Apply velocity.

            for (auto& moon : simulation) {
                moon = moon.advance();
            }
            ++cycle;
        }
        cycles[k] = cycle;
    }

    std::cout << "cycles=" << stream(cycles) << " lcm=" << lcm(rng::all(cycles))
              << '\n';
    return 0;
}
