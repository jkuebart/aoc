#ifndef MOON_H
#define MOON_H

#include "geo/direction.h"
#include "geo/point.h"

#include <iosfwd>

namespace moon
{
struct Moon
{
    using Direction = ::geo::Direction<3>;
    using Point = ::geo::Point<Direction>;

    Moon advance() const;
    int energy() const;

    friend bool operator==(Moon const& lhs, Moon const& rhs);
    friend std::istream& operator>>(std::istream& is, Moon& moon);
    friend std::ostream& operator<<(std::ostream& os, Moon const& moon);

    Point position{};
    Direction velocity{};
};
} // namespace moon

#endif // MOON_H
