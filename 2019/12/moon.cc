#include "moon.h"

#include <string>

namespace moon
{
Moon Moon::advance() const
{
    return Moon{position + velocity, velocity};
}

int Moon::energy() const
{
    return abs(position.x) * abs(velocity);
}

bool operator==(Moon const& lhs, Moon const& rhs)
{
    return lhs.position == rhs.position && lhs.velocity == rhs.velocity;
}

std::istream& operator>>(std::istream& is, Moon& moon)
{
    std::string const prefix[]{"<x=", " y=", " z="};
    char const suffix[]{',', ',', '\n'};

    for (unsigned i{0}; i != moon.position.size(); ++i) {
        std::string line{};
        if (!std::getline(is, line, suffix[i]) ||
            prefix[i] != line.substr(0, prefix[i].size()))
        {
            is.setstate(std::istream::failbit);
            return is;
        }
        moon.position[i] = std::stoi(line.substr(prefix[i].size()));
    }
    return is;
}

std::ostream& operator<<(std::ostream& os, Moon const& moon)
{
    return os << "pos=" << moon.position << " vel=" << moon.velocity;
}
} // namespace moon
