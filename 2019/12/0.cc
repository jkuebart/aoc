#include "moon.h"

#include "streamer/stream.h"

#include <sysexits.h>

#include <iostream>
#include <iterator>
#include <numeric>
#include <string>
#include <string_view>
#include <vector>

using moon::Moon;
using streamer::stream;

namespace
{
int Main(std::vector<std::string_view> args)
{
    if (2 != args.size()) {
        std::cerr << "usage: " << args[0] << R"( steps

    Read planet coordinates from standard input and simulate the given
    number of steps.
)";
        return EX_USAGE;
    }

    std::vector<Moon> moons{
        std::istream_iterator<Moon>(std::cin),
        std::istream_iterator<Moon>()};
    for (int step{std::stoi(std::string{args[1]})}; step != 0; --step) {
        std::cerr << "moons=" << moons.size() << '\n'
                  << stream(moons, "\n") << "\n\n";

        // Apply gravity.

        for (unsigned i{0}; 1 + i != moons.size(); ++i) {
            for (unsigned j{1 + i}; j != moons.size(); ++j) {
                for (unsigned k{0}; k != moons[i].position.size(); ++k) {
                    if (moons[i].position[k] < moons[j].position[k]) {
                        ++moons[i].velocity[k];
                        --moons[j].velocity[k];
                    } else if (moons[j].position[k] < moons[i].position[k]) {
                        ++moons[j].velocity[k];
                        --moons[i].velocity[k];
                    }
                }
            }
        }

        // Apply velocity.

        for (auto& moon : moons) {
            moon = moon.advance();
        }
    }
    int const energy{std::accumulate(
        moons.begin(),
        moons.end(),
        0,
        [](int e, Moon const& moon)
        {
            return e + moon.energy();
        })};
    std::cout << "energy=" << energy << '\n' << stream(moons, "\n") << '\n';
    return 0;
}
} // namespace

int main(int argc, char** argv)
{
    return Main(std::vector<std::string_view>{argv, argc + argv});
}
