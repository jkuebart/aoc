#include "streamer/stream.h"

#include <sysexits.h>

#include <iostream>
#include <string>
#include <string_view>
#include <vector>

using streamer::stream;

namespace
{
int Main(std::vector<std::string_view> args)
{
    if (2 != args.size()) {
        std::cerr << "usage: " << args[0] << R"( repeats

    Read a signal from standard input and apply 100 phases of Flawed
    Frequency Transmission, repeating the signal the given number of times.
)";
        return EX_USAGE;
    }

    unsigned long const repeats{std::stoul(std::string{args[1]})};
    for (std::string line{}; std::getline(std::cin, line);) {
        unsigned const offset{
            static_cast<unsigned>(std::stoul(line.substr(0, 7)))};

        std::string digits{};
        digits.reserve(repeats * line.size() - offset % line.size());
        digits.append(line, offset % line.size());
        while (offset + digits.size() < repeats * line.size()) {
            digits.append(line);
        }
        for (unsigned phase{0}; phase != 100; ++phase) {
            std::cerr << "Phase " << phase << ": "
                      << digits.substr(0, line.size()) << '\n';
            std::vector<int> partials(digits.size(), 0);
            for (std::size_t i{digits.size()}; 0 != i;) {
                --i;
                partials[i] = digits[i] - '0';
                if (1 + i < digits.size()) {
                    partials[i] += partials[1 + i];
                }
            }
            auto const sumFromTo =
                [&partials](unsigned const i, unsigned const j)
            {
                if (partials.size() <= i) {
                    return 0;
                }
                if (partials.size() <= j) {
                    return partials[i];
                }
                return partials[i] - partials[j];
            };
            for (unsigned i{0}; i != digits.size(); ++i) {
                // Length of the runs for the current target digit.
                unsigned const runLength{1 + i + offset};
                int sum{0};
                for (unsigned j{i}; j < partials.size();) {
                    sum += sumFromTo(j, j + runLength);
                    j += 2 * runLength;
                    sum -= sumFromTo(j, j + runLength);
                    j += 2 * runLength;
                }
                digits[i] = '0' + std::abs(sum) % 10;
            }
        }
        std::cout << "offset=" << offset << " message=" << digits.substr(0, 8)
                  << '\n';
    }

    return 0;
}
} // namespace

int main(int argc, char** argv)
{
    return Main(std::vector<std::string_view>{argv, argc + argv});
}
