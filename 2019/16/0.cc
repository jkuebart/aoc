#include <iostream>
#include <string>
#include <vector>

namespace
{
constexpr int kFft[]{0, 1, 0, -1};

int getFft(unsigned i, unsigned j)
{
    return kFft[((1 + j) / (1 + i)) % std::size(kFft)];
}
} // namespace

int main()
{
    for (std::string line{}; std::getline(std::cin, line);) {
        for (unsigned phase{0}; phase != 100; ++phase) {
            for (unsigned i{0}; i != line.size(); ++i) {
                int sum{0};
                for (unsigned j{0}; j != line.size(); ++j) {
                    sum += (line[j] - '0') * getFft(i, j);
                }
                line[i] = '0' + std::abs(sum) % 10;
            }
        }
        std::cout << line << '\n';
    }

    return 0;
}
