#!/usr/bin/env python3

import sys

# Reactions are stored in a map indexed by the output chemical. The values
# are pairs consisting of the produced amount and the list of input
# chemicals. Each input chemical is a pair consisting of the chemical and
# the required amount.

reactions = dict()
for line in sys.stdin:
    reaction = line.rstrip().split(" => ")
    reaction = [ s.split(" ") for r in reaction for s in r.split(", ") ]
    reaction = [ (r[1], int(r[0])) for r in reaction ]
    reactions[reaction[-1][0]] = (reaction[-1][1], reaction[:-1])

print(reactions, file=sys.stderr)

def oreRequired(fuel):
    required = { "FUEL": fuel }
    while True:
        for chemical, amount in required.items():
            if "ORE" != chemical and 0 < amount:
                break

        if "ORE" == chemical or amount <= 0:
            break

        production, reaction = reactions[chemical]
        factor = (amount + production - 1) // production
        print(
            "chemical={} amount={} production={} reaction={} factor={}".
            format(chemical, amount, production, reaction, factor),
            file=sys.stderr
        )

        required[chemical] -= factor * production
        for raw, amt in reaction:
            required[raw] = factor * amt + required.get(raw, 0)

        print(required, file=sys.stderr)

    return required["ORE"]

print("fuel={} ore={}".format(1, oreRequired(1)))

upperBound = 1
TRILLION = 10**12
while oreRequired(upperBound) < TRILLION:
    upperBound *= 2
print("upperBound={} ore={}".format(upperBound, oreRequired(upperBound)))

# oreRequired(lowerBound) <= TRILLION < oreRequired(upperBound)
lowerBound = upperBound // 2
while 1 + lowerBound != upperBound:
    mid = lowerBound + (upperBound - lowerBound) // 2
    ore = oreRequired(mid)
    if ore <= TRILLION:
        lowerBound = mid
    else:
        upperBound = mid

print("lowerBound={} ore={}".format(lowerBound, oreRequired(lowerBound)))
