#include "bugslife.h"

#include <sysexits.h>

#include <iostream>
#include <string>
#include <string_view>

namespace
{
int Main(std::vector<std::string_view> args)
{
    if (2 != args.size()) {
        std::cerr << "usage: " << args[0] << R"( minutes

    Run the simulation for the given number of minutes.
)";
        return EX_USAGE;
    }
    BugsLife bugs{};
    std::cin >> bugs;
    std::cerr << bugs << '\n';

    for (int i{std::stoi(std::string{args[1]})}; 0 != i; --i) {
        bugs = bugs.cycle(true);
        std::cerr << bugs << '\n';
    }
    std::cout << "count=" << bugs.count() << '\n';

    return 0;
}
} // namespace

int main(int argc, char** argv)
{
    return Main(std::vector<std::string_view>{argv, argc + argv});
}
