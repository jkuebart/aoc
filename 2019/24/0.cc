#include "bugslife.h"

#include <iostream>
#include <set>

int main()
{
    BugsLife bugs{};
    std::cin >> bugs;
    std::cerr << bugs << '\n';

    std::set<BugsLife> seen{};
    while (0 == seen.count(bugs)) {
        seen.insert(bugs);
        bugs = bugs.cycle(false);
        std::cerr << bugs << '\n';
    }
    std::cout << "biodiversity: " << bugs.biodiversity() << '\n'
              << bugs << '\n';

    return 0;
}
