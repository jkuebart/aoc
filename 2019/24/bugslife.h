#ifndef BUGSLIFE_H
#define BUGSLIFE_H

#include "geo/bounds.h"
#include "geo/direction.h"
#include "geo/point.h"

#include <iosfwd>
#include <vector>

class BugsLife
{
public:
    using Point = ::geo::Point<geo::Direction<2>>;

    unsigned biodiversity() const;
    unsigned count() const;
    bool get(unsigned level, Point const& point) const;

    BugsLife& set(unsigned level, Point const& point);
    BugsLife& clear(unsigned level, Point const& point);

    BugsLife cycle(bool levels) const;

    friend bool operator<(BugsLife const& lhs, BugsLife const& rhs);
    friend std::istream& operator>>(std::istream& is, BugsLife& bugs);
    friend std::ostream& operator<<(std::ostream& os, BugsLife const& bugs);

private:
    unsigned mask(Point const& point) const;
    BugsLife& compress();

private:
    std::vector<unsigned> mBugs{};
};

#endif // BUGSLIFE_H
