#include "bugslife.h"

#include <bitset>
#include <iostream>
#include <string>

using Point = BugsLife::Point;
using Bounds = geo::Bounds<Point>;

static constexpr int kRows{5};
static constexpr int kColumns{5};
static constexpr Bounds kBounds{{-2, -2}, {2, 2}};

static constexpr Point kBottomLeft{1, -1};
static constexpr Point kBottomRight{1, 1};
static constexpr Point kCentre{0, 0};
static constexpr Point kInnerBottom{1, 0};
static constexpr Point kInnerLeft{0, -1};
static constexpr Point kInnerRight{0, 1};
static constexpr Point kInnerTop{-1, 0};
static constexpr Point kTopLeft{-1, -1};
static constexpr Point kTopRight{-1, 1};

unsigned BugsLife::biodiversity() const
{
    return mBugs[0];
}

unsigned BugsLife::count() const
{
    unsigned count{0};
    for (unsigned bugs : mBugs) {
        count += std::bitset<kRows * kColumns>{bugs}.count();
    }
    return count;
}

bool BugsLife::get(unsigned const level, Point const& point) const
{
    if (level < mBugs.size()) {
        return mBugs[level] & mask(point);
    }
    return false;
}

BugsLife& BugsLife::set(unsigned const level, Point const& point)
{
    if (level < mBugs.size()) {
        mBugs[level] |= mask(point);
    }
    return *this;
}

BugsLife& BugsLife::clear(unsigned const level, Point const& point)
{
    if (level < mBugs.size()) {
        mBugs[level] &= ~mask(point);
    }
    return *this;
}

BugsLife BugsLife::cycle(bool const levels) const
{
    auto const countAdjacent{[this](unsigned const level, Point const& point)
                             {
                                 unsigned count{0};
                                 point.adjacent(
                                     [this, &count, &level](Point const& pnt)
                                     {
                                         if (get(level, pnt)) {
                                             ++count;
                                         }
                                     });
                                 return count;
                             }};

    BugsLife result{};
    result.mBugs.push_back(0);
    result.mBugs.insert(result.mBugs.end(), mBugs.begin(), mBugs.end());
    result.mBugs.push_back(0);
    auto const setOrClear{[this, &result](
                              unsigned const level,
                              Point const& point,
                              unsigned const count)
                          {
                              if (get(level - 1, point)) {
                                  if (1 != count) {
                                      result.clear(level, point);
                                  }
                              } else {
                                  if (1 <= count && count <= 2) {
                                      result.set(level, point);
                                  }
                              }
                          }};

    for (unsigned level{0}; level != result.mBugs.size(); ++level) {
        {
            unsigned const bottom{get(level - 2, kInnerBottom)};
            unsigned const left{get(level - 2, kInnerLeft)};
            unsigned const right{get(level - 2, kInnerRight)};
            unsigned const top{get(level - 2, kInnerTop)};

            for (int i{-2}; i != 2; ++i) {
                Point point{-2, i};
                unsigned count{countAdjacent(level - 1, point)};
                count += -2 == i ? left : 2 == i ? right : 0;
                setOrClear(level, point, top + count);

                point = {2, -i};
                count = countAdjacent(level - 1, point);
                count += -2 == i ? right : 2 == i ? left : 0;
                setOrClear(level, point, bottom + count);

                point = {-i, -2};
                count = countAdjacent(level - 1, point);
                count += -2 == i ? bottom : 2 == i ? top : 0;
                setOrClear(level, point, left + count);

                point = {i, 2};
                count = countAdjacent(level - 1, point);
                count += -2 == i ? top : 2 == i ? bottom : 0;
                setOrClear(level, point, right + count);
            }
        }

        for (Point const& point :
             {kTopLeft, kTopRight, kBottomLeft, kBottomRight}) {
            unsigned const count{countAdjacent(level - 1, point)};
            setOrClear(level, point, count);
        }

        if (!levels) {
            unsigned const count{countAdjacent(level - 1, kCentre)};
            setOrClear(level, kCentre, count);
        }

        {
            auto const countRow{[this, &level](int const row)
                                {
                                    unsigned cnt{0};
                                    for (int column{-2}; column != 3; ++column)
                                    {
                                        if (get(level, {row, column})) {
                                            ++cnt;
                                        }
                                    }
                                    return cnt;
                                }};

            unsigned const top{countRow(-2)};
            unsigned count{countAdjacent(level - 1, kInnerTop)};
            setOrClear(level, kInnerTop, top + count);

            unsigned const bottom{countRow(2)};
            count = countAdjacent(level - 1, kInnerBottom);
            setOrClear(level, kInnerBottom, bottom + count);

            auto const countColumn{[this, &level](int const column)
                                   {
                                       unsigned cnt{0};
                                       for (int row{-2}; row != 3; ++row) {
                                           if (get(level, {row, column})) {
                                               ++cnt;
                                           }
                                       }
                                       return cnt;
                                   }};
            unsigned const left{countColumn(-2)};
            count = countAdjacent(level - 1, kInnerLeft);
            setOrClear(level, kInnerLeft, left + count);

            unsigned const right{countColumn(2)};
            count = countAdjacent(level - 1, kInnerRight);
            setOrClear(level, kInnerRight, right + count);
        }
    }

    if (!levels) {
        result.mBugs.erase(result.mBugs.begin());
        result.mBugs.pop_back();
    } else {
        result.compress();
    }

    return result;
}

unsigned BugsLife::mask(Point const& point) const
{
    if (kBounds.contains(point)) {
        return unsigned{1} << ((2 + point[0]) * kColumns + 2 + point[1]);
    }
    return 0;
}

BugsLife& BugsLife::compress()
{
    mBugs.erase(
        mBugs.begin(),
        std::find_if(
            mBugs.begin(),
            mBugs.end(),
            [](unsigned i)
            {
                return 0 != i;
            }));
    while (0 == mBugs.back()) {
        mBugs.pop_back();
    }
    return *this;
}

bool operator<(BugsLife const& lhs, BugsLife const& rhs)
{
    return lhs.mBugs < rhs.mBugs;
}

std::istream& operator>>(std::istream& is, BugsLife& bugs)
{
    bugs.mBugs = {0};
    unsigned bug{1};
    for (std::string line{}; std::getline(std::cin, line);) {
        for (char c : line) {
            if ('.' != c) {
                bugs.mBugs.front() += bug;
            }
            bug *= 2;
        }
    }
    return is;
}

std::ostream& operator<<(std::ostream& os, BugsLife const& bugs)
{
    unsigned level{0};
    for (auto bgs : bugs.mBugs) {
        os << "level " << level << '\n';
        unsigned bug{1};
        for (int row{0}; row != kRows; ++row) {
            for (int column{0}; column != kColumns; ++column) {
                os << ((bgs & bug) ? '#' : '.');
                bug *= 2;
            }
            os << '\n';
        }
        os << '\n';
        ++level;
    }

    return os;
}
