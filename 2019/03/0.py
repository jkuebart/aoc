#!/usr/bin/env python3

import sys

DIRECTION = {
    'D': (0, -1),
    'L': (-1, 0),
    'R': (1, 0),
    'U': (0, 1),
}

def directions(line):
    """Turn a line of commands into a sequence of directions."""
    for cmd in line.split(','):
        l = int(cmd[1:])
        yield (l * x for x in DIRECTION[cmd[0]])

def polyline(line):
    """Turn a line of commands into a polyline."""
    p = (0, 0)
    yield p
    for d in directions(line):
        p = tuple(map(sum, zip(p, d)))
        yield p

def distance(p0, p1 = (0, 0)):
    """Return the Manhattan distance between two points."""
    return sum(abs(x - y) for x, y in zip(p0, p1))

def intersection(l0, l1):
    """
    Where the horizontal line segment l0 intersects the vertical line
    segment l1. This relies heavily on the fact that line segments are
      - parallel to the axes *and*
      - not collinear.
    """
    i = (l1[0][0], l0[0][1])
    r0 = range(min(l0[0][0], l0[1][0]), 1 + max(l0[0][0], l0[1][0]))
    r1 = range(min(l1[0][1], l1[1][1]), 1 + max(l1[0][1], l1[1][1]))

    # Ignore intersections at the start point of segments because the
    # common starting point of wires should be ignored and all other
    # start points are also end points of the previous segment.
    if i[0] != l0[0][0] and i[1] != l1[0][1] and i[0] in r0 and i[1] in r1:
        return i

    return None

def intersections(poly0, poly1):
    """
    Turn two polylines into a sequence of tuples consisting of
      - intersection point
      - distance along the first polyline.
      - distance along the second polyline.
    """
    d0 = 0
    for l0 in zip(poly0, poly0[1:]):
        d1 = 0
        for l1 in zip(poly1, poly1[1:]):
            i = intersection(l0, l1) or intersection(l1, l0)
            if i:
                yield (i, d0 + distance(l0[0], i), d1 + distance(l1[0], i))
            d1 += distance(*l1)
        d0 += distance(*l0)

# Iterate over wire pairs defined on consecutive lines.
lines = [line.rstrip() for line in sys.stdin]
for wires in zip(lines[0::2], lines[1::2]):
    # Compute polylines representing the wires.
    poly0, poly1 = (list(polyline(wire)) for wire in wires)

    # Compute all intersection points between the two polylines.
    ints = list(intersections(poly0, poly1))

    # Print smallest Manhattan distance of intersection points.
    print("minimal length", min(distance(p[0]) for p in ints))

    # Print smallest combined distance along the wires.
    print("minimal distance", min(p[1] + p[2] for p in ints))
