#include "intcode/processor.h"
#include "streamer/stream.h"

#include <algorithm>
#include <functional>
#include <iostream>
#include <vector>

using intcode::Processor;
using streamer::stream;

constexpr unsigned kNatAddress{255};

int main()
{
    Processor processor{};
    while (std::cin >> processor) {

        // Assign addresses to the network.

        std::vector<Processor> network(50, processor);
        for (unsigned i{0}; i != network.size(); ++i) {
            network[i].input({i});
        }

        // Run the network.

        std::vector<std::vector<std::intmax_t>> packets(network.size());
        std::vector<std::intmax_t> prevNat{};
        std::vector<std::intmax_t> nat{};
        for (bool done{false}; !done;) {
            for (unsigned i{0}; i != network.size(); ++i) {

                // Run.

                network[i].run();

                // Pick up and dispatch output.

                auto const output{network[i].output()};
                if (output.size() % 3) {
                    std::cerr << "Node should speak in triples.\n";
                    done = true;
                    break;
                }
                for (unsigned j{0}; j != output.size(); j += 3) {
                    unsigned const address{static_cast<unsigned>(output[j])};
                    auto const begin{output.begin() + j + 1};
                    auto const end{output.begin() + j + 3};

                    if (kNatAddress == address) {
                        nat.clear();
                        nat.insert(nat.end(), begin, end);
                    }
                    if (address < packets.size()) {
                        packets[address].insert(
                            packets[address].end(),
                            begin,
                            end);
                    }
                }

                // Deliver input.

                if (!packets[i].empty()) {
                    network[i].input(packets[i]);
                    packets[i].clear();
                } else {
                    network[i].input({-1});
                }
            }

            // Send NAT package when network is idle.

            if (std::all_of(
                    packets.begin(),
                    packets.end(),
                    std::mem_fn(&std::vector<std::intmax_t>::empty)) &&
                !nat.empty())
            {
                std::cout << "@nat sent " << stream(nat) << '\n';
                packets[0].insert(packets[0].end(), nat.begin(), nat.end());
                if (prevNat == nat) {
                    done = true;
                }
                prevNat = std::move(nat);
                nat.clear();
            }
        }
    }
    return 0;
}
