#include "geo/direction.h"
#include "geo/point.h"
#include "intcode/processor.h"
#include "streamer/stream.h"

#include <iostream>
#include <queue>
#include <set>
#include <vector>

using intcode::Processor;
using streamer::stream;

using Direction = geo::Direction<2>;
using Point = geo::Point<Direction>;

namespace
{
constexpr int kWall{0};
constexpr int kFree{1};
constexpr int kOxygen{2};

constexpr Direction kDirections[] = {{0, 1}, {0, -1}, {-1, 0}, {1, 0}};

/**
 * Explore using the program running in the processor. When oxygen is
 * found, return its location and the distance from the starting point.
 * When no oxygen is found, return a point at the maximum distance.
 */
std::tuple<Point, Processor, unsigned> explore(Processor const& processor)
{
    std::queue<std::tuple<Point, Processor, unsigned>> seen{
        {{{0, 0}, processor, 0}}};
    std::set<Point> visited{{0, 0}};

    Point droid{0, 0};
    unsigned maxCost{0};
    while (!seen.empty()) {

        // Find next starting position to evaluate.

        auto const& [position, currentProcessor, distance]{seen.front()};
        std::cerr << "\nposition=" << position << " (" << distance << ")\n";
        droid = position;
        maxCost = distance;

        // Evaluate starting position in all unexplored directions.

        unsigned const cost{1 + distance};
        for (unsigned dir{0}; dir != std::size(kDirections); ++dir) {
            Point const next{position + kDirections[dir]};
            if (0 == visited.count(next)) {
                visited.insert(next);
                Processor explorer{currentProcessor};
                explorer.input({1 + dir});
                explorer.run();
                auto const output{explorer.output()};
                switch (output.back()) {
                case kWall:
                    std::cerr << "  position=" << next << " WALL\n";
                    break;
                case kFree:
                    std::cerr << "  position=" << next << " cost=" << cost
                              << '\n';
                    seen.push({next, explorer, cost});
                    break;
                case kOxygen:
                    return {next, explorer, cost};
                default:
                    std::cerr << "Invalid output " << output[0] << '\n';
                    return {};
                }
            }
        }
        seen.pop();
    }
    return {droid, processor, maxCost};
}
} // namespace

int main()
{
    Processor processor{};
    while (std::cin >> processor) {
        // Explore the map until oxygen is found.
        auto const& [oxygen, oxygenProcessor, distance]{explore(processor)};
        std::cout << "oxygen=" << oxygen << " cost=" << distance << '\n';

        // Explore the map again from the current position (oxygen) until
        // the entire map is discovered. Output the length of the longest
        // path.
        auto const& [point, extentProcessor, extent]{explore(oxygenProcessor)};
        std::cout << "extent=" << point << " cost=" << extent << '\n';
    }
    return 0;
}
