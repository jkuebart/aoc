#include "geo/direction.h"
#include "geo/point.h"
#include "intcode/processor.h"
#include "streamer/stream.h"

#include <sysexits.h>

#include <iostream>
#include <set>
#include <string>
#include <string_view>
#include <vector>

using intcode::Processor;
using streamer::stream;

using Direction = geo::Direction<2>;
using Point = geo::Point<Direction>;

namespace
{
class Canvas
{
public:
    explicit Canvas(char const colour) : mCanvas{std::string(1, colour)} {}

    char& operator[](Point const& position)
    {
        while (position[1] < mY) {
            mCanvas.insert(
                mCanvas.begin(),
                std::string(mCanvas[0].size(), '.'));
            mY -= 1;
        }
        std::size_t const uy{static_cast<std::size_t>(position[1] - mY)};
        while (mCanvas.size() <= uy) {
            mCanvas.push_back(std::string(mCanvas.back().size(), '.'));
        }
        if (position[0] < mX) {
            for (auto& line : mCanvas) {
                line.insert(0, static_cast<std::size_t>(mX - position[0]), '.');
            }
            mX = position[0];
        }
        std::size_t const ux{static_cast<std::size_t>(position[0] - mX)};
        if (mCanvas[0].size() <= ux) {
            for (auto& line : mCanvas) {
                line.append(1 + ux - line.size(), '.');
            }
        }
        return mCanvas[uy][ux];
    }

    friend std::ostream& operator<<(std::ostream& os, Canvas const& rhs)
    {
        return os << stream(rhs.mCanvas, "\n");
    }

private:
    int mX{0}, mY{0};
    std::vector<std::string> mCanvas{};
};

int Main(std::vector<std::string_view> args)
{
    if (2 != args.size()) {
        std::cerr << "usage: " << args[0] << R"( colour

    Read a program from standard input and start the robot on a square of
    the given colour which can be '#' for white or anything else for black.
)";
        return EX_USAGE;
    }

    Processor processor;
    while (std::cin >> processor) {
        Direction direction{0, -1};
        Point position{0, 0};
        Canvas canvas{args[1][0]};
        std::set<Point> painted{};

        processor.run();
        while (processor.reading()) {
            auto const output{processor.output()};
            std::cerr << "output: " << stream(output) << '\n';
            if (output.size() % 2) {
                std::cerr << "Robot should speak in pairs.\n";
                return EX_DATAERR;
            }
            for (std::size_t i{0}; i != output.size(); i += 2) {
                std::cerr << "  paint " << position << ": " << output[i]
                          << '\n';
                painted.insert(position);
                canvas[position] = output[i] ? '#' : '.';
                std::cerr << "painted=" << painted.size() << '\n'
                          << canvas << '\n';
                if (output[1 + i]) {
                    direction.rotateLeft();
                } else {
                    direction.rotateRight();
                }
                position = position + direction;
                std::cerr << "  walked " << direction << " to " << position
                          << '\n';
            }

            processor.input({'#' == canvas[position] ? 1 : 0});
            processor.run();
        }
        std::cout << "painted=" << painted.size() << '\n' << canvas << '\n';
    }
    return 0;
}
} // namespace

int main(int argc, char** argv)
{
    return Main(std::vector<std::string_view>{argv, argc + argv});
}
