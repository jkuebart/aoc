#include "maths/gcd.h"
#include "maths/inverse.h"
#include "streamer/stream.h"

#include <sysexits.h>

#include <iostream>
#include <string>
#include <string_view>
#include <vector>

using maths::gcd;
using maths::inverse;
using streamer::stream;

namespace
{
bool startsWith(std::string_view const& str, std::string_view const& start)
{
    return str.substr(0, start.size()) == start;
}

/**
 * Modulo multiplication using the logarithmic algorithm.
 */
std::intmax_t multiply(std::intmax_t x, std::intmax_t y, std::intmax_t const n)
{
    std::intmax_t result{0};
    while (0 != y) {
        if (y % 2) {
            result += x;
            result %= n;
        }
        x *= 2;
        x %= n;
        y /= 2;
    }
    return result;
}

/**
 * Modulo exponentiation using the logarithmic algorithm.
 */
std::intmax_t
power(std::intmax_t base, std::intmax_t exp, std::intmax_t const n)
{
    std::intmax_t result{1};
    while (0 != exp) {
        if (exp % 2) {
            result = multiply(result, base, n);
        }
        base = multiply(base, base, n);
        exp /= 2;
    }
    return result;
}

/**
 * I represent a linear equation m * x + c in a finite ring Z/Zn.
 *
 * Initially m = 1 and c = 0. Operations provided are reverse(), cut(i),
 * deal(i) and repeat(i).
 *
 * The sequence is a permutation if and only if the numbers given to
 * deal(i) are coprime to the number given to the contsructor.
 */
class Deck
{
public:
    explicit Deck(std::intmax_t const cards) : mCards{cards} {}

    /**
     * @return The position of the given number in the sequence.
     */
    std::intmax_t indexOf(std::intmax_t i) const
    {
        // i = m * x + c  <==>  x = (i - c) / m
        return multiply(mCards + i - mCut, inverse(mIncrement, mCards), mCards);
    }

    /**
     * @return The number at the given position in the sequence.
     */
    std::intmax_t operator[](std::intmax_t i) const
    {
        return (multiply(i, mIncrement, mCards) + mCut) % mCards;
    }

    /**
     * Reverse the sequence by c <- c - m and m <- -m.
     */
    Deck& reverse()
    {
        cut(-1);
        mIncrement = mCards - mIncrement;
        return *this;
    }

    /**
     * Shift the sequence by c <- c + m * i.
     */
    Deck& cut(std::intmax_t i)
    {
        while (i < 0) {
            i += mCards;
        }
        mCut += multiply(mIncrement, i, mCards);
        mCut %= mCards;
        return *this;
    }

    /**
     * Spread the sequence by i, setting m <- m / i.
     */
    Deck& deal(std::intmax_t const i)
    {
        mIncrement = multiply(mIncrement, inverse(i, mCards), mCards);
        return *this;
    }

    /**
     * Insert the linear equation m * x + c into itself the given number of
     * times, effectively repeating the operations performed so far for the
     * given number of times.
     */
    Deck& repeat(std::intmax_t i)
    {
        // Inserting m * x + c into itself i times yields
        // c' = (1 + m + ... + m^(i-1)) * c
        if (1 == mIncrement) {
            mCut = multiply(mCut, i, mCards);
            return *this;
        }

        // ...which is (m^i - 1) / (m - 1) * c for m != 1.
        // factor := m^i - 1
        std::intmax_t const factor{power(mIncrement, i, mCards) - 1};
        // To divide by m - 1, compute the inverse of the coprime part:
        // factor / (m - 1) = factor / gcd * inverse((m - 1) / gcd)
        std::intmax_t const d{gcd(mIncrement - 1, mCards)};
        // inv := inverse((m - 1) / d)
        std::intmax_t const inv{inverse((mIncrement - 1) / d, mCards)};
        mCut = multiply(mCut, multiply(factor / d, inv, mCards), mCards);
        mIncrement = power(mIncrement, i, mCards);
        return *this;
    }

    friend std::ostream& operator<<(std::ostream& os, Deck const& deck)
    {
        if (deck.mCards < 20) {
            for (std::intmax_t i{0}; i != deck.mCards; ++i) {
                os << deck[i] << ", ";
            }
        }
        return os << "mIncrement=" << deck.mIncrement << " mCut=" << deck.mCut;
    }

private:
    std::intmax_t mCards{};
    std::intmax_t mIncrement{1};
    std::intmax_t mCut{0};
};

int Main(std::vector<std::string_view> args)
{
    if (3 != args.size()) {
        std::cerr << "usage: " << args[0] << R"( cards repeats

    Shuffle a deck of cards of the given size using the methods provided on
    standard input. Repeat the methods the given number of times and print

      - the index of the card 2019, and
      - the value of the card at index 2020.
)";
        return EX_USAGE;
    }
    Deck deck{static_cast<std::intmax_t>(std::stoul(std::string{args[1]}))};
    for (std::string line{}; std::getline(std::cin, line);) {
        if (line == "deal into new stack") {
            deck.reverse();
        } else if (startsWith(line, "cut ")) {
            deck.cut(std::stoi(line.substr(4)));
        } else if (startsWith(line, "deal with increment ")) {
            deck.deal(std::stoi(line.substr(20)));
        } else {
            std::cerr << "Ignoring \"" << line << "\"\n";
        }
        std::cerr << line << '\n' << deck << '\n';
    }
    std::intmax_t const repeats{
        static_cast<std::intmax_t>(std::stoul(std::string{args[2]}))};
    std::cerr << "repeat " << repeats << '\n';
    deck.repeat(repeats);
    std::cout << deck << '\n'
              << "deck[" << deck.indexOf(2019)
              << "] = " << deck[deck.indexOf(2019)] << '\n'
              << "deck[2020] = " << deck[2020] << '\n';
    return 0;
}
} // namespace

int main(int argc, char** argv)
{
    return Main(std::vector<std::string_view>{argv, argc + argv});
}
