#include <algorithm>
#include <iostream>
#include <string>

int main()
{
    for (std::string line{}; std::getline(std::cin, line);) {

        // Break line at the dash.

        auto const dash{std::find(line.begin(), line.end(), '-')};
        if (line.begin() == dash || line.end() == dash) {
            std::cerr << "invalid line: " << line << '\n';
            continue;
        }
        std::string const end{1 + dash, line.end()};
        line.erase(dash, line.end());

        // Move up to the nearest non-decreasing combination.

        for (auto it{1 + line.begin()}; line.end() != it; ++it) {
            if (it[0] < it[-1]) {
                std::fill(it, line.end(), it[-1]);
                break;
            }
        }

        int ncombinations0{0};
        int ncombinations1{0};
        while (line < end) {

            // Count valid combination.

            for (auto it{1 + line.begin()}; line.end() != it; ++it) {
                if (it[-1] == it[0]) {
                    ++ncombinations0;
                    break;
                }
            }
            for (auto it{1 + line.begin()}; line.end() != it; ++it) {
                if (it[-1] == it[0]) {
                    if (line.end() == 1 + it || it[0] != it[1]) {
                        ++ncombinations1;
                        break;
                    }
                    while (line.end() != 1 + it && it[0] == it[1]) {
                        ++it;
                    }
                }
            }

            // Increase the last digit that isn't a nine.

            auto pos{std::find_if(
                         line.rbegin(),
                         line.rend(),
                         [](char const c)
                         {
                             return '9' != c;
                         })
                         .base()};
            if (line.begin() != pos) {
                --pos;
                std::fill(pos, line.end(), 1 + *pos);
            }
        }

        std::cout << "doubles=" << ncombinations0
                  << " strict=" << ncombinations1 << '\n';
    }
    return 0;
}
