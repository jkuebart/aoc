#include "intcode/processor.h"

#include <sysexits.h>

#include <iostream>
#include <iterator>

using intcode::Processor;

int main()
{
    Processor processor{};
    while (std::cin >> processor) {
        char const path[]{
            "south\n"
            "west\n"
            "north\n"
            "take fuel cell\n"
            "south\n"
            "east\n"
            "north\n"
            "north\n"
            "east\n"
            "take candy cane\n"
            "south\n"
            "take hypercube\n"
            "north\n"
            "west\n"
            "north\n"
            "take coin\n"
            "east\n"
            "take tambourine\n"
            "west\n"
            "west\n"
            "take spool of cat6\n"
            "north\n"
            "take weather machine\n"
            "west\n"
            "take mutex\n"
            "west\n"};
        processor.input({std::begin(path), std::end(path) - 1});
        processor.run();
        processor.output();

        char const* const inventory[]{
            "candy cane",
            "coin",
            "fuel cell",
            "hypercube",
            "mutex",
            "spool of cat6",
            "tambourine",
            "weather machine",
        };
        for (int drop{0}; drop != (1 << std::size(inventory)); ++drop) {
            Processor explore{processor};
            std::string input{};
            for (unsigned item{0}; item != std::size(inventory); ++item) {
                if (drop & (1 << item)) {
                    input.append("drop ");
                    input.append(inventory[item]);
                    input.append("\n");
                }
            }
            std::cerr << "attempt " << drop << '\n' << input << '\n';

            input.append("west\n");
            explore.input({input.begin(), input.end()});
            explore.run();
            auto const output{explore.output()};
            std::string const s{output.begin(), output.end()};
            if (std::string::npos == s.find("Alert")) {
                std::cout << s;
                break;
            }
        }
    }
    return 0;
}
