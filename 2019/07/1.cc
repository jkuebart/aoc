#include "intcode/processor.h"
#include "streamer/stream.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <vector>

using intcode::Processor;
using streamer::stream;

int main()
{
    Processor processor{};
    while (std::cin >> processor) {
        std::intmax_t maxValue{0};
        std::vector<int> phases{5, 6, 7, 8, 9};
        do {
            std::cerr << "phases: " << stream(phases) << '\n';
            std::vector<Processor> procs(phases.size(), processor);
            for (unsigned int i{0}; i != procs.size(); ++i) {
                procs[i].input({phases[i]});
                procs[i].run();
            }

            std::intmax_t value{0};
            while (procs[0].reading()) {
                for (unsigned int i{0}; i != phases.size(); ++i) {
                    assert(procs[i].reading());
                    procs[i].input({value});
                    procs[i].run();
                    std::vector<std::intmax_t> const output{procs[i].output()};
                    assert(1 == output.size());
                    value = output.front();
                    std::cerr << "i=" << i << " value=" << value << '\n';
                }
                if (maxValue < value) {
                    maxValue = value;
                }
            }
        } while (std::next_permutation(phases.begin(), phases.end()));
        std::cout << maxValue << '\n';
    }
    return 0;
}
