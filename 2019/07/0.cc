#include "intcode/processor.h"
#include "streamer/stream.h"

#include <algorithm>
#include <iostream>
#include <vector>

using intcode::Processor;
using streamer::stream;

int main()
{
    Processor processor{};
    while (std::cin >> processor) {
        std::intmax_t maxOutput{0};
        std::vector<int> phases{0, 1, 2, 3, 4};
        do {
            std::cerr << "phases: " << stream(phases) << '\n';
            std::intmax_t output{0};
            for (unsigned int i{0}; i != phases.size(); ++i) {
                Processor proc{processor};
                proc.input({phases[i], output});
                proc.run();
                output = proc.output().front();
                std::cerr << "i=" << i << " output=" << output << '\n';
            }
            if (maxOutput < output) {
                maxOutput = output;
            }
        } while (std::next_permutation(phases.begin(), phases.end()));
        std::cout << maxOutput << '\n';
    }
    return 0;
}
