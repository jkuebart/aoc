#include "geo/direction.h"
#include "geo/point.h"
#include "streamer/getlines.h"
#include "streamer/stream.h"

#include <cctype>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

using streamer::getlines;
using streamer::stream;

using Point = geo::Point<geo::Direction<2>>;

namespace
{

/**
 * Current positions of all robots and the collected keys.
 */
struct Situation
{
    std::vector<Point> positions{};
    std::set<char> have{};

    bool haveKey(char const key) const
    {
        return 0 != have.count(static_cast<char>(std::toupper(key)));
    }

    Situation with(Point const& point, unsigned const i, char const key) const
    {
        Situation situation{*this};
        situation.positions[i] = point;
        situation.have.insert(static_cast<char>(std::toupper(key)));
        return situation;
    }

    friend bool operator<(Situation const& lhs, Situation const& rhs)
    {
        return std::tie(lhs.positions, lhs.have) <
            std::tie(rhs.positions, rhs.have);
    }

    friend std::ostream& operator<<(std::ostream& os, Situation const& rhs)
    {
        return os << '{' << stream(rhs.positions) << ", \""
                  << stream(rhs.have, "") << "\"}";
    }
};

class Tunnels
{
public:
    /**
     * @return Positions of all robots.
     */
    std::vector<Point> locations() const
    {
        std::vector<Point> result{};
        for (unsigned y{0}; y != mTunnels.size(); ++y) {
            for (unsigned x{0}; x != mTunnels[y].size(); ++x) {
                if ('@' == mTunnels[y][x]) {
                    result.push_back(
                        {static_cast<int>(x), static_cast<int>(y)});
                }
            }
        }
        return result;
    }

    /**
     * @param origin Starting location.
     * @param have The available keys.
     * @return All reachable positions, the collected key and the distance.
     */
    std::vector<std::tuple<Point, char, unsigned>> keys(
        Point const& origin,
        std::set<char> const& have) const
    {
        std::vector<std::tuple<Point, char, unsigned>> result{};
        std::queue<std::pair<Point, unsigned>> seen{{{origin, 0}}};
        std::set<Point> visited{origin};
        while (!seen.empty()) {
            auto const& [position, distance]{seen.front()};

            unsigned const cost{1 + distance};
            position.adjacent(
                [this, cost, &have, &result, &seen, &visited](
                    Point const& point)
                {
                    if (0 == visited.count(point)) {
                        visited.insert(point);
                        char const square{get(point)};
                        if ('#' != square &&
                            (!std::isupper(square) || 0 != have.count(square)))
                        {
                            if (std::islower(square)) {
                                result.push_back({point, square, cost});
                            }
                            seen.push({point, cost});
                        }
                    }
                });

            seen.pop();
        }
        return result;
    }

    /**
     * @param situation The current location of all robots and the
     *                  available keys.
     * @return All reachable situations and the respective distances.
     */
    std::vector<std::pair<Situation, unsigned>> keys(
        Situation const& situation) const
    {
        std::vector<std::pair<Situation, unsigned>> result{};
        for (unsigned i{0}; i != situation.positions.size(); ++i) {
            auto const nextKeys{keys(situation.positions[i], situation.have)};
            for (auto const& [position, key, distance] : nextKeys) {
                if (!situation.haveKey(key)) {
                    result.push_back(
                        {situation.with(position, i, key), distance});
                }
            }
        }
        return result;
    }

    char get(Point const& point) const
    {
        unsigned x{static_cast<unsigned>(point[0])};
        unsigned y{static_cast<unsigned>(point[1])};
        if (y < mTunnels.size() && x < mTunnels[y].size()) {
            return mTunnels[y][x];
        }
        return '#';
    }

    friend std::istream& operator>>(std::istream& is, Tunnels& tunnels)
    {
        tunnels.mTunnels = getlines(is);
        return is;
    }

    friend std::ostream& operator<<(std::ostream& os, Tunnels const& tunnels)
    {
        return os << stream(tunnels.mTunnels, "\n");
    }

private:
    std::vector<std::string> mTunnels;
};
} // namespace

int main()
{
    Tunnels tunnels{};
    std::cin >> tunnels;
    std::cerr << tunnels << '\n';

    struct Node
    {
        Situation situation{};
        unsigned steps{0};

        bool operator<(Node const& rhs) const
        {
            return rhs.steps < steps;
        }
    };
    std::priority_queue<Node> seen{{}, {{{tunnels.locations(), {}}, 0}}};
    std::set<Situation> visited{};
    while (!seen.empty()) {
        auto const [situation, steps]{seen.top()};
        seen.pop();
        if (0 == visited.count(situation)) {
            visited.insert(situation);
            std::cerr << "situation=<" << situation << ", " << steps << ">\n";

            auto const neighbours{tunnels.keys(situation)};
            if (neighbours.empty()) {
                std::cout << "steps=" << steps << '\n';
                break;
            }

            for (auto const& n : neighbours) {
                // std::cerr << "  " << stream(n, ", ", "<", ">") << '\n';
                seen.push({n.first, steps + n.second});
            }
        }
    }

    return 0;
}
