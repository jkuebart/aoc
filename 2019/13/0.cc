#include "geo/direction.h"
#include "geo/point.h"
#include "intcode/processor.h"

#include <sysexits.h>

#include <algorithm>
#include <iostream>
#include <set>
#include <string>
#include <string_view>
#include <vector>

using intcode::Processor;

using Point = geo::Point<geo::Direction<2>>;

namespace
{
constexpr int kEmpty{0};
constexpr int kBlock{2};
constexpr int kPaddle{3};
constexpr int kBall{4};
constexpr Point kScore{-1, 0};

int Main(std::vector<std::string_view> args)
{
    if (2 != args.size()) {
        std::cerr << "usage: " << args[0] << R"( coins

    Read the arcade game code from standard input and play with the given
    number of coins. 1 for testing and 2 for gameplay.
)";
        return EX_USAGE;
    }

    Processor processor{};
    while (std::cin >> processor) {
        processor.set(0, std::stoi(std::string{args[1]}));

        std::set<Point> blocks{};
        int score{0};
        Point ball{};
        Point paddle{};
        for (;;) {
            processor.run();
            auto const output{processor.output()};
            if (output.size() % 3) {
                std::cerr << "Arcade should speak triples.\n";
                return EX_DATAERR;
            }
            for (unsigned i{0}; i != output.size(); i += 3) {
                Point const pos{
                    static_cast<int>(output[i]),
                    static_cast<int>(output[1 + i])};

                if (kScore == pos) {
                    score = static_cast<int>(output[2 + i]);
                } else {
                    switch (output[2 + i]) {
                    case kEmpty:
                        blocks.erase(pos);
                        break;
                    case kBlock:
                        blocks.insert(pos);
                        break;
                    case kPaddle:
                        paddle = pos;
                        break;
                    case kBall:
                        ball = pos;
                        break;
                    default:
                        break;
                    }
                }
            }
            if (!processor.reading()) {
                break;
            }
            std::cerr << "blocks=" << blocks.size() << " ball=" << ball
                      << " paddle=" << paddle << " score=" << score << '\n';
            if (ball[0] < paddle[0]) {
                processor.input({-1});
            } else if (paddle[0] < ball[0]) {
                processor.input({1});
            } else {
                processor.input({0});
            }
        }
        std::cout << "blocks=" << blocks.size() << " score=" << score << '\n';
    }
    return 0;
}
} // namespace

int main(int argc, char** argv)
{
    return Main(std::vector<std::string_view>{argv, argc + argv});
}
