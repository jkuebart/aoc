#include "intcode/processor.h"

#include <iostream>
#include <string>
#include <string_view>
#include <vector>

namespace
{
int Main(std::vector<std::string_view> args)
{
    intcode::Processor processor;
    while (std::cin >> processor) {
        for (unsigned int i{1}; 1 + i < args.size(); i += 2) {
            processor.set(
                std::stoi(std::string{args[i]}),
                std::stoi(std::string{args[1 + i]}));
        }
        std::cout << processor.run() << '\n';
    }
    return 0;
}
} // namespace

int main(int argc, char** argv)
{
    return Main(std::vector<std::string_view>{argv, argc + argv});
}
