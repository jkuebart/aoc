#include "intcode/processor.h"

#include <sysexits.h>

#include <iostream>
#include <string>
#include <string_view>
#include <vector>

namespace
{
int Main(std::vector<std::string_view> args)
{
    if (2 != args.size()) {
        std::cerr << "usage: " << args[0] << R"( output

    Find the noun, verb combination that produces the given output.
)";
        return EX_USAGE;
    }

    intcode::Processor processor;
    while (std::cin >> processor) {
        for (int noun{0}; noun != 100; ++noun) {
            for (int verb{0}; verb != 100; ++verb) {
                intcode::Processor p0{processor};
                p0.set(1, noun);
                p0.set(2, verb);
                p0.run();
                if (std::stoi(std::string{args[1]}) == p0.get(0)) {
                    std::cout << 100 * noun + verb << '\n';
                }
            }
        }
    }
    return 0;
}
} // namespace

int main(int argc, char** argv)
{
    return Main(std::vector<std::string_view>{argv, argc + argv});
}
