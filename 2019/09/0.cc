#include "intcode/processor.h"
#include "streamer/stream.h"

#include <sysexits.h>

#include <iostream>
#include <string>
#include <string_view>
#include <vector>

using streamer::stream;

namespace
{
int Main(std::vector<std::string_view> args)
{
    if (2 != args.size()) {
        std::cerr << "usage: " << args[0] << R"( mode

    Run BOOST program in test mode (1) or boost mode (2).
)";
        return EX_USAGE;
    }
    intcode::Processor processor{};
    while (std::cin >> processor) {
        std::cerr << "program " << processor << '\n';
        processor.input({std::stoi(std::string{args[1]})});
        processor.run();
        std::cout << stream(processor.output(), "\n") << '\n';
        std::cerr << "result " << processor << '\n';
    }
    return 0;
}
} // namespace

int main(int argc, char** argv)
{
    return Main(std::vector<std::string_view>{argv, argc + argv});
}
