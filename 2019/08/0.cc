#include <sysexits.h>

#include <algorithm>
#include <iostream>
#include <string>
#include <string_view>
#include <vector>

namespace
{
int Main(std::vector<std::string_view> args)
{
    if (3 != args.size()) {
        std::cerr << "usage: " << args[0] << R"( width height

    Read a Digital Sending Network image of the given width and height
    from standard input and output its checksum.
)";
        return EX_USAGE;
    }

    unsigned long const layerSize{
        std::stoul(std::string{args[1]}) * std::stoul(std::string{args[2]})};

    for (std::string line{}; std::getline(std::cin, line);) {
        unsigned long const layers{
            static_cast<unsigned long>(line.size()) / layerSize};

        unsigned long minZeros{layerSize};
        std::string_view minZeroLayer{};
        for (unsigned long i{0}; i != layers; ++i) {
            std::string_view const layer{
                std::string_view{line}.substr(i * layerSize, layerSize)};
            unsigned long const zeros{static_cast<unsigned long>(std::count_if(
                layer.begin(),
                layer.end(),
                [](char ch)
                {
                    return '0' == ch;
                }))};
            std::cerr << "layer=" << i << " zeros=" << zeros << '\n';
            if (zeros < minZeros) {
                minZeros = zeros;
                minZeroLayer = layer;
            }
        }

        auto const ones{std::count_if(
            minZeroLayer.begin(),
            minZeroLayer.end(),
            [](char const ch)
            {
                return '1' == ch;
            })};
        auto const twos{std::count_if(
            minZeroLayer.begin(),
            minZeroLayer.end(),
            [](char const ch)
            {
                return '2' == ch;
            })};
        std::cerr << "minZeros=" << minZeros << " ones=" << ones
                  << " twos=" << twos << '\n';
        std::cout << ones * twos << '\n';
    }
    return 0;
}
} // namespace

int main(int argc, char** argv)
{
    return Main(std::vector<std::string_view>{argv, argc + argv});
}
