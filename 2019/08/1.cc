#include <sysexits.h>

#include <algorithm>
#include <cassert>
#include <iostream>
#include <string>
#include <string_view>
#include <vector>

namespace
{
int Main(std::vector<std::string_view> args)
{
    if (3 != args.size()) {
        std::cerr << "usage: " << args[0] << R"( width height

    Read a Digital Sending Network image of the given width and height
    from standard input and display it.
)";
        return EX_USAGE;
    }

    unsigned long const width{std::stoul(std::string{args[1]})};
    unsigned long const height{std::stoul(std::string{args[2]})};

    for (std::string line{}; std::getline(std::cin, line);) {
        std::string result(width * height, '.');
        for (std::size_t i{line.size()}; 0 != i;) {
            --i;
            switch (line[i]) {
            case '0':
                result[i % result.size()] = ' ';
                break;
            case '1':
                result[i % result.size()] = '*';
                break;
            case '2':
                break;
            default:
                assert(false);
                break;
            }
        }

        for (std::size_t begin{0}; begin < result.size(); begin += width) {
            std::cout << std::string_view{result}.substr(begin, width) << '\n';
        }
    }
    return 0;
}
} // namespace

int main(int argc, char** argv)
{
    return Main(std::vector<std::string_view>{argv, argc + argv});
}
