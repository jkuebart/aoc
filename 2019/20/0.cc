#include "geo/bounds.h"
#include "geo/direction.h"
#include "geo/point.h"
#include "streamer/stream.h"

#include <sysexits.h>

#include <iostream>
#include <map>
#include <optional>
#include <queue>
#include <set>
#include <string>
#include <string_view>
#include <tuple>
#include <utility>
#include <vector>

using streamer::stream;

using Direction = geo::Direction<2>;
using Point = geo::Point<Direction>;
using Bounds = geo::Bounds<Point>;

namespace
{
class Maze
{
public:
    char get(Point const& point) const
    {
        unsigned row{static_cast<unsigned>(point[0])};
        unsigned column{static_cast<unsigned>(point[1])};
        if (row < mMaze.size() && column < mMaze[row].size()) {
            return mMaze[row][column];
        }
        return ' ';
    }

    unsigned pathLength(bool const levels) const
    {
        std::queue<std::tuple<int, Point, unsigned>> seen{{{0, mStart, 0}}};
        std::set<std::tuple<int, Point>> visited{{0, mStart}};
        while (!seen.empty()) {
            auto const& [level, current, cost]{seen.front()};

            if (0 == level && mFinish == current) {
                return cost;
            }

            current.adjacent(
                [this, cost = cost, level = level, &seen, &visited](
                    Point const& point)
                {
                    if (0 == visited.count({level, point})) {
                        visited.insert({level, point});
                        if ('.' == get(point)) {
                            seen.push({level, point, 1 + cost});
                        }
                    }
                });

            auto const portal{mPortals.find(current)};
            if (mPortals.end() != portal) {
                int const nextLevel{
                    levels ? level + portal->second.first : level};
                if (0 <= nextLevel &&
                    0 == visited.count({nextLevel, portal->second.second})) {
                    visited.insert({nextLevel, portal->second.second});
                    seen.push({nextLevel, portal->second.second, 1 + cost});
                }
            }

            seen.pop();
        }
        return 0;
    }

    friend std::istream& operator>>(std::istream& is, Maze& maze)
    {
        Bounds bounds{};
        for (std::string line{}; getline(is, line);) {
            maze.mMaze.push_back(line);
            bounds.include({0, 0});
            bounds.include(
                {static_cast<int>(maze.mMaze.size() - 1),
                 static_cast<int>(line.size() - 1)});
        }
        std::map<std::string, Point> portals{};
        bounds.each(
            [&bounds, &maze, &portals](Point const& point)
            {
                // Find letter pairs next to open spaces.

                if ('.' != maze.get(point)) {
                    return;
                }
                auto const letter0{maze.findAdjacentLetter(point)};
                if (!letter0) {
                    return;
                }
                auto const letter1{maze.findAdjacentLetter(letter0->first)};
                if (!letter1) {
                    return;
                }
                std::string portal{};
                if (letter0->first < letter1->first) {
                    portal = {letter0->second, letter1->second};
                } else {
                    portal = {letter1->second, letter0->second};
                }

                // Remember start or finish.

                if ("AA" == portal) {
                    maze.mStart = point;
                } else if ("ZZ" == portal) {
                    maze.mFinish = point;
                } else {
                    auto const found{portals.find(portal)};
                    if (portals.end() != found) {

                        // If the counterpart for this portal is already known,
                        // create inner and outer portals depending on the
                        // location of this point.

                        int const direction{
                            bounds.deflate(3).contains(point) ? 1 : -1};
                        maze.mPortals.insert(
                            {point, {direction, found->second}});
                        maze.mPortals.insert(
                            {found->second, {-direction, point}});
                        portals.erase(found);
                    } else {

                        // Remember unmatched portals.

                        portals.insert({portal, point});
                    }
                }
            });
        if (!portals.empty()) {
            std::cerr << "Unmatched portals: " << portals.size() << '\n';
        }
        return is;
    }

    friend std::ostream& operator<<(std::ostream& os, Maze const& maze)
    {
        os << stream(maze.mMaze, "\n") << '\n'
           << "start={" << maze.mStart << "} finish={" << maze.mFinish
           << "}\n\nportals:\n";
        for (auto const& p : maze.mPortals) {
            os << "  from=" << p.first << " to=" << stream(p.second) << "\n";
        }
        return os;
    }

private:
    std::optional<std::pair<Point, char>> findAdjacentLetter(Point const& point)
    {
        std::optional<std::pair<Point, char>> letter{};
        point.adjacent(
            [this, &letter](Point const& pnt)
            {
                char const square{get(pnt)};
                if ('A' <= square && square <= 'Z') {
                    letter = {pnt, square};
                }
            });
        return letter;
    }

private:
    std::vector<std::string> mMaze;
    std::map<Point, std::pair<int, Point>> mPortals;
    Point mStart{}, mFinish{};
};

int Main(std::vector<std::string_view> args)
{
    if (2 != args.size()) {
        std::cerr << "usage: " << args[0] << R"( levels

    Read a maze from standard input and find a shortest path, taking levels
    into account if the argument is nonzero.
)";
        return EX_USAGE;
    }
    Maze maze{};
    std::cin >> maze;
    std::cerr << maze << '\n';
    std::cout << maze.pathLength(0 != std::stoi(std::string{args[1]})) << '\n';
    return 0;
}
} // namespace

int main(int argc, char** argv)
{
    return Main(std::vector<std::string_view>{argv, argc + argv});
}
