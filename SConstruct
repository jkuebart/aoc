from SCons.Tool import FindAllTools

# Allow user to specify build mode.

vars = Variables()
vars.AddVariables(
    ListVariable(
        "MODES",
        "The build mode.",
        "release",
        ["debug", "release"]
    ),
)

common = Environment(tools=["default", RegressionTest, Roc], variables=vars)

# Prefer clang if available (not detected as part of "default").

for tool in FindAllTools(["clang", "clangxx", "roc"], common):
    common.Tool(tool)

Help(vars.GenerateHelpText(common), append=True)
if vars.UnknownVariables():
    print("Unknown command line variables: %s" % (vars.UnknownVariables()))
    Exit(64) # EX_USAGE

# Common configuration.

common.Append(
    CCFLAGS=["-g"],
    CPPPATH=["#include"],
    CXXFLAGS=["-std=c++17"],
)

if common.subst("$CC") == "clang":
    common.Append(CCFLAGS=[
        "-Weverything",
        "-Wno-padded",
        "-Wno-poison-system-directories",
    ])

if common.subst("$CC") == "gcc":
    common.Append(CCFLAGS=["-Wall", "-pedantic"])

if common.subst("$CXX") == "clang++":
    common.Append(CXXFLAGS=["-Wno-c++98-compat"])

# Set up build-mode-specific environments.

for mode in common["MODES"]:
    env = common.Clone()

# debug

    if "debug" == mode:
        env.Append(CCFLAGS=["-O0"])

        if env.subst("$CC") == "clang":
            env.Append(
                CCFLAGS=["-fsanitize=address,undefined"],
                LINKFLAGS=["-fsanitize=address,undefined"],
            )

# release

    if "release" == mode:
        env.Append(
            CCFLAGS=["-O"],
            CPPDEFINES=["NDEBUG"],
        )

# Build each configuration.

    env.SConscript(
        dirs=["."],
        duplicate=0,
        exports=["env"],
        variant_dir=".obj/$PLATFORM/{}".format(mode),
    )
